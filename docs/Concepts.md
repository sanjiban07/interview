# Important Concepts
## What is an API
API is the acronym for Application Programming Interface, which is a software intermediary that allows two applications to talk to each other. 
## Distributed Caching
https://www.youtube.com/watch?v=U3RkDLtS7uY
## Consistent Hashing
https://www.youtube.com/watch?v=zaRkONvyGr8
## Load Balancing
https://www.youtube.com/watch?v=K0Ta65OqQkY
## Message Queue
https://www.youtube.com/watch?v=oUJbuFMyBDk
## Microservice architecture
https://www.youtube.com/watch?v=qYhRvH9tJKw
## How databases scale writes: The power of the log
https://www.youtube.com/watch?v=_5vrfuwhvlQ