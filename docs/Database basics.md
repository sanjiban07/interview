# Database Basic Concepts

## ACID
* Atomicity
By this, we mean that either the entire transaction takes place at once or doesn’t happen at all. It involves the following two operations. 
	* Abort: If a transaction aborts, changes made to database are not visible. 
	* Commit: If a transaction commits, changes made are visible. 
* Consistency 
This means that integrity constraints must be maintained so that the database is consistent before and after the transaction
* Isolation 
This property ensures that multiple transactions can occur concurrently without leading to the inconsistency of database state. Transactions occur independently without interference.
* Durability: 
This property ensures that once the transaction has completed execution, the updates and modifications to the database are stored in and written to disk and they persist even if a system failure occurs.

## Join
Combine data or rows from two or more tables based on a common field
* INNER JOIN
* LEFT JOIN
* RIGHT JOIN
* FULL JOIN

## Indexing
Indexing is a way to optimize the performance of a database by minimizing the number of disk accesses required when a query is processed.
Indexes are created using a few database columns. 
* Search key contains a copy of the primary key or candidate key of the table. These values are stored in sorted order so that the corresponding data can be accessed quickly.
* Data Reference or Pointer which contains a set of pointers holding the address of the disk block where that particular key value can be found.
Primary Indexing: 
### Types of indexing 
* Clustered Indexing:
When more than two records are stored in the same file these types of storing known as cluster indexing.
	* Primary Indexing: This is a type of Clustered Indexing wherein the data is sorted according to the search key and the primary key of the database table is used to create the index.
* Non-clustered or Secondary Indexing 
* Multilevel Indexing

## Partitioning
Partitioning is the database process where very large tables are divided into multiple smaller parts. By splitting a large table into smaller, individual tables, queries that access only a fraction of the data can run faster because there is less data to scan.
### Types of partitioning
* Horizontal partitioning (sharding) divides a table into multiple tables. Each table then contains the same number of columns, but fewer rows. 
Sharding improves performance in following way:
	* reduced index size
	* distribution of the database over a large number of machines
	* segment data by geography 
* Vertical partitioning (normalization) divides a table into multiple tables that contain fewer columns.

## Others
#### DELETE vs TRUNCATE
| DELETE | TRUNCATE |
| ------ | -------- |
| Used when we want to remove some or all of the records from the table. | TRUNCATE statement will delete entire rows from a table. |
| DELETE allows WHERE clause | TRUNCATE command does not allow to use WHERE clause
| performs deletion row-by-row one at a time from the | operates on data pages instead of rows because it deletes entire table data at a time. |
| DELETE statement only deletes records and does not reset the table's identity | TRUNCATE resets the identity of a particular table. |
| DELETE command require more locks and database resources because it acquires the lock on every deleted row. | TRUNCATE acquires the lock on the data page before deleting the data page; thus, it requires fewer locks and few resources. |
| DELETE statement makes an entry in the transaction log for each deleted row | TRUNCATE records the transaction log for each data page. |
| Slower | Faster |
| We can recover the deleted data back which we removed from the DELETE operation. | Once the record deletes by using the TRUNCATE command, we cannot recover it back. |

#### WHERE vs HAVING
| Where | Having |
| ----- | ------ |
| WHERE Clause is used to filter the records from the table based on the specified condition. | HAVING Clause is used to filter record from the groups based on the specified condition. |
| WHERE Clause implements in row operations | HAVING Clause implements in column operation |
| WHERE Clause is used with single row function like UPPER, LOWER etc. | HAVING Clause is used with multiple row function like SUM, COUNT etc. |

#### CHAR vs VARCHAR 
| CHAR | VARCHAR |
| - | - |
| Used to store character string of fixed length | Used to store character string of variable length |
| CHAR take 1 byte for each character | VARCHAR take 1 byte for each character and some extra bytes for holding length information |
| Better performance than VARCHAR | Performance is not good as compared to CHAR |

## Database Normalization ([link](https://www.guru99.com/database-normalization.html))
Database normalization is the process of structuring a database, usually a relational database, in accordance with a series of so-called normal forms in order to reduce data redundancy and improve data integrity.
* 1NF (First Normal Form)
* 2NF (Second Normal Form)
* 3NF (Third Normal Form)
* BCNF (Boyce-Codd Normal Form)

## NoSQL Pros & Cons ([video](https://www.youtube.com/watch?v=xQnIN9bW0og))
* Pros
	* Insertion & retrieval - read/write is less costly since it stores the whole data as a blob
	* Schemaless - schema is easily changeable
	* Horizontal partitioning - built for scale
	* Built for metrics, analytics, aggregations
* Cons
	* Not built for updates - consistancy is a problem since ACID is not guranteed.
	* Not read optimized - read times are generally slower
	* Relatioships are not implicit
	* Joins are hard

## Elasticsearch
### Charactaristics
* NoSQL, Json based datastore
* Interaction is based on Rest Apis
* real-time search engine
* distributed document-oriented, easy to scale up
* easy-backup, multi tenancy
* Major Usage: Logs, metrics, App trace data
### Comparison with RDBMS
* DB -> Indexes/Indices
* Tables -> Patterns/Types
* Rows -> Documents
* Columns -> Fields
### ELK Stack
* Elasticsearch
	* Document storage, text search
* Kibana: 
	* UI Dashboard
	* Widgets, Visualization
* Logstash
	* Input, Transform, Trash

### Elasticsearch vs MongoDB
| What is Elasticsearch?	| What is MongoDB? |
| - | - |
| Since its release in 2010, Elasticsearch  has become one of the world’s top ten databases by popularity. Originally based on Apache’s Lucene search engine, it remains an open-source product, built using Java, and storing data in an unstructured NoSQL format. |	MongoDB is an open-source NoSQL database management program, which can be used to manage large amounts of data in a distributed architecture. It is the world’s most popular document store and is in the top 5 most popular databases in general. 
Elasticsearch is built for search and provides advanced data indexing capabilities. For data analysis, it operates alongside Kibana, and Logstash to form the ELK stack. | MongoDB lets you manage, store and retrieve document-oriented information. It provides features such as fast ad-hoc queries, indexing, load balancing, data aggregation, and server-side JavaScript execution.
Elasticsearch is written in Java and based on the open-source Lucene search engine. It writes data to inverted indexes using Lucene segments. Settings, index mapping, alternative cluster states, and other metadata are saved to Elasticsearch files outside the Lucene environment. | MongoDB is better suited for high write and update throughput operations without draining CPU resources and/or causing disk I/O issues. It is written in C++ and uses a memory map file to map on-disk data files to in-memory byte arrays. It organizes data using a doubly linked data structure: documents contain linked lists to one another and to the BSON-encoded data behind the scenes.