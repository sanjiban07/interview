# Docker
## Container
Application centric way to deliver high-performing, scalable application in infrastructure of your choice.
### Advantage
* Saves time - platform independent(If works in dev, it will work everywhere)
* Cost effective (More apps can be packed in one package)
* Rapid development
* Version control

### VM vs Containers
* Containers are like lightweight VMs
* Runs directly on host OS
## Chroot
Chroot is an operation that changes the apparent root directory for the current running process and their children. A program that is run in such a modified environment cannot access files and commands outside that environmental directory tree. This modified environment is called a chroot jail.
### Lab:
```
# Install debootstrap, which helps us installing a Debian-based system into a subdirectory of another, already installed system.
> sudo apt install debootstrap

# Create a directory
> sudo mkdir /mnt/chroot-ubuntu-trusty

# Using debootstrap, install a Ubuntu distribution in the directory created in earlier step.
> sudo debootstrap trusty /mnt/chroot-ubuntu-trusty http://archive.ubuntu.com/ubuntu/

# chroot into your new chrooted environment.
> sudo chroot /mnt/chroot-ubuntu-trusty /bin/bash

# With the above command, we'll enter into the chrooted environment and, whatever command we would run, they would run with respect to new root. In the above case, the new root would be /mnt/chroot-ubuntu-trusty.
> cat /etc/os-release

# To come out from the chrooted environment type exit.
> exit
```
## Docker
* Provides a platform for developing, shipping and running applications.
* Docker engine hels to run and manage the containers.
::: mermaid
graph TD
	UI[Docker UI] --> DE[Docker Engine]
	DE --> cd[containerd]
	cd --> C1["runc <br> (container#1)"]
	cd --> C2["runc <br> (container#2)"]
	cd --> C3["runc <br> (container#3)"]
:::
::: mermaid
graph LR
	CLI[Docker CLI] --> CR[Rest Api]
	CR[Rest Api] --> SR[Rest Api]
	SR --> DD[Docker<br>Daemon]
	CR[Rest Api] --> SR1[Rest Api]
	SR1 --> DD1[Docker<br>Daemon]
	
	subgraph Docker Client
		CLI & CR
	end
	subgraph "Docker Server (corporate)"
		SR & DD
	end
	subgraph "Docker Server (public)"
		SR1 & DD1
	end
:::
### Basic commands
#### Version
> docker version
####  Info
> docker info
####  Pull
By default, the pull command will pull the images from Docker Registry/Hub. We can pull the images from a private registry as well, as follows:-
```
# sudo docker image pull <private_registry>:<port>/<image>
> sudo docker image pull alpine
```
####  List images
```
sudo docker image ls 
```
#### When we create a container:
* A process will be created in the host system.
* Namespaces like pid, net, uts will be attached to that process.
* With control groups config, host OS would set resource usage limits.
* Security context will be set.
```
sudo docker container create --name test1 -i -t alpine sh
```
#### Start, stop, restart, remove container
```
sudo docker container start test1
sudo docker container stop test2
sudo docker container rename test2 while_loop_container
sudo docker container restart test1
sudo docker container rm test2
```
#### List running containers
```
sudo docker container ls
```
#### Run
Create and start a container in one go.
```
sudo docker container run -i -t --name myalpine1 alpine sh

## Run in background
sudo docker container run --name test2 -d ubuntu /bin/bash -c 'while [ 1 ]; do echo "hello world from container"; sleep 1; done'

## Set hostname
sudo docker container run -h alpine -it --rm alpine sh

## Change current working directory
sudo docker container run -it -w /var --rm alpine sh

## Use environment varia lesudo docker container run -it --env "WEB_HOST=172.168.1.1" --rm alpine sh

## Set ulimit
sudo docker container run -it --ulimit nproc=10 --rm alpine sh

## Bind and run on given CPU(s)
sudo docker container run -d --name cpulimitAlpine --cpuset-cpus="0" alpine top

## Verify the setting from the docker container inspect command.
sudo docker container inspect cpulimitAlpine 

## Remove cpulimit
sudo docker container rm -f cpulimitAlpine

## Set memory of a container
sudo docker container run -d --name memorylimitAlpine --memory "200m" alpine top

## Restart o failover
sudo docker container run -d --restart=on-failure:3 --name web2 nginx

## Label a container
sudo docker container run -d --label env=dev nginx
## Use label to search
sudo docker container ls --filter label=env=dev

## Privileged access
sudo docker container run -it --net=host --privileged alpine sh

## Expose port
sudo docker container run -d --name web -p 8088:80 nginx
## Expose docker port to random host port
sudo docker container run -d --name web3 -P nginx:alpine

## Use particular network
sudo docker container run -it --network=container:web alpine sh
## Use host network
sudo docker container run -it --network=host alpine sh
```
#### Docker network command
Docker uses container network interface (CNI) for communicating.
```
## List
sudo docker network ls
## Inspect
sudo docker network inspect 8446379bf701
## Create
sudo docker network create mynet
## Connect running container to specific network
sudo docker network connect mynet web4
# Disconnect
sudo docker network disconnect mynet web4
# Remove network
sudo docker network rm mynet
```
#### Attach
```
sudo docker container attach myalpine
```
#### Making changes and push
```
## Get difference
sudo docker container diff myalpine
## Commit
sudo docker container commit myalpine cloudyuga/alpine:training
## Push
sudo docker image push cloudyuga/alpine:training
```
#### Build image from docker file
```
sudo docker image build -t cloudyuga/alpine:dockerfile myapp
```

## Dockerfile
### Single stage 
```
FROM node:13-alpine
ENV MONGO_DB_USERNAME=admin \
    MONGO_DB_PWD=password

RUN mkdir -p /home/app
COPY ./app /home/app

# set default dir so that next commands executes in /home/app dir
WORKDIR /home/app

# will execute npm install in /home/app because of WORKDIR
RUN npm install

# no need for /home/app/server.js because of WORKDIR
CMD ["node", "server.js"]
```
### Multi stage
Images created using the multi-stage build is generally smaller than the single stage builds.
```
FROM ubuntu AS buildstep
RUN apt-get update && apt-get install -y build-essential gcc
COPY hello.c /app/hello.c
WORKDIR /app
RUN gcc -o hello hello.c && chmod +x hello
 
FROM ubuntu
RUN mkdir -p /usr/src/app/
WORKDIR /usr/src/app
COPY --from=buildstep /app/hello ./hello
COPY ./start.sh ./start.sh
CMD ["bash", "/usr/src/app/start.sh"]
```

## Docker-compose
### Example
```
version: '3.3'

services:
   db:
     image: mysql:5.7
     volumes:
       - db_data:/var/lib/mysql
     restart: always
     environment:
       MYSQL_ROOT_PASSWORD: somewordpress
       MYSQL_DATABASE: wordpress
       MYSQL_USER: wordpress
       MYSQL_PASSWORD: wordpress

   wordpress:
     depends_on:
       - db
     image: wordpress:latest
     ports:
       - "8000:80"
     restart: always
     environment:
       WORDPRESS_DB_HOST: db:3306
       WORDPRESS_DB_USER: wordpress
       WORDPRESS_DB_PASSWORD: wordpress
       WORDPRESS_DB_NAME: wordpress
volumes:
    db_data: {}
```

### Exercise
#### RSVPapp
* Backend
	* image : registry.cloudyuga.guru/library/mongo:3.3
	* port: 27017
	* volume: /data/db
* Frontend
	* Image: registry.cloudyuga.guru/library/rsvpapp
	* port: 5000
* env:
	* MONGODB_HOST: <backend container name>:27017
```
version: '3.3'
services:
  mongo_backend:
    image: registry.cloudyuga.guru/library/mongo:3.3
    container_name: mongo_backend
    ports:
      - 27017:27017
    # environment:
    #   - MONGO_INITDB_DATABASE=test
    #   - MONGO_INITDB_ROOT_USERNAME=admin
    #   - MONGO_INITDB_ROOT_PASSWORD=admin
    volumes:
      - mongodb:/data/db
    # networks:
    #   - rsvp_net

  rsvp_frontend:
    depends_on:
      - mongo_backend
    image: registry.cloudyuga.guru/library/rsvpapp
    ports:
      - "80:5000"
    restart: always
    environment:
      MONGODB_HOST: mongo_backend:27017
    # networks:
    #   - rsvp_net

volumes:
  mongodb: {}

networks:
  mongo_net:
    driver: bridge
```









