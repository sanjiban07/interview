# Kubernetes
Open source container orchestration tool.
### Features
* High availability - No downtime(?)
* Scalability - high performance
* Disaster recovery - backup and restore

## Architecture
::: mermaid
flowchart TD
	C <--> Api
	S <--> Api
	Api --> K1[Kubelet]
	Api --> K2[Kubelet]
	Api --> K3[Kubelet]
	Api --> KV
	KV[Key-Value store]
	subgraph Master Node
		C[Controller]
		S[Scheduler]
		Api{Api-server}
	end
	subgraph Node#3
		KP3[Kube-proxy]
		K3
	end
	subgraph Node#2
		KP2[Kube-proxy]
		K2
	end
	subgraph Node#1
		KP1[Kube-proxy]
		K1
	end
	subgraph .
		KV
	end
:::
### Api-server (kube-apiserver)
* High available
* Provides REST interface to Control Plane.
* All communication between different component happens via api-server.
* Handles
	* Authentication
	* Authorization
	* Admission control
### Controller (kube-controller-manager)
* Monitors cluster state via api-server and makes sure current cluster state is desired state at any point of time.
* Build-in controllers
	* Node Controller
	* Replication Controller
	* Endpoint Controller
	* Service Account Controller and Token Controller
### Scheduler (kube-scheduler)
* Watches the newly created pods and assigns pods to the most feasible nodes.
::: mermaid
graph TD
	subgraph Node
		Kubelet
		Kube-Proxy
		subgraph Container endpoint
			subgraph Pod#1
				Container#1
				Container#2
				Container#3
			end
			subgraph Pod#2
				Container#4
			end
		end
	end
:::
### Key-value store
* Stores all the cluster information.
* Single point of truth
### Pods
* Smallest unit in k8
* Abstraction over the container
* Usually a pod will contain a single container
* Each pods will have unique IP
### Container Network Interface (CNI)
Kubernetes create the containers and offloads the work to the CNI driver to create the network for the pods.
## Basic commands
```
kubectl version
kubectl get nodes
kubectl proxy
kubectl api-versions
```
### Create and list a pod
* Create a YAML file
```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
  labels:
     app: nginx
spec:
  containers:
  - name: nginx-demo
    image: registry.cloudyuga.guru/library/nginx:alpine
    ports:
    - containerPort: 80
```
* Basic commands
``` 
# Create the Pod
kubectl create -f pod.yaml

# List the Pods
kubectl get pods
kubectl get pods -o wide

# Describe the Pods
kubectl describe pod mypod

# Get the Pod details in yaml format
kubectl get pod mypod -o yaml

# Delete the Pod
kubectl delete pod mypod
```