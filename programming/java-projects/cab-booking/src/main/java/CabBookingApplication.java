import controller.CabBookingController;

import java.io.*;

public class CabBookingApplication {
    public static void main(String args[]) throws IOException {
        CabBookingController controller = new CabBookingController();
        BufferedWriter bw = new BufferedWriter(new FileWriter("src/main/resources/output.txt"));
        BufferedReader br = new BufferedReader(new FileReader("src/main/resources/input.txt"));
        for(String line; (line = br.readLine()) != null; ) {
            System.out.println("$ " + line);
            String output = controller.execute(line);
            System.out.println(output);
            bw.write(output);
            bw.newLine();
        }
        bw.close();
    }
}
