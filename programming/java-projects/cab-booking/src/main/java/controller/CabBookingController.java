package controller;

import models.CommandInput;
import models.CommandOutput;
import services.ICommandExecutor;
import services.impl.CabBookingService;

import java.util.Arrays;

public class CabBookingController {
    private final ICommandExecutor commandExecutor;

    public CabBookingController() {
        commandExecutor = new CabBookingService();
    }
    public String execute(String inputStr) {
        String[] inputStrSplit = inputStr.split("\\s+");
        CommandInput commandInput = CommandInput.builder()
                .cmd(inputStrSplit[0])
                .args(Arrays.copyOfRange(inputStrSplit, 1, inputStrSplit.length))
                .build();
        try {
            CommandOutput commandOutput = commandExecutor.execute(commandInput);
            return commandOutput.getMsg().orElse("");
        } catch (Exception exception) {
            return exception.getMessage();
        }
    }
}
