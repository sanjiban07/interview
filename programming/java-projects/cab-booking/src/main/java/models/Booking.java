package models;

import lombok.Data;

@Data
public class Booking {
    public enum Status {
        IN_PROGRESS, COMPLETED;
    }

    private static long _ID = 1;
    private String id;
    private Driver driver;
    private Status status;
    private long distance;

    public Booking(Driver driver, long distance) {
        this.id = "booking-" + _ID++;
        this.driver = driver;
        this.distance = distance;
        status = Status.IN_PROGRESS;
    }
}
