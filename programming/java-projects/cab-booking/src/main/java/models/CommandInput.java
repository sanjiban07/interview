package models;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

@Data
@Builder
public class CommandInput {
    private @NonNull String cmd;
    private @NonNull String[] args;
}
