package models;

import lombok.Builder;
import lombok.Data;

import java.util.Optional;

@Data
@Builder
public class CommandOutput {
    public enum Status {
        SUCCESSFUL, FAILED, INVALID;
    }
    private Optional<String> msg;
    private Status status;
}
