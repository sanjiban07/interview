package models;

import lombok.Data;
import lombok.NonNull;

import java.util.Objects;

@Data
public class Driver {
    public enum Status {
        BUSY, AVAILABLE;
    }

    private @NonNull String id;
    private Status status;

    public Driver(@NonNull String id) {
        this.id = id;
        status = Status.AVAILABLE;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
