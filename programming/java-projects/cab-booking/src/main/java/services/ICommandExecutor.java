package services;

import exceptions.InvalidArgumentException;
import exceptions.InvalidCommandException;
import models.CommandInput;
import models.CommandOutput;

public interface ICommandExecutor {
    CommandOutput execute(CommandInput input) throws InvalidCommandException, InvalidArgumentException;
}
