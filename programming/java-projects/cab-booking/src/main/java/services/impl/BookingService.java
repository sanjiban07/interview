package services.impl;

import exceptions.InvalidArgumentException;
import models.Booking;
import models.CommandInput;
import models.CommandOutput;
import models.Driver;
import services.ICommandExecutor;

import java.util.*;

import static models.Driver.Status.BUSY;

public class BookingService implements ICommandExecutor {
    private final DriverService driverService;
    private final Map<String, Booking> bookingMap;

    public BookingService(DriverService driverService) {
        this.driverService = driverService;
        bookingMap = new HashMap<>();
    }

    @Override
    public CommandOutput execute(CommandInput input) throws InvalidArgumentException {
        switch (input.getCmd()) {
            case "dispatch_driver_for_a_booking":
                if (input.getArgs().length == 0) {
                    throw new InvalidArgumentException("dispatch_driver_for_a_booking takes 1 argument!!");
                }
                return dispatch(input.getArgs()[0]);
            case "complete_booking":
                if (input.getArgs().length == 0) {
                    throw new InvalidArgumentException("dispatch_driver_for_a_booking takes 1 argument!!");
                }
                return complete(input.getArgs()[0]);
            default:
                return CommandOutput.builder()
                        .status(CommandOutput.Status.INVALID)
                        .msg(Optional.empty())
                        .build();
        }
    }

    private CommandOutput complete(String bookingId) {
        Booking booking = bookingMap.getOrDefault(bookingId, null);
        if (booking == null) {
            return CommandOutput.builder()
                    .status(CommandOutput.Status.FAILED)
                    .msg(Optional.of("Incorrect booking id"))
                    .build();
        }
        booking.setStatus(Booking.Status.COMPLETED);
        booking.getDriver().setStatus(Driver.Status.AVAILABLE);
        return CommandOutput.builder()
                .status(CommandOutput.Status.SUCCESSFUL)
                .msg(Optional.of("Driver " + booking.getDriver().getId() + " is released to allocation pool"))
                .build();
    }

    private CommandOutput dispatch(String distanceStr) throws InvalidArgumentException {
        long distance;
        try {
            distance = Long.parseLong(distanceStr);
            if (distance < 1) {
                throw new Exception("Distance must be greater than '0'");
            }
        } catch (Exception exception) {
            throw new InvalidArgumentException(exception);
        }

        Optional<Driver> optionalDriver = driverService.findIdleDriver();
        if (optionalDriver.isPresent()) {
            optionalDriver.get().setStatus(BUSY);
            Booking booking = new Booking(optionalDriver.get(), distance);
            bookingMap.put(booking.getId(), booking);
            String msg = "Driver " + booking.getDriver().getId()
                    + " is assigned to booking " + booking.getId()
                    + " with " + booking.getDistance()
                    + " km distance";
            return CommandOutput.builder()
                    .status(CommandOutput.Status.SUCCESSFUL)
                    .msg(Optional.of(msg))
                    .build();
        } else {
            return CommandOutput.builder()
                    .status(CommandOutput.Status.FAILED)
                    .msg(Optional.of("Sorry, driver is not available"))
                    .build();
        }
    }

    public Collection<Booking> getBookings() {
        return bookingMap.values();
    }
}
