package services.impl;

import exceptions.InvalidArgumentException;
import exceptions.InvalidCommandException;
import models.CommandInput;
import models.CommandOutput;
import services.ICommandExecutor;

import java.util.Arrays;
import java.util.List;

public class CabBookingService implements ICommandExecutor {
    private final List<ICommandExecutor> commandExecutors;

    public CabBookingService() {
        DriverService driverService = new DriverService();
        BookingService bookingService = new BookingService(driverService);
        commandExecutors = Arrays.asList(
                driverService,
                bookingService,
                new QueryService(bookingService)
        );
    }

    //    public CabBookingService()
    @Override
    public CommandOutput execute(CommandInput input) throws InvalidCommandException, InvalidArgumentException {
        for (ICommandExecutor executor: commandExecutors) {
            CommandOutput output = executor.execute(input);
            if (output.getStatus() != CommandOutput.Status.INVALID) {
                return output;
            }
        }

        throw new InvalidCommandException("Command '" + input + "' is invalid!!");
    }
}
