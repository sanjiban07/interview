package services.impl;

import exceptions.InvalidArgumentException;
import models.CommandInput;
import models.CommandOutput;
import models.Driver;
import services.ICommandExecutor;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

public class DriverService implements ICommandExecutor {
    private final Set<Driver> drivers;

    public DriverService() {
        drivers = new HashSet<>();
    }

    @Override
    public CommandOutput execute(CommandInput input) throws InvalidArgumentException {
        switch (input.getCmd()) {
            case "register_driver":
                if (input.getArgs().length == 0) {
                    throw new InvalidArgumentException("register_driver takes 1 argument!!");
                }
                return register(input.getArgs()[0]);
            default:
                return CommandOutput.builder()
                        .status(CommandOutput.Status.INVALID)
                        .msg(Optional.empty())
                        .build();
        }
    }

    public CommandOutput register(String id) {
        Driver driver = new Driver(id);
        if (drivers.contains(driver)) {
            return CommandOutput.builder().status(CommandOutput.Status.FAILED)
                    .msg(Optional.of("Driver exists"))
                    .build();
        } else {
            drivers.add(driver);
            return CommandOutput.builder().status(CommandOutput.Status.FAILED)
                    .msg(Optional.of("Driver " + driver.getId() + " registered"))
                    .build();
        }
    }

    public Optional<Driver> findIdleDriver() {
        return drivers.stream().filter(driver -> driver.getStatus() == Driver.Status.AVAILABLE).findAny();
    }
}
