package services.impl;

import exceptions.InvalidArgumentException;
import exceptions.InvalidCommandException;
import models.Booking;
import models.CommandInput;
import models.CommandOutput;
import services.ICommandExecutor;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class QueryService implements ICommandExecutor {
    private final BookingService bookingService;

    public QueryService(BookingService bookingService) {
        this.bookingService = bookingService;
    }

    @Override
    public CommandOutput execute(CommandInput input) throws InvalidCommandException, InvalidArgumentException {
        switch (input.getCmd()) {
            case "booking_completed_distance_gt":
                if (input.getArgs().length == 0) {
                    throw new InvalidArgumentException("booking_completed_distance_gt takes 1 argument!!");
                }
                return queryByDistance(input.getArgs()[0]);
            case "drivers_completed_booking_gt":
                if (input.getArgs().length == 0) {
                    throw new InvalidArgumentException("drivers_completed_booking_gt takes 1 argument!!");
                }
                return queryByBookings(input.getArgs()[0]);
            default:
                return CommandOutput.builder()
                        .status(CommandOutput.Status.INVALID)
                        .msg(Optional.empty())
                        .build();
        }

    }

    private CommandOutput queryByBookings(String bookingCntStr) throws InvalidArgumentException {
        long bookingCnt;
        try {
            bookingCnt = Long.parseLong(bookingCntStr);
            if (bookingCnt < 1) {
                throw new Exception("Distance must be greater than '0'");
            }
        } catch (Exception exception) {
            throw new InvalidArgumentException(exception);
        }
        String msg = "Driver Completed\n" + bookingService.getBookings().stream()
                .filter(booking -> booking.getStatus() == Booking.Status.COMPLETED)
                .collect(Collectors.groupingBy(Booking::getDriver, Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() >= bookingCnt)
                .sorted((e1, e2) -> Long.compare(e2.getValue(), e1.getValue()))
                .map(entry -> entry.getKey().getId() + " " + entry.getValue())
                .collect(Collectors.joining("\n"));
        return CommandOutput.builder()
                .status(CommandOutput.Status.SUCCESSFUL)
                .msg(Optional.of(msg))
                .build();
    }

    private CommandOutput queryByDistance(String distanceStr) throws InvalidArgumentException {
        long distance;
        try {
            distance = Long.parseLong(distanceStr);
            if (distance < 1) {
                throw new Exception("Distance must be greater than '0'");
            }
        } catch (Exception exception) {
            throw new InvalidArgumentException(exception);
        }

        String msg = "Driver Completed\n" + bookingService.getBookings().stream()
                .filter(booking -> booking.getStatus() == Booking.Status.COMPLETED)
                .collect(Collectors.groupingBy(Booking::getDriver, Collectors.summingLong(Booking::getDistance)))
                .entrySet().stream()
                .filter(entry -> entry.getValue() >= distance)
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .map(entry -> entry.getKey().getId() + " " + entry.getValue())
                .collect(Collectors.joining("\n"));
        return CommandOutput.builder()
                .status(CommandOutput.Status.SUCCESSFUL)
                .msg(Optional.of(msg))
                .build();
    }
}
