import spock.lang.Specification

class Test extends Specification {
    def "test"() {
        when:
        def x = 1
        then:
        x == 1
    }
}
