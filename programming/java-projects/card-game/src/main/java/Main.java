import models.Deck;
import models.Game;
import service.IGameService;
import service.impl.GameService;
import service.impl.PlayerService;

public class Main {
    public static void main (String args[]) {
        IGameService gameService = new GameService(new PlayerService());
        Game game = new Game();
        while (gameService.notComplete(game)) {
            gameService.action(game);
        }
    }
}
