package models;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class Card implements Comparable<Card> {
    public enum Suit {
        CLUB, DIAMOND, HEARTS, SPADE;
        private static final Suit[] allValues = values();
        public static Suit fromOrdinal(int n) {return allValues[n];}
    }

    private final int no;
    private final Suit suit;

    public Card(int no, Suit suit) {
        this.no = no;
        this.suit = suit;
    }

    @Override
    public String toString() {
        String str;
        switch (no) {
            case 11:
                str = "J";
                break;
            case 12:
                str = "Q";
                break;
            case 13:
                str = "K";
                break;
            case 14:
                str = "A";
                break;
            default:
                str = Integer.toString(no);
        }
        return suit + " : " + str;
    }

    @Override
    public int compareTo(Card other) {
        return Integer.compare(no, other.no);
    }
}
