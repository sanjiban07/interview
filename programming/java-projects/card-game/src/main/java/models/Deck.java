package models;

import lombok.Data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Data
public class Deck {
    private List<Card> cards;
    public Deck() {
        cards = new ArrayList<>();
        for (int i = 0; i <= 3; i++) {
            Card.Suit suit = Card.Suit.fromOrdinal(i);
            for (int no = 2; no <= 14; no++) {
                cards.add(new Card(no, suit));
            }
        }
    }

    public List<Card> shuffle() {
        Collections.shuffle(cards);
        return cards;
    }
}
