package models;

import lombok.Data;

@Data
public class Game {
    private final int[] wins;
    private int roundNo;
    private final Hand[] hand;
    private int currentPlayer;
    private Card.Suit trump;
//    private final Round[] rounds;
    public Game() {
        wins = new int[4];
        roundNo = 0;
        currentPlayer = -1;
        trump = null;
        hand = new Hand[4];
//        rounds = new Round[13];
    }

//    @Override
//    public String toString() {
//        StringBuilder builder = new StringBuilder();
//        builder.append("Game\n");
//
//    }
}
