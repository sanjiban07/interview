package models;

import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.stream.Collectors;

@Data
public class Hand {
    private final Map<Card.Suit, List<Card>> suitWiseCards;

    public Hand() {
        suitWiseCards = new HashMap<>();
        suitWiseCards.put(Card.Suit.CLUB, new ArrayList<>());
        suitWiseCards.put(Card.Suit.DIAMOND, new ArrayList<>());
        suitWiseCards.put(Card.Suit.HEARTS, new ArrayList<>());
        suitWiseCards.put(Card.Suit.SPADE, new ArrayList<>());
    }

    public void sort() {
        for (Card.Suit suit: suitWiseCards.keySet()) {
            suitWiseCards.put(suit, suitWiseCards.get(suit).stream().sorted().collect(Collectors.toList()));
        }
    }

    public void add(Card card) {
        suitWiseCards.get(card.getSuit()).add(card);
    }

    public void remove(Card card) {
        suitWiseCards.get(card.getSuit()).remove(card);
    }
}
