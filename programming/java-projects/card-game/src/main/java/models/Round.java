package models;

import lombok.Data;

@Data
public class Round {
    private final int playerMap[];
    private final Card cardsPlayed[];
    private final Card.Suit trump;

    public Round(Card.Suit trump) {
        this.trump = trump;
        playerMap = new int[4];
        cardsPlayed = new Card[4];
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Trump: " + trump);
        for (int i = 0; i < 4; i++) {
            builder.append("\nPlayer No: " + playerMap[i] + ", Card played: " + cardsPlayed[i]);
        }

        return builder.toString();
    }
}
