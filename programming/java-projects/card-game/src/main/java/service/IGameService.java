package service;

import models.Game;

public interface IGameService {
    void action(Game game);

    boolean notComplete(Game game);
}
