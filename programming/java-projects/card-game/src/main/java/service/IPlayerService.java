package service;

import models.Card;
import models.Hand;
import models.Round;

public interface IPlayerService {
    Card play(int playerId, Round round, Hand hand);

    int getWinner(Round round);
}
