package service.impl;

import models.*;
import service.IGameService;
import service.IPlayerService;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class GameService implements IGameService {
    private final IPlayerService playerService;
    private final Random random;
    private final Deck deck;
    public GameService(IPlayerService playerService) {
        this.playerService = playerService;
        random = new Random();
        deck = new Deck();
    }

    @Override
    public void action(Game game) {
        if (game.getRoundNo() == 0) {
            initializeGame(game);
        } else if (game.getRoundNo() < 14) {
            playRound(game);
        } else {
            printGameWinners(game);
        }
    }

    @Override
    public boolean notComplete(Game game) {
        return game.getRoundNo() < 14;
    }

    private void printGameWinners(Game game) {
        System.out.println("Result");
        int winner = 0, max = -1;
        for (int i = 0; i < 4; i++) {
            System.out.println("Player No. " + i + ". Wins: " + game.getWins()[i]);
            if (max < game.getWins()[i]) {
                max = game.getWins()[i];
                winner = i;
            }
        }
        System.out.println("Player No. 0. Wins: " + game.getWins()[0]);
        System.out.println("Winner: player no. " + winner);
    }

    private void playRound(Game game) {
        Round round = new Round(game.getTrump());
        for (int i = 0; i < 4; i++) {
            Card card = playerService.play(i, round, game.getHand()[game.getCurrentPlayer()]);
            game.getHand()[game.getCurrentPlayer()].remove(card);
            round.getPlayerMap()[i] = game.getCurrentPlayer();
            round.getCardsPlayed()[i] = card;
            game.setCurrentPlayer((game.getCurrentPlayer() + 1) % 4);
        }

        System.out.println("Round No: " + game.getRoundNo());
        System.out.println(round);
        int winner = round.getPlayerMap()[playerService.getWinner(round)];
        game.getWins()[winner] = game.getWins()[winner] + 1;
        System.out.println("Winner player no.: " + winner);
        System.out.println("Wins: " + Arrays.toString(game.getWins()));
        game.setCurrentPlayer(winner);
        game.setRoundNo(game.getRoundNo() + 1);
    }

    private void initializeGame(Game game) {
        game.setCurrentPlayer(random.nextInt(4));
        List<Card> shuffledCards = deck.shuffle();
        for (int i = 0; i < 4; i++) {
            game.getHand()[i] = new Hand();
            for (int j = 0; j < 13; j++) {
                game.getHand()[i].add(shuffledCards.get(i * 13 + j));
            }
            game.getHand()[i].sort();
            System.out.println("Player no '" + i + "'. Hand: " + game.getHand()[i]);
        }
        game.setRoundNo(1);
        game.setTrump(Card.Suit.fromOrdinal(random.nextInt(4)));
    }
}
