package service.impl;

import models.Card;
import models.Hand;
import models.Round;
import service.IPlayerService;

import java.util.List;

public class PlayerService implements IPlayerService {
    @Override
    public Card play(int turn, Round round, Hand hand) {
        if (turn == 0) {
            return getBestCard(hand);
        }

        int winner = getWinner(round);
        Card winCard = round.getCardsPlayed()[winner];
        boolean isTrumped = winCard.getSuit() == round.getTrump();
        Card.Suit color = round.getCardsPlayed()[0].getSuit();
        if (hasColor(color, hand)) {
            if (isTrumped && round.getTrump() != color) {
                return getWorstCard(hand, color);
            }

            Card bestCard = getBestCard(hand, color);
            if (bestCard == max(bestCard, winCard, round.getTrump(), color)) {
                return bestCard;
            } else {
                return getWorstCard(hand, color);
            }
        } else {
            Card bestTrump = getBestCard(hand, round.getTrump());
            if (bestTrump != null && bestTrump == max(bestTrump, winCard, round.getTrump(), color)) {
                return bestTrump;
            }
            return getWorstCardExcept(hand, round.getTrump());
        }
    }

    private boolean hasColor(Card.Suit color, Hand hand) {
        return hand.getSuitWiseCards().get(color).size() > 0;
    }

    public int getWinner(Round round) {
        int winner = 0;
        Card winCard = round.getCardsPlayed()[0];
        Card.Suit color = round.getCardsPlayed()[0].getSuit();
        for (int i = 1; i < 4; i++) {
            Card cardPlayed = round.getCardsPlayed()[i];
            if (cardPlayed == null) break;
            if (cardPlayed == max(winCard, cardPlayed, round.getTrump(), color)) {
                winCard = cardPlayed;
                winner = i;
            }
        }
        return winner;
    }

    private Card max(Card card1, Card card2, Card.Suit trump, Card.Suit color) {
        try {
            if (card1.getSuit() == trump && card2.getSuit() == trump) {
                return card1.compareTo(card2) > 0 ? card1 : card2;
            } else if (card1.getSuit() == trump) {
                return card1;
            } else if (card2.getSuit() == trump) {
                return card2;
            } else {
                if (card1.getSuit() == color && card2.getSuit() == color) {
                    return card1.compareTo(card2) > 0 ? card1 : card2;
                } else if (card1.getSuit() == color) {
                    return card1;
                } else if (card2.getSuit() == color) {
                    return card2;
                } else {
                    return card1.compareTo(card2) > 0 ? card1 : card2;
                }
            }
        } catch (Exception exception) {
            throw exception;
        }
    }

    Card getBestCard(Hand hand, Card.Suit suit) {
        List<Card> colorCords = hand.getSuitWiseCards().get(suit);
        return colorCords.size() == 0? null : colorCords.get(colorCords.size() - 1);
    }

    Card getWorstCard(Hand hand, Card.Suit suit) {
        List<Card> colorCords = hand.getSuitWiseCards().get(suit);
        return colorCords.size() == 0? null : colorCords.get(0);
    }

    Card getBestCard(Hand hand) {
        return hand.getSuitWiseCards().entrySet().stream()
                .filter(map -> map.getValue().size() > 0)
                .map(map -> {
                    int N = map.getValue().size();
                    return map.getValue().get(N - 1);
                }).max(Card::compareTo).orElse(null);
    }

    Card getWorstCardExcept(Hand hand, Card.Suit trump) {
        Card min = hand.getSuitWiseCards().entrySet().stream()
                .filter(map -> map.getValue().size() > 0)
                .filter(entry -> entry.getKey() != trump)
                .map(map -> map.getValue().get(0))
                .min(Card::compareTo).orElse(null);
        if (min != null) {
            return min;
        }

        return hand.getSuitWiseCards().entrySet().stream()
                .filter(entry -> entry.getKey() == trump)
                .map(map -> map.getValue().get(0))
                .min(Card::compareTo).get();
    }
}
