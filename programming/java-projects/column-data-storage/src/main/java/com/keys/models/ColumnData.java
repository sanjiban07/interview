package com.keys.models;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class ColumnData {
    private final String name;
    private final Class type;
    private Map<String, Object> dataMap;

    public ColumnData(String name, Class type) {
        this.name = name;
        this.type = type;
        this.dataMap = new HashMap<>();
    }

    public boolean isEmpty() {
        return this.dataMap.isEmpty();
    }
}
