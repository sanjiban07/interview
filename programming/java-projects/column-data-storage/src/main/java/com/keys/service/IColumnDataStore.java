package com.keys.service;

import com.keys.models.Attribute;

import java.util.List;

public interface IColumnDataStore {
    boolean exists(String key);

    List<Attribute> get(String key);

    boolean put(String key, Attribute attribute);

    List<String> find(String column, Object value);

    boolean remove(String key);

    boolean isInvalid(List<Attribute> attributes);
}
