package com.keys.service.impl;

import com.keys.models.Attribute;
import com.keys.models.ColumnData;
import com.keys.service.IColumnDataStore;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ColumnDataStore implements IColumnDataStore {
    private final List<ColumnData> columnDataList;
    public ColumnDataStore() {
        columnDataList = new ArrayList<>();
    }

    @Override
    public boolean isInvalid(List<Attribute> attributes) {
        for (Attribute attribute: attributes) {
            if (isInvalid(attribute)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean exists(String key) {
        return columnDataList.stream()
                .filter(columnData -> columnData.getDataMap().containsKey(key))
                .findAny().isPresent();
    }

    @Override
    public List<Attribute> get(String key) {
        return columnDataList.stream()
                .map(columnData -> getAttribute(key, columnData))
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    @Override
    public boolean put(String key, Attribute attribute) {
        Optional<ColumnData> existingColumn = getColumn(attribute.getName());
        if (existingColumn.isPresent() && attribute.getValue().getClass() != existingColumn.get().getType()) {
            return false;
        }

        ColumnData columnData = existingColumn.isPresent()? existingColumn.get()
                : new ColumnData(attribute.getName(), attribute.getValue().getClass());
        columnData.getDataMap().put(key, attribute.getValue());

        if (!existingColumn.isPresent()) {
            columnDataList.add(columnData);
        }

        return true;
    }

    @Override
    public List<String> find(String column, Object value) {
        Optional<ColumnData> existingColumn = getColumn(column);
        if (!existingColumn.isPresent()) {
            return new ArrayList<>();
        }

        return existingColumn.get().getDataMap().entrySet().stream()
                .filter(entry -> entry.getValue().equals(value))
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    @Override
    public boolean remove(String key) {
        boolean exists = false;
        List<ColumnData> emptyColumns = new ArrayList<>();
        for (ColumnData columnData : columnDataList) {
            if (columnData.getDataMap().containsKey(key)) {
                columnData.getDataMap().remove(key);
                exists = true;
            }

            if (columnData.isEmpty()) {
                emptyColumns.add(columnData);
            }
        }

        columnDataList.removeAll(emptyColumns);
        return exists;
    }

    private Optional<ColumnData> getColumn(String column) {
        return columnDataList.stream().filter(columnData -> columnData.getName().equalsIgnoreCase(column)).findAny();
    }

    private Optional<Attribute> getAttribute(String key, ColumnData columnData) {
        Object value = columnData.getDataMap().getOrDefault(key, null);
        if (value == null) {
            return Optional.empty();
        }

        return Optional.of(new Attribute(columnData.getName(), value));
    }

    private boolean isInvalid(Attribute attribute) {
        Class type = attribute.getValue().getClass();
        Optional<ColumnData> columnData = getColumn(attribute.getName());
        return columnData.isPresent() && type != columnData.get().getType();
    }
}