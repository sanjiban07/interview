package com.keys.service.impl;

import com.keys.models.Attribute;
import com.keys.service.IKeyValueStore;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class KeyValueStore implements IKeyValueStore {
    private final ColumnDataStore columnDataStore;
    public KeyValueStore() {
        columnDataStore = new ColumnDataStore();
    }

    @Override
    public boolean store(String key, List<Attribute> attributes) {
        if (columnDataStore.exists(key) || columnDataStore.isInvalid(attributes)) {
            return false;
        }

        return createOrUpdate(key, attributes);
    }

    @Override
    public List<Attribute> get(String key) {
        return columnDataStore.get(key);
    }

    @Override
    public boolean update(String key, List<Attribute> attributes) {
        if (columnDataStore.isInvalid(attributes)) {
            return false;
        }

        return createOrUpdate(key, attributes);
    }

    @Override
    public boolean delete(String key) {
        return columnDataStore.remove(key);
    }

    @Override
    public Map<String, List<Attribute>> getSecondaryIndex(String predicateKey, Object predicateValue) {
        return columnDataStore.find(predicateKey, predicateValue).stream()
                .collect(Collectors.toMap(key -> key, key -> columnDataStore.get(key)));
    }

    private boolean createOrUpdate(String key, List<Attribute> attributes) {
        for (Attribute attribute: attributes) {
            if (!columnDataStore.put(key, attribute)) {
                return false;
            }
        }

        return true;
    }
}
