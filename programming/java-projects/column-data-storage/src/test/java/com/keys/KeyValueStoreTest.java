package com.keys;

import com.google.common.collect.Lists;
import com.keys.models.Attribute;
import com.keys.service.IKeyValueStore;
import com.keys.service.impl.KeyValueStore;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class KeyValueStoreTest {
    private IKeyValueStore kvStore = new KeyValueStore();

    private Random random = new Random();

    @Test
    void testAddKeys() {
        String key = "key1";
        List<Attribute> attributes = Lists.newArrayList(createBoolean("is_hungry"), createNumber("latitude"), createString("capital"));
        assertTrue(kvStore.store(key, attributes));
    }

    @Test
    void testAddKeysFailOnDuplicate() {
        String key = "key1";
        List<Attribute> attributes = Lists.newArrayList(createBoolean("is_hungry"), createNumber("latitude"), createString("capital"));
        assertTrue(kvStore.store(key, attributes));
        assertFalse(kvStore.store(key, attributes));
    }

    @Test
    void testAddIncorrectDataType() {
        String key1 = "key1";
        List<Attribute> attributes1 = Lists.newArrayList(createBoolean("is_hungry"), createNumber("latitude"), createString("capital"));
        String key2 = "key2";
        List<Attribute> attributes2 = Lists.newArrayList(createString("is_hungry"));

        assertTrue(kvStore.store(key1, attributes1));
        if (attributes1.get(0).getValue().getClass().equals(attributes2.get(0).getValue().getClass()))
            assertTrue(kvStore.store(key2, attributes2));
        else
            assertFalse(kvStore.store(key2, attributes2));
    }

    @Test
    void fetchKeyExists() {
        String key1 = "key1";
        List<Attribute> attributes1 = Lists.newArrayList(createBoolean("is_hungry"), createNumber("latitude"), createString("capital"));
        assertTrue(kvStore.store(key1, attributes1));
        final List<Attribute> actualAttributes = kvStore.get(key1);
        assertEquals(new HashSet<>(attributes1), new HashSet<>(actualAttributes));

    }

    @Test
    void fetchKeyDoesntExist() {
        String key1 = "key1";
        List<Attribute> value = kvStore.get(key1);
        assertEquals(0, value.size());
//        fail("Key doesn't exist");
    }

    @Test
    void mergeKeys() {
        String key = "key1";
        final Attribute hungry1 = createBoolean("is_hungry");
        final Attribute latitude = createNumber("latitude");
        final Attribute capital = createString("capital");
        final Attribute state = createString("state");
        hungry1.setValue(true);
        final Attribute hungry2 = createBoolean("is_hungry");
        hungry2.setValue(false);
        List<Attribute> attributes = Lists.newArrayList(hungry1, latitude, capital);
        List<Attribute> updateAttributes = Lists.newArrayList(hungry2, state);
        List<Attribute> mergedAttributes = Lists.newArrayList(capital, latitude, hungry2, state);
        assertTrue(kvStore.store(key, attributes));
        final List<Attribute> actualAttributes = kvStore.get(key);
        assertEquals(new HashSet<>(actualAttributes), new HashSet<>(attributes));
        assertTrue(kvStore.update(key, updateAttributes));
        //TODO: Fix for any ordering issues when comparing attributes.
        assertEquals(new HashSet<>(mergedAttributes), new HashSet<>(kvStore.get(key)));
    }

    @Test
    void deleteKeyExists() {
        String key1 = "key1";
        List<Attribute> attributes1 = Lists.newArrayList(createBoolean("is_hungry"), createNumber("latitude"), createString("capital"));
        assertTrue(kvStore.store(key1, attributes1));
        final List<Attribute> actualAttributes = kvStore.get(key1);
        assertEquals(new HashSet<>(attributes1), new HashSet<>(actualAttributes));
        assertTrue(kvStore.delete(key1));
    }

    @Test
    void deleteKeyDoesntExist() {
        String key1 = "key1";
        assertFalse(kvStore.delete(key1));
    }

    @Test
    void testSecondaryIndex() {
        String key1 = "key1";
        final Attribute valid = createBoolean("is_valid");
        final Attribute longitude = createNumber("longitude");
        final Attribute capital = createString("capital");
        final Attribute state = createString("state");
        String key2 = "key2";

        kvStore.store(key1, Lists.newArrayList(valid, longitude, capital));
        kvStore.store(key2, Lists.newArrayList(valid, capital, state));
        final Map<String, List<Attribute>> secondaryMap = kvStore.getSecondaryIndex("is_valid", true);
        if (Boolean.TRUE.equals(valid.getValue()))
            assertEquals(2, secondaryMap.size());
        else
            assertEquals(0, secondaryMap.size());

    }

    Attribute createBoolean(String name) {
        List<Object> values = Lists.newArrayList("true", Boolean.TRUE, true, false, "false");
        return new Attribute(name, values.get(random.nextInt(values.size())));
    }

    Attribute createString(String name) {
        List<Object> values = Lists.newArrayList("delhi", "snow", "flake", "db");
        return new Attribute(name, values.get(random.nextInt(values.size())));
    }

    Attribute createNumber(String name) {
        List<Object> values = Lists.newArrayList(11.12, 23.12, 119.12, -123.423);
        return new Attribute(name, values.get(random.nextInt(values.size())));
    }
}
