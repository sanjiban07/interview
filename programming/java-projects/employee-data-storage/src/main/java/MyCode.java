import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import models.Compensation;
import models.EmployeeData;
import services.EmployeeDataService;
import services.ReportService;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class MyCode {
    public static void main (String[] args) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        // String sampleInput = "[{\"id\":\"583fd999a2a3a4121dd6b6fc\",\"name\":\"BrookQuigley\",\"employmentType\":\"HOURLY_FT\",\"dob\":\"1993-04-31\",\"identifiedGender\":\"FEMALE\",\"title\":\"SalesDevelopmentRep\",\"compensation\":{\"currency\":\"USD\",\"annualSalary\":65000.00},\"department\":\"Sales\",\"workLocation\":\"SanFrancisco\",\"startDate\":\"2020-05-31\"},{\"id\":\"583fd982a2a3a4121dd6b429\",\"name\":\"JohnGrady\",\"employmentType\":\"HOURLY_FT\",\"dob\":\"1996-05-31\",\"identifiedGender\":\"MALE\",\"title\":\"CustomerSupportAssociate\",\"compensation\":{\"currency\":\"USD\",\"annualSalary\":52000.00},\"department\":\"CustomerSupport\",\"workLocation\":\"SanFrancisco\",\"startDate\":\"2019-05-31\"},{\"id\":\"583fd982a2a3a4121dd6b430\",\"name\":\"JackBrady\",\"employmentType\":\"HOURLY_FT\",\"dob\":\"1996-07-31\",\"identifiedGender\":\"MALE\",\"title\":\"CustomerSupportAssociate\",\"compensation\":{\"currency\":\"USD\",\"annualSalary\":48000.00},\"department\":\"CustomerSupport\",\"workLocation\":\"SanFrancisco\",\"startDate\":\"2019-07-31\"}]";
        String sampleInput = "[{\"id\":\"583fd999a2a3a4121dd6b6fc\",\"name\":\"BrookQuigley\",\"employmentType\":\"HOURLY_FT\",\"dob\":\"1993-04-31\",\"identifiedGender\":\"FEMALE\",\"title\":\"SalesDevelopmentRep\",\"compensation\":{\"currency\":\"USD\",\"annualSalary\":65000.00},\"department\":\"Sales\",\"workLocation\":\"SanFrancisco\",\"startDate\":\"2020-05-31\"},{\"id\":\"583fd982a2a3a4121dd6b429\",\"name\":\"JohnGrady\",\"employmentType\":\"HOURLY_FT\",\"dob\":\"1996-05-31\",\"identifiedGender\":\"MALE\",\"title\":\"CustomerSupportAssociate\",\"compensation\":{\"currency\":\"USD\",\"annualSalary\":52000.00},\"department\":\"CustomerSupport\",\"workLocation\":\"SanFrancisco\",\"startDate\":\"2019-05-31\"},{\"id\":\"583fd982a2a3a4121dd6b430\",\"name\":\"JackBrady\",\"employmentType\":\"HOURLY_FT\",\"dob\":\"1996-07-31\",\"identifiedGender\":\"MALE\",\"title\":\"CustomerSupportAssociate\",\"compensation\":{\"currency\":\"USD\",\"annualSalary\":48000.00},\"department\":\"CustomerSupport\",\"workLocation\":\"SanFrancisco\",\"startDate\":\"2019-07-31\"}]";
        EmployeeDataService employeeDataService = new EmployeeDataService();
        employeeDataService.setEmployeeData(Arrays.asList(
                EmployeeData.builder()
                        .id("583fd999a2a3a4121dd6b6fc")
                        .department("Sales")
                        .identifiedGender("MALE")
                        .compensation(Compensation.builder().annualSalary(65000L).build())
                        .build(),
                EmployeeData.builder()
                        .id("583fd982a2a3a4121dd6b429")
                        .department("Sales")
                        .identifiedGender("FEMALE")
                        .compensation(Compensation.builder().annualSalary(52000L).build())
                        .build()
        ));
        ReportService reportService = new ReportService();
        Map<String, Long> report = reportService.group(employeeDataService.getEmployeeData(), "identifiedGender");
        System.out.println(mapper.writeValueAsString(report));
        List<EmployeeData> filteredData = reportService.filterExclude(employeeDataService.getEmployeeData(), "identifiedGender", "MALE");
        report = reportService.group(filteredData, "identifiedGender");
        System.out.println(mapper.writeValueAsString(report));
    }
}
