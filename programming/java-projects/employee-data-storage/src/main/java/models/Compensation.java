package models;

import lombok.Builder;
import lombok.Data;

import java.util.PriorityQueue;

@Data
@Builder
public class Compensation {
    public String  currency;
    public Long  annualSalary;
    PriorityQueue pqueue;
}
