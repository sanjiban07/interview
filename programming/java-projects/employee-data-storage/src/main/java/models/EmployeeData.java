package models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class EmployeeData {
    public String id;
    public String name;
    public String employmentType;
    public String dob;
    public String identifiedGender;
    public String title;
    public String department;
    public String workLocation;
    public String startDate;
    public Compensation compensation;
}
