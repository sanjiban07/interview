package services;


import lombok.Data;
import models.EmployeeData;

import java.util.List;

@Data
public class EmployeeDataService {
    private List<EmployeeData> employeeData;
}
