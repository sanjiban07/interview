package services;

import models.EmployeeData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ReportService {
    public List<EmployeeData> filterExclude(List<EmployeeData> employeeDataList, String field, String value) {
        return employeeDataList.stream().filter(data -> !isMatch(data, field, value)).collect(Collectors.toList());
    }

    public Map<String, Long> group(List<EmployeeData> employeeDataList, String field) {
        if (employeeDataList == null) {
            return null;
        }
        Map<String, Long> fieldwiseSalary = new HashMap<>();
        for(EmployeeData employeeData : employeeDataList) {
            try {
                String value = getValue(employeeData, field);
                fieldwiseSalary.put(value, fieldwiseSalary.getOrDefault(value, 0L) + employeeData.getCompensation().getAnnualSalary());
            } catch (Exception exception) {
                continue;
            }
        }
        return fieldwiseSalary;
    }

    boolean isMatch(EmployeeData data, String field, String value) {
        try {
            return getValue(data, field).equalsIgnoreCase(value);
        } catch (Exception exception) {
            return false;
        }
    }

    String getValue(EmployeeData data, String field) throws NoSuchFieldException, IllegalAccessException {
        return data.getClass().getField(field).get(data).toString();
    }
}
