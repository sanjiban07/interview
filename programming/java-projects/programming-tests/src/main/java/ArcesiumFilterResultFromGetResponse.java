import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ArcesiumFilterResultFromGetResponse {
    private static List<Record> records = null;
    abstract static class Base {
        public String getOrDefault(String fieldName, String defaultValue) {
            String[] fields = fieldName.split("\\.", 2);
            try {
                if (fields.length == 1) {
                    return this.getClass().getDeclaredField(fieldName).get(this).toString();
                } else {
                    Base child = (Base) this.getClass().getDeclaredField(fields[0]).get(this);
                    return child.getOrDefault(fields[1], defaultValue);
                }

            } catch (Exception exception) {
                return defaultValue;
            }
        }
    }

    static class Record extends Base {
        public String id;
        public String name;
        public String username;
        public String email;
        public Address address;
        public String website;
        public Company company;
    }
    static class Address extends Base {
        public String street;
        public String suite;
        public String city;
        public String zipcode;
        public Geo geo;
    }
    static class Geo extends Base {
        public String lat;
        public String lng;
    }
    static class Company extends Base {
        public String name;
        public String basename;
    }
    /*
     * Complete the 'apiResponseParser' function below.
     *
     * The function is expected to return an INTEGER_ARRAY.
     * The function accepts following parameters:
     *  1. STRING_ARRAY inputList
     *  2. INTEGER size
     */

    public static List<Integer> apiResponseParser(List<String> inputList, int size) {
        if (records == null) {
            populateRecords();
        }

        String operation = inputList.get(1);
        String[] queryValues;
        if (operation.equalsIgnoreCase("EQUALS")) {
            queryValues = new String[]{ inputList.get(2) };
        } else if (operation.equalsIgnoreCase("IN")) {
            queryValues = inputList.get(2).split(",");
        } else {
            return Arrays.asList(-1);
        }
        List<Integer> matching = records.stream().filter(record ->
                anyMatch(record, inputList.get(0), queryValues)
        ).map(record -> Integer.parseInt(record.getOrDefault("id", "-1")))
                .collect(Collectors.toList());
        return matching.size() > 0? matching : Arrays.asList(-1);
    }

    private static boolean anyMatch(Record record, String fieldName, String[] queryValues) {
        String value = record.getOrDefault(fieldName, null);
        if (value == null) {
            return false;
        }

        return Arrays.stream(queryValues).filter(str -> str.equalsIgnoreCase(value)).findAny().isPresent();
    }

    private static void populateRecords() {
        try {
            populateRecordsFromUrl();
        } catch (Exception exception) {
            System.out.println("Exception: " + exception);
            String input = "[ { \"id\": 1, \"name\": \"Vinay Kumar\", \"username\": \"vinayk\", \"email\": \"vinayk@abcu.com\", \"address\": { \"street\": \"random1\", \"suite\": \"APR\", \"city\": \"Mumbai\", \"zipcode\": \"192008-13874\", \"geo\": { \"lat\": \"-17.3159\", \"lng\": \"91.1496\" } }, \"website\": \"seuinfra.org\", \"company\": { \"name\": \"sec infra\", \"basename\": \"seu infra tech\" } }, { \"id\": 2, \"name\": \"Anandita Basu\", \"username\": \"PrernaB\", \"email\": \"Anandita.b@abc1f.cpm\", \"address\": { \"street\": \"Hawroh Bridge\", \"suite\": \"ATY\", \"city\": \"Kolkata\", \"zipcode\": \"700001\", \"geo\": { \"lat\": \"-67.3159\", \"lng\": \"91.8006\" } }, \"website\": \"techInfar.org\", \"company\": { \"name\": \"tech infar world\", \"basename\": \"seu infra tech\" } }, { \"id\": 3, \"name\": \"Charvi Malhotra\", \"username\": \"CharviM\", \"email\": \"Charvim@mail.net\", \"address\": { \"street\": \"whitehouse Extension\", \"suite\": \"A782\", \"city\": \"Bengaluru\", \"zipcode\": \"560001\", \"geo\": { \"lat\": \"-68.6102\", \"lng\": \"-47.0653\" } }, \"website\": \"Infesystem.info\", \"company\": { \"name\": \"infeystems\", \"basename\": \"Information E stsyem\" } }, { \"id\": 4, \"name\": \"Patricia Wilson\", \"username\": \"WilsonP\", \"email\": \"Wilsonp@mymail.org\", \"address\": { \"street\": \"Kalangut\", \"suite\": \"Apt 6\", \"city\": \"Panjim\", \"zipcode\": \"403110\", \"geo\": { \"lat\": \"29.4572\", \"lng\": \"-164.2990\" } }, \"website\": \"giant.Tech.biz\", \"company\": { \"name\": \"robert-techgiant\", \"basename\": \"transition cutting-edge web services provider\" } }, { \"id\": 5, \"name\": \"Chetan Chauhan \", \"username\": \"ChauhanChetan\", \"email\": \"chetanc@mailme.in\", \"address\": { \"street\": \"Willow Walks\", \"suite\": \"1351\", \"city\": \"Hyderabad\", \"zipcode\": \"500001\", \"geo\": { \"lat\": \"-31.8129\", \"lng\": \"62.5342\" } }, \"website\": \"sanganak.info\", \"company\": { \"name\": \"Sanganak\", \"basename\": \"end-to-end solution provider\" } }, { \"id\": 6, \"name\": \"Pragya Mathur\", \"username\": \"mathurpragya\", \"email\": \"pragya.mathur@mail.in\", \"address\": { \"street\": \"Rosewind Crossing\", \"suite\": \"A-50\", \"city\": \"Delhi\", \"zipcode\": \"100001\", \"geo\": { \"lat\": \"-71.4197\", \"lng\": \"71.7478\" } }, \"website\": \"hola.in\", \"company\": { \"name\": \"Hola Technocrafts\", \"basename\": \"e-enable innovative applications\" } }, { \"id\": 7, \"name\": \"Krish Ahuja\", \"username\": \"ahujakrish\", \"email\": \"ahujakrish@mails.in\", \"address\": { \"street\": \"Havmore Extension\", \"suite\": \"A3221\", \"city\": \"Bengalura\", \"zipcode\": \"560058\", \"geo\": { \"lat\": \"24.8918\", \"lng\": \"21.8984\" } }, \"website\": \"tellybelly.in\", \"company\": { \"name\": \"Telly Belly\", \"basename\": \"generate application support solutions\" } }, { \"id\": 8, \"name\": \"Nilofar Anam\", \"username\": \"anamnilofar\", \"email\": \"nilofaranam.d@maily.me\", \"address\": { \"street\": \"fountains lane\", \"suite\": \"B902\", \"city\": \"pune\", \"zipcode\": \"400001\", \"geo\": { \"lat\": \"-14.3990\", \"lng\": \"-120.7677\" } }, \"website\": \"techoba.com\", \"company\": { \"name\": \"Tech Happy Group\", \"basename\": \"e-support to middle retailers\" } }, { \"id\": 9, \"name\": \"Garima Gupta\", \"username\": \"Garimag\", \"email\": \"gupta.garima22@myemails.io\", \"address\": { \"street\": \"Little Park\", \"suite\": \"B68\", \"city\": \"Surat\", \"zipcode\": \"764920\", \"geo\": { \"lat\": \"24.6463\", \"lng\": \"-168.8889\" } }, \"website\": \"contech.com\", \"company\": { \"name\": \"Configs Techs\", \"basename\": \"real-time technologies support\" } }, { \"id\": 10, \"name\": \"Dharma Dhar\", \"username\": \"Dharmadhar55\", \"email\": \"dharmadhar55@olexa.in\", \"address\": { \"street\": \"Anam Street\", \"suite\": \"198/23\", \"city\": \"Surat\", \"zipcode\": \"314280\", \"geo\": { \"lat\": \"-38.2386\", \"lng\": \"57.2232\" } }, \"website\": \"ampitech.net\", \"company\": { \"name\": \"Ampitech Solutions Ltd\", \"basename\": \"target end-to-end startup support\" } } ]";
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<Record>>(){}.getType();
            records = gson.fromJson(input, listType);
        }
    }
    private static void populateRecordsFromUrl() throws Exception {
        String urlString = "https://raw.githubusercontent.com/arcjsonapi/ApiSampleData/master/api/users";
        URL url = new URL(urlString);
        InputStream inputStream = url.openConnection().getInputStream();
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<Record>>(){}.getType();
        records = gson.fromJson(new BufferedReader(new InputStreamReader(inputStream)), listType);
    }
}
