import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/*
We are writing a tool to help users manage their calendars. Given an unordered list of times of day when someone is busy, write a function that tells us whether they're available during a specified period of time.

Each time is expressed as an integer using 24-hour notation, such as 1200 (12:00), 1530 (15:30), or 800 (8:00).

Sample input:

meetings = [
  [1230, 1300], // 12:30 PM to 1:00 PM
  [845, 900],   //  8:45 AM to 9:00 AM
  [1300, 1500]  //  1:00 PM to 3:00 PM
]

Expected output:

isAvailable(meetings, 915, 1215)   => true
isAvailable(meetings, 900, 1230)   => true
isAvailable(meetings, 850, 1240)   => false
isAvailable(meetings, 1200, 1300)  => false
isAvailable(meetings, 700, 1600)   => false
isAvailable(meetings, 800, 845)    => true
isAvailable(meetings, 1500, 1800)  => true
isAvailable(meetings, 845, 859)    => false
isAvailable(meetings, 846, 900)    => false
isAvailable(meetings, 846, 859)    => false
isAvailable(meetings, 845, 900)    => false
isAvailable(meetings, 2359, 2400)  => true
isAvailable(meetings, 930, 1600)   => false
isAvailable(meetings, 800, 850)    => false
isAvailable(meetings, 1400, 1600)  => false
isAvailable(meetings, 1300, 1501)  => false

Complexity Analysis:

n = number of meetings
r = total minutes in range of all meetings
*/
class Slot implements Comparable<Slot> {
    int startTime;
    int endTime;
    public Slot(int startTime, int endTime) {
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int compareTo(Slot slot) {
        if (slot.endTime <= this.startTime || slot.startTime >= this.endTime) {
            if (slot.startTime  == this.startTime) {
                return Integer.compare(this.endTime, slot.endTime);
            }
            return Integer.compare(this.startTime, slot.startTime);
        }
        return 0;
    }
}
class ScheduleMeeting {
    public static boolean isAvailable(int[][] meetings, int startTime, int endTime) {
        List<Slot> slots = getSortedSlots(meetings);
        return !isCollidingWithExistingSLots(slots, 0, meetings.length - 1, new Slot(startTime, endTime));
    }

    private static boolean isCollidingWithExistingSLots(List<Slot> slots, int start, int end, Slot slot) {
        if (start > end) return false;
        int mid = (end - start)/2 + start;
        Slot midSlot = slots.get(mid);
        if (midSlot.compareTo(slot) == 0) {
            return true;
        } else if (midSlot.compareTo(slot) < 0) {
            return isCollidingWithExistingSLots(slots, mid + 1, end, slot);
        } else {
            return isCollidingWithExistingSLots(slots, start, mid - 1, slot);
        }
    }

    private static List<Slot> getSortedSlots(int[][] meetings) {
        return Arrays.stream(meetings).map(
                m -> new Slot(m[0], m[1])
        ).sorted().collect(Collectors.toList());
    }
}
