import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
class Shift implements Comparable<Shift> {
    private long startTs, endTs;

    @Override
    public int compareTo(Shift other) {
        return startTs == other.getStartTs()?
            Long.compare(endTs, other.getEndTs()) : Long.compare(startTs, other.getStartTs());
    }

    public boolean tryMerge(Shift other) {
        if (other.startTs <= endTs) {
            endTs = Math.max(endTs, other.endTs);
            return true;
        }
        return false;
    }
}

@Data
class EmployeeData {
    private final String name;
    private final List<Shift> shifts;

    EmployeeData(String name) {
        this.name = name;
        this.shifts = new ArrayList<>();
    }
}

class DataStoreService {
    private final Map<String, EmployeeData> dataStore;
    public DataStoreService() {
        dataStore = new HashMap<>();
    }

    public void add(EmployeeData employeeData) {
        dataStore.put(employeeData.getName(), employeeData);
    }

    public EmployeeData get(String employee, EmployeeData defaultData) {
        return dataStore.getOrDefault(employee, defaultData);
    }
}

class ShiftService {
    private final DataStoreService dataStoreService;

    ShiftService(DataStoreService dataStoreService) {
        this.dataStoreService = dataStoreService;
    }

    public List<Shift> getCombinedShifts(String employee) {
        List<Shift> combinedShifts = new ArrayList<>();
        EmployeeData employeeData = dataStoreService.get(employee, null);
        if (employeeData == null || employeeData.getShifts() == null || employeeData.getShifts().size() == 0) {
            return combinedShifts;
        }

        List<Shift> sortedShifts = employeeData.getShifts().stream().sorted().collect(Collectors.toList());
        int index = 0, loopIdx = 1;
        combinedShifts.add(sortedShifts.get(0));
        while (loopIdx < sortedShifts.size()) {
            Shift shift = combinedShifts.get(index);
            Shift other = sortedShifts.get(loopIdx);
            if (shift.tryMerge(other)) {
                loopIdx++;
            } else {
                combinedShifts.add(other);
                index++;
                loopIdx++;
            }
        }

        return combinedShifts;
    }
}


public class TescoTest {

    public static void main(String args[]) {
        DataStoreService dataStoreService = new DataStoreService();
        ShiftService shiftService = new ShiftService(dataStoreService);
        EmployeeData employeeData1 = new EmployeeData("emp1");
        employeeData1.getShifts().addAll(Arrays.asList(
                new Shift(8, 10),
                new Shift(10, 12),
                new Shift(14, 19)
        ));
        EmployeeData employeeData2 = new EmployeeData("emp2");
        employeeData2.getShifts().addAll(Arrays.asList(
                new Shift(8, 10),
                new Shift(14, 19),
                new Shift(9, 12)
        ));
        dataStoreService.add(employeeData1);
        dataStoreService.add(employeeData2);
        System.out.println(shiftService.getCombinedShifts("emp1"));
        System.out.println(shiftService.getCombinedShifts("emp2"));
    }
}
