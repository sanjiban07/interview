import spock.lang.Specification

/*
You are given an integer array bloomDay, an integer m and an integer k.

You want to make m bouquets. To make a bouquet, you need to use k adjacent flowers from the garden.

The garden consists of n flowers, the ith flower will bloom in the bloomDay[i] and then can be used in exactly one bouquet.

Return the minimum number of days you need to wait to be able to make m bouquets from the garden. If it is impossible to make m bouquets return -1

Input: bloomDay = [1,10,3,10,2], m = 3, k = 1
Output: 3
Explanation: Let us see what happened in the first three days. x means flower bloomed and _ means flower did not bloom in the garden.
We need 3 bouquets each should contain 1 flower.
After day 1: [x, _, _, _, _] // we can only make one bouquet.
After day 2: [x, _, _, _, x] // we can only make two bouquets.
After day 3: [x, _, x, _, x] // we can make 3 bouquets. The answer is 3.

N >= m * k
m = 3
k = 2

[1,10,3,6,2, 7, 4, 1]
[-1, 10, 10, 6, 6, 7, 7, 4]
[]

[0, 1, 1, 2, 2, 3, 3, 4, 4]
0,   1,  1, 2, 2, 3, 4
-1, 10, 10, 6, 6,  7, 7

10, 3 => 10, 6

[1, 2, 3, 10, 10]
[0, 4, 2, 1, 3]

O(N2)
 */
class BloomDay extends Specification {
    int getMinDays(int[] bloomDay, int m, int k) {
        if (bloomDay == null || bloomDay.length == 0) {
            return -1;
        }

        if (k <= 0 || m <= 0 || bloomDay.length < m * k) {
            return -1;
        }

        int[] maxArray = new int[bloomDay.length];
        for (int i = 0; i < bloomDay.length; i++) {
            if (i < k - 1) {
                maxArray[i] = - 1;
                continue;
            }

            int max = bloomDay[i];
            for (int j = 1; j < k; j++) {
                if (max < bloomDay[i - j]) {
                    max = bloomDay[i - j];
                }
            }
            maxArray[i] = max;
        }

        PriorityQueue<Integer> queue = new PriorityQueue(3)
//        for (int i = 0; i < bloomDay.length; i = i + k) {
//            queue.add(maxArray[i])
//            if (queue.size() > m * k) {
//                queue.removeAt(0)
//            }
//        }
//        return queue.stream().mapToInt().min().orElseGet(-1);
        return Arrays.stream(maxArray).sorted()
            .limit(m*k)
            .max().orElseGet(-1);
    }
    def "test"() {
//        expect:
//        getMinDays(bloomDay as int[], m, k) == min
//        where:
//        bloomDay | m | k | min
//        [1,10,3,6,2, 7, 4, 1] | 3 | 1 | 3
//        [1,10,3,6,2, 7, 4, 1] | 3 | 2 | 7
    }
}
