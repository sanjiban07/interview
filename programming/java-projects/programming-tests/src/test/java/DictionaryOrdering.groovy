import spock.lang.Specification

class DictionaryOrdering extends Specification {
    class Node {
        public char ch;
        public Set<Node> children;
        public Node(char ch) {
            this.ch = ch;
            children = new HashSet<>();
        }

        boolean equals(o) {
            if (this.is(o)) return true
            if (getClass() != o.class) return false

            Node node = (Node) o

            if (ch != node.ch) return false

            return true
        }

        int hashCode() {
            return (int) ch
        }
    }

    List<Character> topoSort(Node head) {
        Set<Node> visited = new HashSet<>();
        List<Node> stack = new ArrayList<>();
        traverse(head, visited, stack);
        return stack.reverse();
    }

    void traverse(Node node, Set<Node> visited, List<Node> stack) {
        visited.add(node);
        for (Node child: node.children) {
            if (!visited.contains(child)) {
                traverse(child, visited, stack);
            }
        }

        stack.add(node);
    }

    char[] getCharOrderFromWords(String word1, String word2) {
        int len1 = word1.length(), len2 = word2.length();
        int i = 0, j = 0;
        char c1, c2;
        while (i < len1 && j < len2) {
            c1 = word1.charAt(i);
            c2 = word2.charAt(j);
            if (c1 == c2) {
                i++;
                j++;
                continue;
            }

            return [c1, c2] as char[];
        }

        return null;
    }

    List<Character> getOrderOfCharacter(String[] words) {
        if (words == null || words.length == 1) {
            return new LinkedList<>();
        }

        Node head = createDAG(words);
        List<Character> characterList = new ArrayList<>();
        List<Node> nodes = topoSort(head);
        nodes.remove(0);
        for (Node node: nodes) {
            characterList.add(node.ch);
        }
        return characterList;
    }

    Node createDAG(String[] words) {
        Node head = new Node(" ".charAt(0));
        Map<Character, Node> charLocationMap = new HashMap<>();
        for (int i = 0; i < words.length - 1; i++) {
            String word1 = words[i];
            String word2 = words[i + 1];
            char[] result = getCharOrderFromWords(word1, word2);
            if (result == null) {
                continue;
            }
            Node node1 = charLocationMap.getOrDefault(result[0], null);
            if (node1 == null) {
                node1 = new Node(result[0]);
                head.children.add(node1);
            }
            Node node2 = charLocationMap.getOrDefault(result[1], new Node(result[1]));
            node1.children.add(node2);
            charLocationMap.put(result[0], node1);
            charLocationMap.put(result[1], node2);
        }

        return head;
    }

    def "test"() {
        expect:
        getOrderOfCharacter(dictionary) == characterList
        where:
        dictionary | characterList
        ["b", "c", "cb", "cx", "cxc", "cxx"] as String[] | ['b', 'c', 'x']
        ["a", "c", "ca", "cb", "cba", "cbd", "cbdb", "cbdd", "cbddd", "cbddc"] as String[] | ['a', 'b', 'd', 'c']
        ["a", "c", "cb", "cd", "cda", "cdb", "cdba", "cdbd", "cdbdd", "cdbdc"] as String[] | ['a', 'b', 'd', 'c']
    }
}