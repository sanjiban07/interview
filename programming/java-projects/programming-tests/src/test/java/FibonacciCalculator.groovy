import spock.lang.Specification

class FibonacciCalculator extends Specification {
    int getNthFibonacciNumber(int n) {
        if (n == 1 || n == 2) {
            return 1;
        }

        int[] table = new int[n];
        table[0] = table[1] = 1;

        for (int i = 2; i < n; i++) {
            table[i] = table[i - 1] + table[i - 2];
        }

        return table[n - 1];
    }

    def "test fibonacci 1" () {
        when:
        def i = 40
        then:
        getNthFibonacciNumber(i) == 102334155
    }
    def "test fibonacci 2" () {
        expect:
        getNthFibonacciNumber(a) == b
        where:
        a | b
        1 | 1
        2 | 1
        40 | 102334155
    }
    def "test"() {

    }
}