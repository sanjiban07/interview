import spock.lang.Specification

class FilterResultFromGetResponseTest extends Specification {
    def "Test" () {
        expect:
        ArcesiumFilterResultFromGetResponse.apiResponseParser(inputList, size) == response
        where:
        inputList |size | response
        ["username", "EQUALS", "Garimag"] | 3 | [9]
        ["address.city", "EQUALS", "Kolkata"] | 3 | [2]
        ["address.city", "IN", "Mumbai,Kolkata"] | 3 | [1, 2]
        ["username", "EQUALS", "Tom"] | 3 | [-1]

    }
}
