import spock.lang.Specification

class FindNthDigitInIntStream extends Specification{
    int findNthDigitInIntStream(int[] arr, int N) {
        if (arr == null || arr.length == 0 || N < 1) {
            return -1;
        }
        int i;
        for (i = 0; i < arr.length; i++) {
            int X = arr[i];
            if (X == 0) {
                if ( N == 1)
                    return X;
                N--;
                continue;
            }
            int countDigit = 0, temp = X, X1 = 1, X2 = 1;
            int revX = 0;
            while (temp > 0) {
                revX = (revX * 10) + (temp % 10);
                temp = temp / 10;
                countDigit++;
                X2 = X1
                X1 = X1 * 10; // X1 = 10 * 10 * 10;
            }

            if (countDigit < N) {
                N = N - countDigit;
                continue;
            }

            revX = (revX % X1);
            revX = revX / X2;
            return revX;
        }
    }

    def "test"() {
        expect:
        findNthDigitInIntStream(arr as int[], N) == D
        where:
        arr | N | D
        [0] | 1 | 0
        [1, 0] | 2 | 0
        [0, 1] | 2 | 1
        [1, 0] | 1 | 1
        [12, 0] | 2 | 2
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,123] | 3 | 3
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,123] | 11 | 0
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,123] | 16 | 3
    }

    static class TreeNode {
        public TreeNode left = -1, right = -1;
    }

    int findInTree(TreeNode root, TreeNode X) {
        if (root == null) return false;
        if (root == X) {
            return 0;
        }
        int distance = findInTree(root.left, X);
        if (distance >= 0) {
            return distance + 1;
        }

        distance = findInTree(root.right, X);
        if (distance >= 0) {
            return distance + 1;
        }
        return -1;
    }

    int findDistance(TreeNode root, TreeNode X, TreeNode Y) {
        if (root == null) return -1;

        if (root == X) {
            return findInTree(root, Y);
        }

        if (root == Y) {
            return findInTree(root, X);
        }

        int XinLeft = findInTree(root.left, X);
        int XinRight = findInTree(root.right, X);
        int YinLeft = findInTree(root.left, Y);
        int YinRight = findInTree(root.right, Y);
        if (XinLeft >= 0 && YinRight >= 0) {
            return XinLeft + YinRight;
        } else if (XinRight >= 0 && YinLeft >= 0) {
            return XinRight + YinLeft;
        } else if (XinLeft >= 0 && YinLeft >= 0) {
            return findDistance(root.left, X, Y);
        } else if (XinRight >= 0 && YinRight >= 0) {
            return findDistance(root.right, X, Y);
        } else {
            return -1;
        }
    }
}
