import spock.lang.Specification

class FindRootInABinaryTree extends Specification {
    class BSTNode {
        public int value;
        public BSTNode left;
        public BSTNode right;

        public BSTNode(int value, BSTNode left, BSTNode right) {
            this.value = value;
            this.left = left;
            this.right = right;
        }
    }

    int findRoot(List<List<Integer>> nodes) {
        // Write your code here
        Map<Integer, Integer> parentMap = new HashMap<>();
        for (List<Integer> node: nodes) {
            if (node.get(0) < 0) {
                return -1;
            }
            parentMap.put(node.get(0), -1);
        }
        for (List<Integer> node: nodes) {
            int root = node.get(0);
            int left = node.get(1);
            int right = node.get(2);
            if (left >= 0) {
                if (parentMap.containsKey(left) && parentMap.get(left) == -1 && left <= root) {
                    parentMap.put(left, root);
                } else {
                    return -1;
                }
            }
            if (right >= 0) {
                if (parentMap.containsKey(right) && parentMap.get(right) == -1 && right >= root) {
                    parentMap.put(right, root);
                }
            }
        }
        int rootCount = 0, root = -1;
        for (List<Integer> node: nodes) {
            if (parentMap.get(node.get(0)) == -1) {
                rootCount++;
                root = node.get(0);
            }
        }
        if (rootCount != 1) {
            return -1;
        }
        return root;
    }

    def "find root 1" () {
        when:
        def nodes = [[17, -1, -1], [15, 13, 17], [7, -1, -1], [13, -1, -1], [5, 3, 7], [3, -1, -1], [10, 5, 15]]
        then:
        findRoot(nodes) == 10
    }

    def "find root 2" () {
        when:
        def nodes = [[17, -1, -1], [15, 13, 17], [7, -1, -1], [13, -1, -1], [5, 3, 7], [3, -1, -1]]
        then:
        findRoot(nodes) == -1
    }

    def "find root 3" () {
        when:
        def nodes = [[17, -1, -1], [15, 13, 17], [7, -1, -1], [13, -1, -1], [5, 3, 7], [3, -1, -1]]
        then:
        findRoot(nodes) == -1
    }
}
