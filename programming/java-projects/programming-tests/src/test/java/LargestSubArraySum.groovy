import spock.lang.Specification

class LargestSubArraySum extends Specification {
    long maximumSum(List<Long> a, long m) {
        // Write your code here
        int N = a.size();
        long[][] dp = new long[N][N];
        long max = 0;
        int subLen;
        for (subLen = 0; subLen < N; subLen++) {
            int i = 0, j = subLen;
            while (i < N - subLen && j < N) {
                if (i == j) {
                    dp[i][j] = a.get(i) % m;
                } else {
                    dp[i][j] = (dp[i][j-1] + a.get(i)) % m;
                }

                if (dp[i][j] > max) {
                    max = dp[i][j];
                }
                i++;
                j++;
            }
        }

        return max;
    }

    def "Test maxSubArray"() {
        expect:
        maximumSum(array, mod) == max
        where:
        array           | mod   | max
        [1, 2, 3]       | 2     | 1
        [3, 3, 9, 9, 5] | 6     | 5
    }
}
