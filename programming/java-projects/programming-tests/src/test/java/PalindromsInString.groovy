import javafx.util.Pair
import spock.lang.Specification

class PalindromsInString extends Specification {
    // https://www.youtube.com/watch?v=XmSOWnL6T_I
    int countPalindromsInString(String string) {
//        System.out.println("string: " + string);
        int size = string.length();
        boolean[][] dp = new boolean[size][size];
        int i, j, subStrLen;
        int count = 0;
        for (subStrLen = 0; subStrLen < size; subStrLen++) {
            i = 0;
            j = subStrLen;
            while (i < size - subStrLen && j < size) {
                if (subStrLen == 0) {
                    dp[i][j] = true;
                } else if (subStrLen == 1) {
                    dp[i][j] = string.charAt(i) == string.charAt(j);
                } else {
                    dp[i][j] = (string.charAt(i) == string.charAt(j)) && dp[i+1][j-1];
                }
//                if (dp[i][j])
//                    System.out.println(string.substring(i, j + 1));
                count = dp[i][j]? count + 1 : count;
                i++;
                j++;
            }
        }
        return count;
    }

    // https://www.youtube.com/watch?v=DK5OKKbF6GI
    String longestPalindromInString(String string) {
        System.out.println("string: " + string);
        if (string == null) {
            return string;
        }

        Pair<Integer, Integer> startAndEndMax = new Pair<>(0, 0);
        int maxLen = 1;
        for (int mid = 0; mid < string.length(); mid++) {
            // Odd length palindrome
            Pair<Integer, Integer> startAndEndOdd = getLongestPalindromeWithMid(string, mid, mid);
            if (startAndEndOdd.getValue() - startAndEndOdd.getKey() + 1 > maxLen) {
                maxLen = startAndEndOdd.getValue() - startAndEndOdd.getKey() + 1;
                startAndEndMax = startAndEndOdd;
            }

            if (mid < string.length() - 1 && string.charAt(mid) == string.charAt(mid+1)) {
                // Even length palindrome
                Pair<Integer, Integer> startAndEndEven = getLongestPalindromeWithMid(string, mid, mid + 1);
                if (startAndEndEven.getValue() - startAndEndEven.getKey() + 1 > maxLen) {
                    maxLen = startAndEndEven.getValue() - startAndEndEven.getKey() + 1;
                    startAndEndMax = startAndEndEven;
                }
            }
        }

        return string.substring(startAndEndMax.getKey(), startAndEndMax.getValue() + 1);
    }

    Pair<Integer, Integer> getLongestPalindromeWithMid(String string, int begin, int end) {
        while (begin >= 0 && end < string.size() && string.charAt(begin) == string.charAt(end)) {
            System.out.println(string.substring(begin, end + 1));
            begin --;
            end ++;
        }
        return new Pair<Integer, Integer>(begin+1, end-1);
    }

    def "test countPalindromsInString"() {
        expect:
        countPalindromsInString(string) == count
        where:
        string | count
        "a"         | 1
        "ab"        | 2
        "asasd"     | 7
        "abcbaba"   | 11
        "aaaa"      | 10
    }

    def "test longestPalindromInString"() {
        expect:
        longestPalindromInString(string) == palindrome
        where:
        string      | palindrome
        "a"         | "a"
        "ab"        | "a"
        "asasd"     | "asa"
        "abcbaba"   | "abcba"
        "aaaa"      | "aaaa"
    }
}

/*
A string is said to be a special string if either of two conditions is met:
All of the characters are the same, e.g. aaa.
    * All characters except the middle one are the same, e.g. aadaa.
    * A special substring is any substring of a string which meets one of those criteria.
    Given a string, determine how many special substrings can be formed from it.

A special substring is any substring of a string which meets one of those criteria.
Given a string, determine how many special substrings can be formed from it.
*/