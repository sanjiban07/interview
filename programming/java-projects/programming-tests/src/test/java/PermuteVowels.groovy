import spock.lang.Specification

class PermuteVowels extends Specification {
    boolean isVowel(char ch) {
        return ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u';
    }
    List<String> findConsonantsBetweenVowels(String[] words) {
        List<String> output = new ArrayList<>();
        for (int i = 0; i < words.length; i ++) {
            String word = words[i];
            int count = 0;
            boolean printWord = false;
            for (int pos = 1; pos < word.length() - 1; pos ++) {
                if (!isVowel(word.charAt(pos))) {
                    if (isVowel(word.charAt(pos - 1)) && isVowel(word.charAt(pos + 1))) {
                        count ++;
                        printWord = true;
                    }
                }
            }

            if (printWord) {
                output.add(word + "(" + count + ")");
            }
        }
        return output;
    }

    def "Test findConsonantsBetweenVowels"() {
        expect:
        findConsonantsBetweenVowels(words as String[]) == output
        where:
        words                   |       output
        ["Sanjiban"]            |       ["Sanjiban(1)"]
        ["xay"]                 |       []
        ["xayizot", "xayi"]     |       ["xayizot(2)", "xayi(1)"]
    }
}
