import spock.lang.Specification

class PriorityQueue extends Specification {
    class BookIssueCnt implements Comparable<BookIssueCnt> {
        private String book;
        private long count;

        BookIssueCnt(String book, long count) {
            this.book = book
            this.count = count
        }

        String getBook() {
            return book
        }

        void setBook(String book) {
            this.book = book
        }

        long getCount() {
            return count
        }

        void setCount(long count) {
            this.count = count
        }

        public int compareTo(BookIssueCnt other) {
            return Long.compare(this.getCount(), other.getCount());
        }
    }

    List<String> topKMostIssuedBooks(List<String> bookCheckoutSequence, int K) {
        if (bookCheckoutSequence == null || K <= 0) {
            return new ArrayList<String>();
        }
        if (bookCheckoutSequence.size() <= K) {
            return bookCheckoutSequence;
        }

        Map<String, Integer> bookwiseIssueCount = new HashMap<>()
        for (String book: bookCheckoutSequence) {
            long existingCount = bookwiseIssueCount.getOrDefault(book, 0L)
            bookwiseIssueCount.put(book, existingCount + 1)
        }
        java.util.PriorityQueue<BookIssueCnt> queue = new java.util.PriorityQueue<BookIssueCnt>(K);
        for (def entry : bookwiseIssueCount.entrySet()) {
            queue.add(new BookIssueCnt(entry.getKey(), entry.getValue()))
            if (queue.size() > K) {
                queue.removeAt(0)
            }
        }
        List<String> books = new ArrayList<>();
        for (BookIssueCnt bookIssueCnt: queue) {
            books.add(bookIssueCnt.getBook())
        }
        return books;
    }

    def "Test"() {
        expect:
        topKMostIssuedBooks(bookCheckoutSequence, K) == books
        where:
        bookCheckoutSequence | K | books
        null | 2 | []
        [] | 2 | []
        ["B1"] | 2 | ["B1"]
        ["B1", "B1", "B2"] | 2 | ["B2", "B1"]
        ["B1", "B2", "B1", "B2", "B3"] | 2 | ["B1", "B2"]
        ["B1", "B1", "B2", "B1", "B2", "B3"] | 1 | ["B1"]
        ["B1", "B2", "B1", "B2", "B3"] | 2 | ["B1", "B2"]
    }
}
