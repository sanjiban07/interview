import spock.lang.Specification

class ReOrderArray extends Specification {
    void reOrder(int[] arr, int[][] operations) {
        for (int i = 0; i < operations.length; i++) {
            reOrder(arr, operations[i]);
        }
    }

    void reOrder(int[] arr, int[] operation) {
        int pos1 = -1, pos2 = -1;
        for (int i = 0; i < arr.length; i++) {
            if (operation[0] == arr[i]) pos1 = i;
            else if (operation[1] == arr[i]) pos2 = i;
        }
        if (pos1 == -1 || pos2 == -1 || pos1 == arr.length - 1) return;
        int xPos = pos1 + 1;
        int x = arr[xPos];
        if (xPos < pos2) {
            for (int i = xPos; i < pos2; i++) {
                arr[i] = arr[i + 1];
            }
            arr[pos2] = x;
        } else {
            for (int i = xPos; i > pos2 + 1; i--) {
                arr[i] = arr[i-1];
            }
            arr[pos2 + 1] = x;
        }
    }

    def "test playment 1"() {
        when:
        def arr = [1, 2, 3, 4] as int[]
        def operations = [
                [1, 3],
                [3, 1]
        ] as int[][]
        reOrder(arr, operations)
        then:
        arr == [1, 2, 3, 4]
    }

    def "test playment 2"() {
        when:
        def arr = [1, 2, 3, 4] as int[]
        def operations = [
                [1, 3],
                [3, 1]
        ] as int[][]
        reOrder(arr, operations)
        then:
        arr == [1, 2, 3, 4]
    }
}