import spock.lang.Specification

class RecursivelyRemoveAdjascentDuplicates extends Specification {
    String recursivelyRemoveAdjascentDuplicates(String input) {
        if (input.size() == 0 || input.size() == 1) {
            return input;
        }
        boolean[] flag = new boolean[input.size()];
        int left = 0, right = 1, lastLeft = -1;
        while (right < input.size()) {
            if (input.charAt(left) != input.charAt(right)) {
                lastLeft = left;
                left = right;
                right = right + 1;
            } else {
                flag[left] = true;
                while (right < input.size() && input.charAt(left) == input.charAt(right)) {
                    flag[right] = true;
                    right++;
                }

                if (lastLeft == -1) {
                    left = right;
                    right = right + 1;
                } else {
                    left = lastLeft;
                }
            }

        }

        StringBuilder stringBuilder = new StringBuilder();
        for (left = 0; left < input.size(); left++) {
            if (!flag[left]) stringBuilder.append(input.charAt(left));
        }
        return stringBuilder.toString();
    }

    def "test"() {
        expect:
        recursivelyRemoveAdjascentDuplicates(input) == output
        where:
        input               | output
        "aaaa"              | ""
        "aabba"             | "a"
        "azxxzy"            | "ay"
        "geeksforgeeg"      | "gksfor"
        "caaabbbaacdddd"    | ""
        "acaaabbbacdddd"    | "acac"
    }
}
