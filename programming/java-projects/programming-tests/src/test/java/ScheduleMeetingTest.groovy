import spock.lang.Specification

class ScheduleMeetingTest extends Specification {
    int[][] meetings = [
            [1230, 1300], // 12:30 PM to 1:00 PM
            [845, 900],   //  8:45 AM to 9:00 AM
            [1300, 1500]  //  1:00 PM to 3:00 PM
    ];

    def "test" () {
        expect:
        ScheduleMeeting.isAvailable(meetings, startTime, endTime) == isAvailable
        where:
        startTime | endTime | isAvailable
        915     | 1215  | true
        900     | 1230  | true
        850     | 1240  | false
        1200    | 1300  | false
        700     | 1600  | false
        800     | 845   | true
        1500    | 1800  | true
        845     | 859   | false
        846     | 900   | false
        846     | 859   | false
        845     | 900   | false
        2359    | 2400  | true
        930     | 1600  | false
        800     | 850   | false
        1400    | 1600  | false
        1300    | 1501  | false
    }
}
