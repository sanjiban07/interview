import spock.lang.Specification

class SecureChannel extends Specification {
    String encode(String message, String key) {
        char[] messageArray = message.toCharArray();
        int messageLen = messageArray.length;
        char[] keyArray = key.toCharArray();
        int keyLen = keyArray.length;

        String buf = "";
        if (messageLen > keyLen) {
            buf = message.substring(keyLen);
        } else {
            keyLen = messageLen;
        }

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < keyLen; i ++) {
            char keyChar = keyArray[i];
            if (keyChar < '1' || keyChar > '9') {
                return "-1";
            }

            char zero = '0';
            for (int j = 0; j < keyChar - zero; j ++) {
                stringBuilder.append(messageArray[i]);
            }
        }

        return stringBuilder.toString() + buf;
    }

    String decode(String message, String key) {
        if (message.equals("-1")) {
            return "-1";
        }

        char[] messageArray = message.toCharArray();
        int messageLen = messageArray.length;
        char[] keyArray = key.toCharArray();
        int keyLen = keyArray.length;

        StringBuilder stringBuilder = new StringBuilder();
        int messagePos = 0;
        for (int keyPos = 0; keyPos < keyLen && messagePos < messageLen; keyPos ++) {
            char keyChar = keyArray[keyPos];
            if (keyChar < '1' || keyChar > '9') {
                return "-1";
            }

            int repeatCount = keyChar - (char) '0';
            char lastChar = messageArray[messagePos];
            for (int idx = 1; idx < repeatCount; idx ++) {
                if (messagePos + idx >= messageLen || messageArray[messagePos] != lastChar) {
                    return "-1";
                }
            }

            messagePos += repeatCount;
            stringBuilder.append(lastChar);
        }

        if (messagePos < messageLen) {
            stringBuilder.append(message.substring(messagePos));
        }

        return stringBuilder.toString();
    }

    String secureChannel(int operation, String message, String key) {
        if (message == null || key == null || operation != 1 && operation != 2) {
            return "-1";
        } else if (message.length() == 0 || key.length() == 0) {
            return message;
        } else if (operation == 1) {
            return encode(message, key);
        } else {
            return decode(message, key);
        }
    }

    def "Test secure channel"() {
        expect:
        secureChannel(operation, message, key) == secureMsg
        where:
        operation | message | key | secureMsg
        1 | "Open" | "12345" | "Oppeeennnn"
        2 | "Oppeeen" | "123" | "Open"
    }
}