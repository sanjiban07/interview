import spock.lang.Specification

class SortMeetings extends Specification {
    int maxEvents(List<Integer> arrival, List<Integer> duration) {
        if (arrival == null || duration == null)
        {
            return 0;
        }

        int N = arrival.size();
        List<Integer> departure = new ArrayList<Integer>();
        for (int i = 0; i < N; i++)
        {
            departure.add(i, arrival.get(i) + duration.get(i));
        }

        int max = 0;
        int i = 0, j = 0;
        int count = 0;
        arrival.sort({ o1, o2 -> o1.compareTo(o2) });
        departure.sort({ o1, o2 -> o1.compareTo(o2) });
        for (; i < N && j < N; )
        {
            if (arrival.get(i) <= departure.get(j))
            {
                count++;
                i++;
                if (count > max)
                {
                    max = count;
                }
            }
            else
            {
                j++;
                count--;
            }
        }

        return max;
    }

    def "Test sort meetings"() {
        when:
        def arrival = [1, 3, 3, 5, 7]
        def duration = [2, 2, 1, 2, 1]
        then:
        maxEvents(arrival, duration) == 3
    }
}