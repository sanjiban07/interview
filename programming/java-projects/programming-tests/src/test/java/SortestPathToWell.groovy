class SortestPathToWell {
    int getBestDistance(int[][] absoluteDistanceMap, int[][] temporaryDistanceMap, int i, int j) {
        if (absoluteDistanceMap[i][j] != Integer.MAX_VALUE) {
            return absoluteDistanceMap[i][j] + 1;
        } else if (temporaryDistanceMap[i][j] != Integer.MAX_VALUE){
            return temporaryDistanceMap[i][j] + 1;
        } else {
            return Integer.MAX_VALUE;
        }
    }
    void findShortestPathToWell(int M, int N, int[][] absoluteDistanceMap, int i, int j, int[][] temporaryDistanceMap) {
        int left = Integer.MAX_VALUE, right = Integer.MAX_VALUE, top = Integer.MAX_VALUE, bottom = Integer.MAX_VALUE;
        if (i > 0) {
            top = getBestDistance(absoluteDistanceMap, temporaryDistanceMap, i-1, j);
            temporaryDistanceMap[i][j] = Math.min(temporaryDistanceMap[i][j], top);
        }
        if (j > 0) {
            left = getBestDistance(absoluteDistanceMap, temporaryDistanceMap, i, j-1);
            temporaryDistanceMap[i][j] = Math.min(temporaryDistanceMap[i][j], left);
        }
        if (i < M - 1) {
            bottom = getBestDistance(absoluteDistanceMap, temporaryDistanceMap, i+1, j);
            temporaryDistanceMap[i][j] = Math.min(temporaryDistanceMap[i][j], bottom);
        }
        if (j < N - 1) {
            right = getBestDistance(absoluteDistanceMap, temporaryDistanceMap, i, j+1);
            temporaryDistanceMap[i][j] = Math.min(temporaryDistanceMap[i][j], right);
        }

        if (top != Integer.MAX_VALUE && left != Integer.MAX_VALUE && bottom != Integer.MAX_VALUE && right != Integer.MAX_VALUE) {
            absoluteDistanceMap[i][j] = temporaryDistanceMap[i][j];
        }

        if (top == Integer.MAX_VALUE && i > 0) {
            findShortestPathToWell(M, N, absoluteDistanceMap, i-1, j, temporaryDistanceMap);
            top = Math.min(top, absoluteDistanceMap[i-1][j]);
        }
        if (left == Integer.MAX_VALUE && j > 0) {
            findShortestPathToWell(M, N, absoluteDistanceMap, i, j-1, temporaryDistanceMap);
            left = Math.min(left, absoluteDistanceMap[i-1][j]);
        }
        if (bottom == Integer.MAX_VALUE && i < M - 1) {
            findShortestPathToWell(M, N, absoluteDistanceMap, i+1, j, temporaryDistanceMap);
            bottom = Math.min(bottom, absoluteDistanceMap[i-1][j]);
        }
        if (right == Integer.MAX_VALUE && j < N - 1) {
            findShortestPathToWell(M, N, absoluteDistanceMap, i, j+1, temporaryDistanceMap);
            right = Math.min(right, absoluteDistanceMap[i-1][j]);
        }

        absoluteDistanceMap[i][j] = Math.min(top, Math.min(left, Math.min(bottom, right)));
    }

    int[][] findMinDistanceMap(char [][] wellMap, int M, int N) {
        if (wellMap == null) {
            return null;
        }

        int[][] absoluteDistanceMap = new int[M][N];
        int[][] temporaryDistanceMap = new int[M][N];
        for (int i = 0; i < M; i++) {
            for (int j = 0; i < N; j++) {
                if (wellMap[i][j] == 'W') {
                    absoluteDistanceMap[i][j] = 0;
                    temporaryDistanceMap[i][j] = 0;
                } else {
                    absoluteDistanceMap[i][j] = Integer.MAX_VALUE;
                    temporaryDistanceMap[i][j] = Integer.MAX_VALUE;
                }
            }
        }

        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) {
                if (absoluteDistanceMap[i][j] == 0 || absoluteDistanceMap[i][j] != Integer.MAX_VALUE) {
                    continue;
                }

                findShortestPathToWell(M, N, absoluteDistanceMap, i, j, temporaryDistanceMap);
            }
        }
        return absoluteDistanceMap;
    }
}

/*
w w w w
w - - w
- - - -
- - - -
- - - -
w w w w

M*N*F


0 0 0 0
0 1 1 0
1 2 2 1
2 2 2 2
1 1 1 1
0 0 0 0

0 0 0 0
0 1 1 0
1 2 2 1
2 2 3 2
2 2 2 2
1 1 1 1
0 0 0 0
min (3, 0) + 1

w 1 2
1 2 3
1 w -

*/
