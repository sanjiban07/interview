import spock.lang.Specification

class StepsRequiredToReachN extends Specification {
    int findPossibility(int N) {
        if (N == 0) {
            return 0;
        } else if (N == 1) {
            return 1;
        } else if (N == 2) {
            return 2;
        }

        int x = 1;
        int y = 2, temp;
        for (int i = 3; i <= N; i++) {
            temp = y;
            y = x + y;
            x = temp;
        }

        return y;
    }

    def "test"() {
        expect:
        findPossibility(N) == X
        where:
        N | X
        0 | 0
        1 | 1
        2 | 2
        3 | 3
        4 | 5
        5 | 8
    }
}
