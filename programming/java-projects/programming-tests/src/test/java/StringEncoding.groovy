import spock.lang.Specification

class StringEncoding extends Specification {
    String collapseString(String inputString) {
        if (inputString == null || inputString.length() == 0) {
            return "";
        }

        char[] inputStrArray = inputString.toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        char lastChar = inputStrArray[0];
        int count = 1;
        for (int i = 1; i < inputStrArray.length; i ++) {
            if (lastChar == inputStrArray[i]) {
                count ++;
            } else {
                stringBuilder.append(count);
                stringBuilder.append(lastChar);
                lastChar = inputStrArray[i];
                count = 1;
            }
        }

        stringBuilder.append(count);
        stringBuilder.append(lastChar);
        return stringBuilder.toString();
    }

    def "Test string encoding"() {
        expect:
        collapseString(input) == output
        where:
        input       | output
        "t"         | "1t"
        "aabbbcdd"  | "2a3b1c2d"
    }
}
