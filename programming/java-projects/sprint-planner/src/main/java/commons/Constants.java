package commons;

public final class Constants {
    public static final int MAX_SPRINT_SIZE = 20;
    public static final int MAX_TASK_LIMIT = 2;
}
