package models;

import lombok.Data;

@Data
public class Employee {
    public Employee() {
        _ID++;
        id = "EMP" + _ID;
    }

    private static int _ID = 0;
    private String id;
}
