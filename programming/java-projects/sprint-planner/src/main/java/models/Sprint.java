package models;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

public class Sprint {
    public Sprint() {
        _ID++;
        id = "SPR" + _ID;
        tasks = new HashMap<>();
        employeeWiseTaskCount = new HashMap<>();
    }

    private static int _ID = 0;
    @Getter private final String id;
    @Getter private final Map<String, Task> tasks;
    @Getter private final Map<String, Integer> employeeWiseTaskCount;
}
