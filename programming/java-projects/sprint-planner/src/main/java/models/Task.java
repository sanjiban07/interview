package models;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Task {
    public enum Type {
        STORY, FEATURE, BUG
    }

    @Getter
    public enum Status {
        TODO(0), INPROGRESS(1), DONE(2);

        private final int value;
        private Status(int value) {
            this.value = value;
        }

        public Optional<Status> getNextStatus() {
            switch (this) {
                case TODO:
                    return Optional.of(INPROGRESS);
                case INPROGRESS:
                    return Optional.of(DONE);
                default:
                    return Optional.empty();
            }
        }
    }

    @Builder
    public Task(Type type, long startTs, long endTs) {
        _ID++;
        id = "TSK" + _ID;
        this.type = type;
        this.startTs = startTs;
        this.endTs = endTs;
        status = Status.TODO;
    }

    private static int _ID;
    @Getter private final String id;
    @Getter private final Type type;
    @Getter @Setter private Status status;
    @Getter private long startTs;
    @Getter private long endTs;
    @Getter @Setter private Optional<Employee> assignee;
}
