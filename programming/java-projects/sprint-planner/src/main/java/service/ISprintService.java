package service;

import models.Sprint;
import models.Task;

import java.util.List;
import java.util.Optional;

public interface ISprintService {
    String create();

    Optional<Sprint> get(String sprintId);

    boolean addTask(String sprintId, Task task) throws Exception;

    Optional<Task> getTask(String sprintId, String taskId) throws Exception;

    Task removeTask(String sprintId, String taskId) throws Exception;

    List<Task> getDelayedTasks(String sprintId) throws Exception;
}
