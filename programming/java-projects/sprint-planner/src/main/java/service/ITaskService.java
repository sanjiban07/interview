package service;

import models.Sprint;
import models.Task;

public interface ITaskService {
    Task createTask(Task.Type type, long startTs, long endTs);

    void update(Task task);

    boolean isDelayed(Task task);

    Task removeTask(Sprint sprint, String taskId);

    boolean addTask(Sprint sprint, Task task);
}
