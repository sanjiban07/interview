package service.impl;

import commons.Constants;
import models.Employee;
import models.Sprint;
import models.Task;
import service.ISprintService;
import service.ITaskService;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class SprintService implements ISprintService {
    private final Map<String, Sprint> sprints;
    private final ITaskService taskService;

    public SprintService(TaskService taskService) {
        this.taskService = taskService;
        sprints = new ConcurrentHashMap<>();
    }

    @Override
    public String create() {
        Sprint sprint = new Sprint();
        sprints.put(sprint.getId(), sprint);
        return sprint.getId();
    }

    @Override
    public Optional<Sprint> get(String sprintId) {
        return Optional.ofNullable(sprints.getOrDefault(sprintId, null));
    }

    @Override
    public boolean addTask(String sprintId, Task task) throws Exception {
        Optional<Sprint> sprint = get(sprintId);
        if (!sprint.isPresent()) throw new Exception("Sprint is not present");
        return taskService.addTask(sprint.get(), task);
    }

    @Override
    public Optional<Task> getTask(String sprintId, String taskId) throws Exception {
        Optional<Sprint> sprint = get(sprintId);
        if (!sprint.isPresent()) throw new Exception("Sprint is not present");
        return Optional.ofNullable(sprint.get().getTasks().getOrDefault(taskId, null));
    }

    @Override
    public Task removeTask(String sprintId, String taskId) throws Exception {
        Optional<Sprint> sprint = get(sprintId);
        if (!sprint.isPresent()) throw new Exception("Sprint is not present");
        return taskService.removeTask(sprint.get(), taskId);
    }

    @Override
    public List<Task> getDelayedTasks(String sprintId) throws Exception {
        Optional<Sprint> sprint = get(sprintId);
        if (!sprint.isPresent()) throw new Exception("Sprint is not present");
        return sprint.get().getTasks().values().stream()
                .filter(task -> taskService.isDelayed(task))
                .collect(Collectors.toList());
    }

    public synchronized boolean assign(Employee employee, String sprintId, String taskId) throws Exception {
        Optional<Task> task = getTask(sprintId, taskId);
        if (!task.isPresent()) throw new Exception("Task is not present");

        Map<String, Integer> employeeWiseTaskCount = sprints.get(sprintId).getEmployeeWiseTaskCount();
        if (employeeWiseTaskCount.getOrDefault(employee.getId(), 0) >= Constants.MAX_TASK_LIMIT) {
            return task.get().getAssignee().isPresent() && task.get().getAssignee().get().getId().equals(employee.getId());
        }

        if (task.get().getAssignee().isPresent()) {
            String prevEmployeeId = task.get().getAssignee().get().getId();
            employeeWiseTaskCount.put(prevEmployeeId, employeeWiseTaskCount.get(prevEmployeeId) - 1);
        }

        task.get().setAssignee(Optional.of(employee));
        employeeWiseTaskCount.put(employee.getId(), employeeWiseTaskCount.getOrDefault(employee.getId(), 0) + 1);
        return true;
    }
}