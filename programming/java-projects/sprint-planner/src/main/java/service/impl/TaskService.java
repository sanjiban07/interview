package service.impl;

import commons.Constants;
import models.Sprint;
import models.Task;
import service.ITaskService;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

public class TaskService implements ITaskService {
    private final Map<String, Task> tasks;

    public TaskService() {
        tasks = new HashMap<>();
    }

    @Override
    public Task createTask(Task.Type type, long startTs, long endTs) {
        return new Task(type, startTs, endTs);
    }

    @Override
    public synchronized void update(Task task) {
        Task.Status status = task.getStatus().getNextStatus().isPresent()? task.getStatus().getNextStatus().get() : task.getStatus();
        task.setStatus(status);
    }

    @Override
    public boolean isDelayed(Task task) {
        return Instant.now().getEpochSecond() > task.getStartTs() && compareStatus(task.getStatus(), Task.Status.INPROGRESS) < 0
                || Instant.now().getEpochSecond() > task.getEndTs() && compareStatus(task.getStatus(), Task.Status.DONE) < 0;
    }

    @Override
    public synchronized Task removeTask(Sprint sprint, String taskId) {
        return sprint.getTasks().remove(taskId);
    }

    @Override
    public synchronized boolean addTask(Sprint sprint, Task task) {
        if (sprint.getTasks().size() <= Constants.MAX_SPRINT_SIZE) {
            sprint.getTasks().putIfAbsent(task.getId(), task);
            return true;
        }

        return false;
    }

    private int compareStatus(Task.Status status1, Task.Status status2) {
        return Integer.compare(status1.getValue(), status2.getValue());
    }
}
