import service.ISprintService
import service.ITaskService
import service.impl.SprintService
import service.impl.TaskService
import spock.lang.Specification

class TestSprintPlanner extends Specification {
    ISprintService sprintService;
    ITaskService taskService
    def setup() {
        taskService = new TaskService()
        sprintService = new SprintService(taskService)
    }
}
