package synchronization;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CallableFutureTest {
    private static class CallableExample implements Callable<String> {
        private static int _ID = 0;
        private final String name;

        CallableExample() {
            ++_ID;
            name = "Callable_" + _ID;
        }

        @Override
        public String call() throws Exception {
            System.out.println(name + " called.");
            Thread.sleep(1000);
            System.out.println(name + " returning.");
            return name;
        }
    }

    static void callableParallel() throws Exception {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        CallableExample callables[] = new CallableExample[5];
        for (int i = 0; i < 5; i++) {
            callables[i] = new CallableExample();
        }

        List<Future<String>> resultList = executorService.invokeAll(Arrays.asList(callables));
        executorService.shutdown();
        for (int i = 0; i < 5; i++) {
            System.out.println("Future returned" + resultList.get(i).get());
        }
    }

    static void callableDemo() throws Exception {
        for (int i = 0; i < 5; i++) {
            System.out.println("Future returned" + new CallableExample().call());
        }
    }

    static void futureBlocking() throws ExecutionException, InterruptedException {
        Future<String> futures[] = new Future[5];
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        for (int i = 0; i < 5; i++) {
            futures[i] = executorService.submit(new CallableExample());
            System.out.println("Future returned" + futures[i].get());
        }
        executorService.shutdown();
    }

    static void callableAny() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        CallableExample callables[] = new CallableExample[5];
        for (int i = 0; i < 5; i++) {
            callables[i] = new CallableExample();
        }

        String result = executorService.invokeAny(Arrays.asList(callables));
        System.out.println("Future returned" + result);
        executorService.shutdown();
    }

    static void completableFuture() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);
        CompletableFuture<String> completableFutures[] = new CompletableFuture[5];//CompletableFuture.supplyAsync(task, service);
        for (int i = 0; i < 5; i++) {
            int id = i;
            completableFutures[i] = CompletableFuture.supplyAsync(() -> {
                try {
                    System.out.println("Future_" + id + " called.");
                    Thread.sleep(1000);
                    System.out.println("Future_" + id + " returning.");
                    return "Future_" + id;
                } catch (Exception exception) {
                    return "";
                }
            }, executorService);
        }
//        for (int i = 0; i < 5; i++) {
//            System.out.println("Future returned" + completableFutures[i].get());
//        }
        System.out.println(
                Stream.of(completableFutures).map(CompletableFuture::join).collect(Collectors.joining(", "))
        );
        executorService.shutdown();
    }

    public static void main(String args[]) throws Exception {
//        callableDemo();
//        callableAny();
//        callableParallel();
//        futureBlocking();
//        completableFuture();
    }
}
