package synchronization;

import java.util.concurrent.locks.ReentrantLock;

public class DiningPhilosophers {
    private static class Philosopher implements Runnable {
        private final ReentrantLock leftFork;
        private final ReentrantLock rightFork;

        public Philosopher(ReentrantLock leftFork, ReentrantLock rightFork) {
            this.leftFork = leftFork;
            this.rightFork = rightFork;
        }

        @Override
        public void run() {
            try {
                while (true) {
//                doAction(": Thinking");
//                // DEAD LOCK
//                leftFork.lock();
//                doAction(": Picked up left fork");
//                rightFork.lock();
//                doAction(": Picked up right fork - eating");
//                doAction(": Put down right fork");
//                rightFork.unlock();
//                doAction(": Put down right fork");
//                leftFork.unlock();
//                doAction(": Put down left fork. Back to thinking");
                    if (leftFork.tryLock()) {
//                    doAction(": Picked up left fork");
                        if (rightFork.tryLock()) {
                            doAction(": Picked up right fork - eating");
//                        doAction(": Put down right fork");
                            rightFork.unlock();
                        }
//                    doAction(": Put down left fork. Back to thinking");
                        leftFork.unlock();
                    }
                }
            } catch (InterruptedException exception) {
                System.out.println("Exiting. Exception: " + exception);
            }
        }

        private void doAction(String action) throws InterruptedException {
            System.out.println(Thread.currentThread().getName() + " " + action);
            Thread.sleep(((int) (Math.random() * 100)));
        }
    }

    public static void main(String[] args) throws Exception {
        Philosopher[] philosophers = new Philosopher[5];
        ReentrantLock[] forks = new ReentrantLock[philosophers.length];

        for (int i = 0; i < forks.length; i++) {
            forks[i] = new ReentrantLock();
        }

        for (int i = 0; i < philosophers.length; i++) {
            ReentrantLock leftFork = forks[i];
            ReentrantLock rightFork = forks[(i + 1) % forks.length];
            philosophers[i] = new Philosopher(leftFork, rightFork);
            Thread t = new Thread(philosophers[i], "Philosopher " + (i + 1));
            t.start();
        }
    }
}
