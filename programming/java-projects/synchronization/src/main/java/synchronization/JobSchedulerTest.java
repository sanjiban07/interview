package synchronization;

import lombok.SneakyThrows;

import java.util.PriorityQueue;
import java.util.concurrent.*;

public class JobSchedulerTest {
    static class Job<T> implements Comparable<Job> {
        long startTs;
        Callable<T> task;
        Future<T> result;
        Object status;
        public Job(Callable<T> task, long delayMs) {
            this.task = task;
            this.startTs = System.currentTimeMillis() + delayMs;
            this.status = new Object();
        }

        public int compareTo(Job other) {
            return Long.compare(this.startTs, other.startTs);
        }
    }

    static class JobScheduler implements Runnable {
        private int maxJobs;
        private final ExecutorService executorService;
        private final PriorityQueue<Job> jobQueue;
        private final Object condition;

        public JobScheduler(ExecutorService executorService, int maxJobs) {
            this.executorService = executorService;
            jobQueue = new PriorityQueue<>();
            condition = new Object();
            this.maxJobs = maxJobs;
        }

        public void run() {
            System.out.println("Job scheduler started!!");

            try {
                while (true) {
                    Job job = jobQueue.peek();
                    if (job == null) {
                        System.out.println("No pending jobs. Job scheduler waiting!!");
                        synchronized (condition) {
                            condition.wait(1000);
                            continue;
                        }
                    }

                    if (job.startTs <= System.currentTimeMillis()) {
                        job.result = executorService.submit(job.task);
                        synchronized (job.status) {
                            job.status.notify();
                        }
                        maxJobs--;
                        jobQueue.poll();
                    }

                    if (maxJobs == 0) {
                        System.out.println("Job scheduler exiting.");
                        return;
                    }
                    if (jobQueue.peek() != null && jobQueue.peek().startTs > System.currentTimeMillis()) {
                        long timeToSleep = jobQueue.peek().startTs - System.currentTimeMillis();
                        System.out.println("Pending job count: " + jobQueue.size() + ". Job scheduler waiting for " + timeToSleep + "msec!!");
                        synchronized (condition) {
                            condition.wait(timeToSleep);
                        }
                    }
                }
            } catch (InterruptedException exception) {
                System.out.println("Job scheduler exiting. Exception: " + exception);
            }
        }

        public Job schedule(Callable task, long delayMs) {
            Job job = new Job(task, delayMs);
            jobQueue.add(job);
            synchronized (condition) {
                condition.notify();
            }
            System.out.println("New job added");
            return job;
        }
    }

    static class SampleJob implements Callable<String> {
        final String name;

        public SampleJob(String name) {
            this.name = name;
        }

        @Override
        public String call() throws Exception {
            for (int i = 0; i < 10; i++) {
//                System.out.println("Job running: " + name);
//                Thread.sleep(100);
            }
//            System.out.println("Job finished: " + name);
            return "Job returned: " + name;
        }
    }

    static class MonitorJob implements Runnable {
        private final Job job;

        MonitorJob(Job job) {
            this.job = job;
        }

        @SneakyThrows
        @Override
        public void run() {
            synchronized (job.status) {
                job.status.wait();
            }
            System.out.println("Job finished. Output: " + job.result.get());
        }
    }

    public static void main(String args[]) throws ExecutionException, InterruptedException {
        ExecutorService executors = Executors.newFixedThreadPool(2);
        JobScheduler jobScheduler = new JobScheduler(executors, 10);
        Thread jobSchedulerThread = new Thread(jobScheduler);
        jobSchedulerThread.start();
        Thread[] threads = new Thread[10];
        for (int i = 0; i < 10; i++) {
            threads[i] = new Thread(
                    new MonitorJob(
                        jobScheduler.schedule(new SampleJob("JOB_" + (i + 1)), 100 + 10 * (i/2))
                    )
            );
            threads[i].start();
        }
        for (int i = 0; i < 10; i++) {
            threads[i].join();
        }
        jobSchedulerThread.join();
        executors.shutdown();
    }
}

