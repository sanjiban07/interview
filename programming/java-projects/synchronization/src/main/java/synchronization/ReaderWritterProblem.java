package synchronization;

import lombok.SneakyThrows;

public class ReaderWritterProblem {
    private static class Message {
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

    private static class Reader implements Runnable {
        private Message message;
        private final String name;

        public Reader(Message message, String name) {
            this.message = message;
            this.name = name;
        }

        @SneakyThrows
        @Override
        public void run() {
            while (true) {
                System.out.println(name + ": waiting...");
                synchronized(message) {
                    message.wait();
                }
                if (message.getMsg().equalsIgnoreCase("msg_10")) {
                    System.out.println(name + ": final msg rcvd: " + message.getMsg());
                    return;
                } else {
                    System.out.println(name + ": msg rcvd: " + message.getMsg());
                }
            }
        }
    }

    private static class Writter implements Runnable {
        private int i;
        private Message message;
        public Writter(Message message) {
            i = 0;
            this.message = message;
        }

        @Override
        public void run() {
            while (i < 10) {
                if (i == 10) {
                    System.out.println("Writter: sending final: " + message.getMsg());
                } else {
                    System.out.println("Writter: sending: " + message.getMsg());
                }

                synchronized(message) {
                    message.setMsg("msg_" + ++i);
                    message.notifyAll();
                }
            }
        }
    }

    public static void main(String args[]) throws InterruptedException {
        Message message = new Message();
        Thread reader1 = new Thread(new Reader(message, "Reader_1"));
        Thread reader2 = new Thread(new Reader(message, "Reader_2"));
        Thread writter = new Thread(new Writter(message));
        reader1.start();
        reader2.start();
        writter.start();
        reader1.join();
        reader2.join();
        writter.join();
    }
}
