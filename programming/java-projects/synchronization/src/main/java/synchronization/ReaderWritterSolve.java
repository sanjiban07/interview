package synchronization;

import lombok.SneakyThrows;

import java.util.concurrent.Semaphore;

public class ReaderWritterSolve {
    private static class Message {
        private String msg;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }
    }

    private static class Reader implements Runnable {
        private Message message;
        private final String name;
        private final Semaphore semaphore;

        public Reader(Message message, String name, Semaphore semaphore) {
            this.message = message;
            this.name = name;
            this.semaphore = semaphore;
        }

        @SneakyThrows
        @Override
        public void run() {
            try {
                while (true) {
                    synchronized(message) {
                        message.wait();
                    }

                    System.out.println(name + ": msg rcvd: " + message.getMsg());
                    if (message.getMsg().equalsIgnoreCase("msg_10")) {
                        return;
                    }
                    semaphore.release();
                }
            } catch (Exception exception) {
                System.out.println(name + ": failed. Exiting. Exception: " + exception);
                throw exception;
            }
        }
    }

    private static class Writter implements Runnable {
        private int i;
        private Message message;
        private final Semaphore semaphore;
        public Writter(Message message, Semaphore semaphore) {
            this.semaphore = semaphore;
            i = 0;
            this.message = message;
        }

        @Override
        public void run() {
            try {
                while (i < 10) {
                    semaphore.acquire(2);
                    message.setMsg("msg_" + ++i);
                    if (i == 10) {
                        System.out.println("Writter: sending final: " + message.getMsg());
                    } else {
                        System.out.println("Writter: sending: " + message.getMsg());
                    }

                    synchronized(message) {
                        message.notifyAll();
                    }
                }
            } catch (Exception exception) {
                return;
            }
        }
    }

    public static void main(String args[]) throws InterruptedException {
        Message message = new Message();
        Semaphore semaphore = new Semaphore(2);
        Thread reader1 = new Thread(new Reader(message, "Reader_1", semaphore));
        Thread reader2 = new Thread(new Reader(message, "Reader_2", semaphore));
        Thread writter = new Thread(new Writter(message, semaphore));
        reader1.start();
        reader2.start();
        writter.start();
        reader1.join();
        reader2.join();
        writter.join();
    }
}
