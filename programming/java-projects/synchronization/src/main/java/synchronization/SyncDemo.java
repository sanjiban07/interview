package synchronization;

import lombok.SneakyThrows;

import java.util.concurrent.locks.ReentrantLock;

//class ThreadDemo extends Thread {
//    private static int _ID = 0;
//    private static int _running = 0;
//
//    private final String name;
//    private final int id;
//    private ReentrantLock mutex = new ReentrantLock();
////    private Semaphore semaphore = new Semaphore(1);
//
//    public ThreadDemo() {
//        this.id = ++_ID;
//        this.name = "Thread_" + id;
//    }
//
//    @Override
//    public void start() {
//        System.out.println(name + " started");
//        super.start();
//    }
//
//    @SneakyThrows
//    @Override
//    public void run() {
//        for (int i = 0; i < 10; i++) {
//            try {
//                mutex.lock();
////                semaphore.acquire();
//                System.out.println(name + ": " + i);
//            } finally {
//                mutex.unlock();
////                semaphore.release();
//            }
////            Thread.sleep(100);
//        }
//    }
//}


public class SyncDemo {
    private static class ThreadDemo extends Thread {
        private static int _ID = 0;
        private static int _running = 0;

        private final String name;
        private final int id;
        private ReentrantLock mutex = new ReentrantLock();

        public ThreadDemo() {
            this.id = _ID++;
            this.name = "Thread_" + id;
        }

        @Override
        public void start() {
            System.out.println(name + " started");
            super.start();
        }

        @SneakyThrows
        @Override
        public synchronized void run() {
            int i = 0;
            while (i < 10) {
//            if (mutex.tryLock()) {
//                if (id == _running) {
//                    System.out.println(name + ": " + i);
//                    _running = (_running + 1) % 2;
//                    i++;
//                } else {
//                    mutex.unlock();
//                }
//            } else {
//                continue;
//            }
//            mutex.lock();
                if (id == _running) {
                    System.out.println(name + ": " + i);
                    _running = (_running + 1) % 2;
                    i++;
                }
//            mutex.unlock();
            }
        }
    }

    public static void main(String args[]) throws InterruptedException {
        System.out.println("Main started!!");
        Thread thread1 = new ThreadDemo();
        Thread thread2 = new ThreadDemo();

        thread1.start();
        thread2.start();

        thread1.join();
        thread2.join();
        System.out.println("Main exited!!");
    }
}
