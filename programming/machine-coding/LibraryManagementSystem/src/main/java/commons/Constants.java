package commons;

import java.time.format.DateTimeFormatter;

public class Constants {
    public static final int MAX_ISSUE_BOOK_LIMIT = 2;
    public static final int MAX_ISSUE_DATE_LIMIT = 7;
    public static final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public static final double FINE_AMT_FOR_A_DAY = 2.0D;
}
