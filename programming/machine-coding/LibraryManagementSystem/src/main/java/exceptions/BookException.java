package exceptions;

public class BookException extends Throwable {
    private BookException(String msg) {
        super(msg);
    }

    private BookException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    public static BookException alreadyExists(String bookName, Throwable throwable) {
        return new BookException("Book '" + bookName + "'  already exists!!", throwable);
    }

    public static BookException doesntExists(String bookName) {
        return new BookException("Book '" + bookName + "'  doesn't exist!!");
    }
}
