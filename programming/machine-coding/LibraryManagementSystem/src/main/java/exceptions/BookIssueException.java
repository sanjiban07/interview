package exceptions;

import commons.Constants;

public class BookIssueException extends Throwable {
    private BookIssueException(String msg) {
        super(msg);
    }

    public BookIssueException(String message, Throwable cause) {
        super(message, cause);
    }

    public static BookIssueException bookLimitReached(String userName) {
        return new BookIssueException("User '" + userName + "' has already issued " + Constants.MAX_ISSUE_BOOK_LIMIT + " books!!");
    }

    public static BookIssueException dayLimitReached() {
        return new BookIssueException("Book can not be issued for more than " + Constants.MAX_ISSUE_DATE_LIMIT + " days!!");
    }

    public static BookIssueException invalidDateFormat(String dateStr, Exception exception) {
        return new BookIssueException("Provide date in dd/MM/yyyy only!!", exception);
    }

    public static BookIssueException bookNotIssuedByUser(String bookName, String userName) {
        return new BookIssueException("User " + userName + " has not issued '" + bookName + "!!");
    }

    public static BookIssueException bookAlreadyIssued(String userName, String bookName) {
        return new BookIssueException("User " + userName + " has already issued '" + bookName + "!!");
    }
}
