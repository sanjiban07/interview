package exceptions;

public class UserException extends Throwable {
    private UserException(String msg, Throwable throwable) {
        super(msg, throwable);
    }

    private UserException(String msg) {
        super(msg);
    }

    public static UserException alreadyExists(String userName, Throwable throwable) {
        return new UserException("User '" + userName + "'  already exists!!", throwable);
    }

    public static UserException doesntExists(String userName) {
        return new UserException("User '" + userName + "'  doesn't exist!!");
    }
}
