package models;

import lombok.Data;
import lombok.NonNull;

import java.util.List;

@Data
public class Book {
    private static int _ID = 0;
    public enum Status {
        AVAILABLE, NOT_AVAILABLE
    }
    private int id;
    private @NonNull String name;
    private @NonNull List<String> authors;
    private Status status;
    private double rating;
    private long peopleRatedCount;

    public Book(String name, List<String> authors) {
        if (authors == null || authors.isEmpty()) {
            throw new UnsupportedOperationException("Author list can't be null or empty!!");
        }

        this.name = name;
        this.authors = authors;
        this.status = Status.AVAILABLE;
        this.rating = 0;
        this.peopleRatedCount = 0;
        id = ++_ID;
    }

    @Override
    public String toString() {
        return "Id: " + id
               + "\nName: " + name
               + "\nAuthors: " + String.join(", ", authors)
               + "\nStatus: " + status
               + "\nRating: " + rating;
    }
}
