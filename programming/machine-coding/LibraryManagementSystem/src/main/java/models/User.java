package models;

import lombok.Data;
import lombok.NonNull;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Data

public class User {
    private @NonNull String name;
    private final Map<Book, LocalDate> issueMap;

    public User(@NonNull String name) {
        this.name = name;
        issueMap = new HashMap<>();
    }
}
