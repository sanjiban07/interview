package services;

import exceptions.UserException;
import models.Book;

import java.util.List;
import java.util.Optional;

public interface IBookService {
    Book addBook(String bookName, List<String> authors) throws UserException;
    Optional<Book> getBook(String bookName);
    List<Book> searchBook(String query);
}
