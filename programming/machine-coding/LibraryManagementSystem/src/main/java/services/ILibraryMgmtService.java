package services;

import exceptions.BookException;
import exceptions.BookIssueException;
import exceptions.UserException;

public interface ILibraryMgmtService {
    void issueBookToUser(String userName, String bookName, String dateStr, int duration) throws UserException, BookException, BookIssueException;
    double checkLateFine(String userName, String bookName, String dateStr) throws BookIssueException, UserException, BookException;
    void returnBook(String userName, String bookName) throws UserException, BookException, BookIssueException;
    void rateBook(String bookName, double rating) throws BookException;
}
