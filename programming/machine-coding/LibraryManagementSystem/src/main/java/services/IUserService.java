package services;

import exceptions.UserException;
import models.User;

import java.util.Optional;

public interface IUserService {
    Optional<User> getUser(String userName);
    User addUser(String userName) throws UserException;
}
