package services.impl;

import exceptions.UserException;
import models.Book;
import services.IBookService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class BookService implements IBookService {
    private final Map<String, Book> bookMap;

    public BookService() {
        bookMap = new HashMap<>();
    }

    public Optional<Book> getBook(String bookName) {
        return Optional.ofNullable(bookMap.getOrDefault(bookName.toUpperCase(), null));
    }

    public Book addBook(String bookName, List<String> authors) throws UserException {
        try {
            return bookMap.putIfAbsent(bookName.toUpperCase(), new Book(bookName, authors));
        } catch (UnsupportedOperationException exception) {
            throw UserException.alreadyExists(bookName, exception);
        }
    }

    public List<Book> searchBook(String query) {
        String queryUpperCase = query.toUpperCase();
        return bookMap.values().stream()
                .filter(book -> book.getName().toUpperCase().contains(queryUpperCase))
                .collect(Collectors.toList());
    }
}
