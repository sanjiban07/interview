package services.impl;

import commons.Constants;
import exceptions.BookException;
import exceptions.BookIssueException;
import exceptions.UserException;
import models.Book;
import models.User;
import services.ILibraryMgmtService;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

public class LibraryMgmtService implements ILibraryMgmtService {
    private final BookService bookService;
    private final UserService userService;

    public LibraryMgmtService(BookService bookService, UserService userService) {
        this.bookService = bookService;
        this.userService = userService;
    }

    public void issueBookToUser(String userName, String bookName, String dateStr, int duration) throws UserException, BookException, BookIssueException {
        LocalDate date;
        try {
            date = LocalDate.parse(dateStr, Constants.format);
        } catch (DateTimeParseException exception) {
            throw BookIssueException.invalidDateFormat(dateStr, exception);
        }

        if (duration < 0 || duration > Constants.MAX_ISSUE_DATE_LIMIT) {
            throw BookIssueException.dayLimitReached();
        }

        Optional<User> userOptional = userService.getUser(userName);
        if (!userOptional.isPresent()) {
            throw UserException.doesntExists(userName);
        }

        User user = userOptional.get();
        if (user.getIssueMap().size() > 1) {
            throw BookIssueException.bookLimitReached(userName);
        }

        Optional<Book> bookOptional = bookService.getBook(bookName);
        if (!bookOptional.isPresent()) {
            throw BookException.doesntExists(bookName);
        }

        if (bookOptional.get().getStatus() == Book.Status.NOT_AVAILABLE) {
            throw BookIssueException.bookAlreadyIssued(userName, bookName);
        }

        if (user.getIssueMap().containsKey(bookOptional.get())) {
            throw BookIssueException.bookAlreadyIssued(userName, bookName);
        }

        bookOptional.get().setStatus(Book.Status.NOT_AVAILABLE);
        user.getIssueMap().put(bookOptional.get(), date);
    }

    public double checkLateFine(String userName, String bookName, String dateStr) throws BookIssueException, UserException, BookException {
        LocalDate date;
        try {
            date = LocalDate.parse(dateStr, Constants.format);
        } catch (DateTimeParseException exception) {
            throw BookIssueException.invalidDateFormat(dateStr, exception);
        }

        Optional<User> userOptional = userService.getUser(userName);
        if (!userOptional.isPresent()) {
            throw UserException.doesntExists(userName);
        }

        Optional<Book> bookOptional = bookService.getBook(bookName);
        if (!bookOptional.isPresent()) {
            throw BookException.doesntExists(bookName);
        }

        Book book = bookOptional.get();
        User user = userOptional.get();
        Optional<LocalDate> issueDateOptional = Optional.ofNullable(user.getIssueMap().getOrDefault(book, null));
        if (!issueDateOptional.isPresent()) {
            throw BookIssueException.bookNotIssuedByUser(bookName, userName);
        }

        return ChronoUnit.DAYS.between(issueDateOptional.get(), date) * Constants.FINE_AMT_FOR_A_DAY;
    }

    public void returnBook(String userName, String bookName) throws UserException, BookException, BookIssueException {
        Optional<User> userOptional = userService.getUser(userName);
        if (!userOptional.isPresent()) {
            throw UserException.doesntExists(userName);
        }

        Optional<Book> bookOptional = bookService.getBook(bookName);
        if (!bookOptional.isPresent()) {
            throw BookException.doesntExists(bookName);
        }

        Book book = bookOptional.get();
        User user = userOptional.get();
        if (!user.getIssueMap().containsKey(book)) {
            throw BookIssueException.bookNotIssuedByUser(bookName, userName);
        }

        user.getIssueMap().remove(book);
        book.setStatus(Book.Status.AVAILABLE);
    }

    public void rateBook(String bookName, double rating) throws BookException {
        if (rating < 0 || rating > 10) {
            throw new UnsupportedOperationException("Rating should be between 0 and 10.");
        }

        Optional<Book> bookOptional = bookService.getBook(bookName);
        if (!bookOptional.isPresent()) {
            throw BookException.doesntExists(bookName);
        }

        Book book = bookOptional.get();
        double totalRating = book.getRating() * book.getPeopleRatedCount() + rating;
        book.setPeopleRatedCount(book.getPeopleRatedCount() + 1);
        book.setRating(totalRating / book.getPeopleRatedCount());
    }
}
