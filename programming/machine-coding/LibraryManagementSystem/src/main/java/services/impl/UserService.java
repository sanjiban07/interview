package services.impl;

import exceptions.UserException;
import models.User;
import services.IUserService;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class UserService implements IUserService {
    private final Map<String, User> userMap;

    public UserService() {
        this.userMap = new HashMap<>();
    }

    public Optional<User> getUser(String userName) {
        return Optional.ofNullable(userMap.getOrDefault(userName.toUpperCase(), null));
    }

    public User addUser(String userName) throws UserException {
        try {
            return userMap.putIfAbsent(userName.toUpperCase(), new User(userName));
        } catch (UnsupportedOperationException exception) {
            throw UserException.alreadyExists(userName, exception);
        }
    }
}
