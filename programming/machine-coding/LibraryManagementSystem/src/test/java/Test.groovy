import exceptions.BookIssueException
import models.Book
import services.impl.BookService
import services.impl.LibraryMgmtService
import services.impl.UserService
import spock.lang.Shared
import spock.lang.Specification

class Test extends Specification {
    @Shared BookService bookService
    @Shared UserService userService
    @Shared LibraryMgmtService libraryMgmtService
    def setupSpec() {
        bookService = new BookService()
        userService = new UserService()
        libraryMgmtService = new LibraryMgmtService(bookService, userService)
        // Add Users
        userService.addUser("reader_1")
        userService.addUser("reader_2")
        userService.addUser("reader_3")

        // Add books
        bookService.addBook("Beginning Linux Programming", ["Mathew Neil, Richard Stones"])
        bookService.addBook("Introduction to Algorithms", ["Thomas H. Cormen"])
        bookService.addBook("Java The Complete Reference", ["Herbert Schildt"])
        bookService.addBook("Game Theory: An Introduction", ["Steven Tadelis"])
        bookService.addBook("A Game of Thrones", ["George R. R. Martin"])
        bookService.addBook("The Winds of Winter", ["George R. R. Martin"])
    }

    def "test add books" () {
        // search a book:
        when:
        def bookList = bookService.searchBook("game")
        then:
        bookList.size() == 2
        bookList.get(0).name == "Game Theory: An Introduction"
        bookList.get(0).authors.size() == 1
        bookList.get(0).status == Book.Status.AVAILABLE
        bookList.get(1).name == "A Game of Thrones"
        bookList.get(1).authors.size() == 1
        bookList.get(1).status == Book.Status.AVAILABLE
    }

    def "test issue book"() {
        when:
        libraryMgmtService.issueBookToUser("reader_1", "A Game of Thrones", "01/03/2022", 5)
        then:
        bookService.searchBook("game of thrones").get(0).status == Book.Status.NOT_AVAILABLE
    }

    def "test reissue same book"() {
        when:
        libraryMgmtService.issueBookToUser("reader_2", "A Game of Thrones", "01/03/2022", 5)
        then:
        def exception = thrown(BookIssueException)
        exception.message == "User reader_2 has already issued 'A Game of Thrones!!"
    }

    def "check late fine"() {
        when:
        def fineAmount = libraryMgmtService.checkLateFine("reader_1", "a game of thrones", "10/03/2022")
        then:
        fineAmount == 18.0
    }

    def "reader_1 returns the book"() {
        when:
        libraryMgmtService.returnBook("reader_1", "a game of thrones")
        // reader_2 issues books
        libraryMgmtService.issueBookToUser("reader_2", "a game of thrones", "11/03/2022", 5)
        then:
        userService.getUser("reader_1").get().getIssueMap().size() == 0
        userService.getUser("reader_2").get().getIssueMap().size() == 1
    }

    def "test date limit check"() {
        when:
        libraryMgmtService.issueBookToUser("reader_2", "Java The Complete Reference", "11/03/2022", 5)
        libraryMgmtService.issueBookToUser("reader_2", "The Winds of Winter", "11/03/2022", 5)
        then:
        def exception = thrown(BookIssueException)
        exception.message == "User 'reader_2' has already issued 2 books!!"
    }
    def "test rate a book"() {
        when:
        libraryMgmtService.rateBook("a game of thrones", 5)
        libraryMgmtService.rateBook("a game of thrones", 10)
        then:
        bookService.searchBook("game of thrones").get(0).rating == 7.5
    }
}