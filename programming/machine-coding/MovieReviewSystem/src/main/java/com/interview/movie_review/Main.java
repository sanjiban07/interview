package com.interview.movie_review;

import com.interview.movie_review.exceptions.*;
import com.interview.movie_review.models.Movie;
import com.interview.movie_review.models.User;
import com.interview.movie_review.service.MovieService;
import com.interview.movie_review.service.UserService;

import java.util.Arrays;

public class Main {
    public static void main (String [] args) throws MovieAlreadyExists, UserNotFoundException, MultipleReviewException, MovieNotYetReleased, MovieNotFoundException {
        MovieReviewSystem movieReviewSystem = new MovieReviewSystem(new MovieService(), new UserService());
//        Onboard 10 movies onto your platform in 3 different years.
//        Add Movie("Don" released in Year 2006 for Genres “Action” & “Comedy”)
//        Add Movie("Tiger" released in Year 2008 for Genre “Drama”)
//        Add Movie("Padmaavat" released in Year 2006 for Genre “Comedy”)
//        Add Movie("Lunchbox-2" released in Year 2030 for Genre “Drama”)
//        Add Movie("Guru" released in Year 2006 for Genre “Drama”)
//        Add Movie("Metro" released in Year 2006 for Genre “Romance”)
        movieReviewSystem.addMovie("Don", 2006, Arrays.asList(Movie.Genre.ACTION, Movie.Genre.COMEDY));
        movieReviewSystem.addMovie("Tiger", 2008, Arrays.asList(Movie.Genre.DRAMA));
        movieReviewSystem.addMovie("Padmaavat", 2006, Arrays.asList(Movie.Genre.COMEDY));
        movieReviewSystem.addMovie("Lunchbox-2", 2030, Arrays.asList(Movie.Genre.DRAMA));
        movieReviewSystem.addMovie("Guru", 2006, Arrays.asList(Movie.Genre.DRAMA));
        movieReviewSystem.addMovie("Metro", 2006, Arrays.asList(Movie.Genre.ROMANCE));
//        Add User(“SRK”)
//        Add User(“Salman”)
//        Add User(“Deepika”)
        movieReviewSystem.addUser("SRK");
        movieReviewSystem.addUser("Salman");
        movieReviewSystem.addUser("Deepika");
//        Add Review(“SRK”, “Don”, 2)
//        Add Review(“SRK”, “Padmavaat”, 8)
//        Add Review(“Salaman”, “Don”, 5)
//        Add Review(“Deepika”, “Don”, 9)
//        Add Review(“Deepika”, “Guru”, 6)
//        Add Review(“SRK”,”Don”, 10)  - Exception multiple reviews not allowed
//        Add Review(“Deepika”, “Lunchbox-2”, 5) - Exception movie yet to be released
//        Add Review(“SRK”, “Tiger”, 5). Since ‘SRK’ has published 3 reviews, he is promoted to ‘critic’ now.
//        Add Review(“SRK”, “Metro”, 7)
        movieReviewSystem.addReview("SRK", "Don", 2);
        movieReviewSystem.addReview("SRK", "Padmaavat", 8);
        movieReviewSystem.addReview("Salman", "Don", 5);
        movieReviewSystem.addReview("Deepika", "Don", 9);
        movieReviewSystem.addReview("Deepika", "Guru", 6);
        try {
            movieReviewSystem.addReview("SRK","Don", 10);
        } catch (MultipleReviewException ex) {
            System.out.println(ex.getMessage());
        }
        try {
            movieReviewSystem.addReview("Deepika", "Lunchbox-2", 5);
        } catch (MovieNotYetReleased ex) {
            System.out.println(ex.getMessage());
        }
        movieReviewSystem.addReview("SRK", "Tiger", 5);
        movieReviewSystem.addReview("SRK", "Metro", 7);
//        List top 1 movie by review score in “2006” year:
//        Top in Year “2006”:
//        Output: Don - 16 ratings (sum of all ratings of Deepika, Salman & SRK)
//        List top 1 movie by review score in “Drama” genre:
//        Output: Guru - 6 ratings
        System.out.println(movieReviewSystem.topMoviesInGivenYear(1, User.Level.VIEWER, 2006).get(0).getName());
        System.out.println(movieReviewSystem.topMoviesInGivenGenre(1, User.Level.VIEWER, Movie.Genre.DRAMA).get(0).getName());
    }
}
