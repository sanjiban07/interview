package com.interview.movie_review;

import com.interview.movie_review.exceptions.*;
import com.interview.movie_review.models.Movie;
import com.interview.movie_review.models.User;
import com.interview.movie_review.service.MovieService;
import com.interview.movie_review.service.QueryService;
import com.interview.movie_review.service.ReviewService;
import com.interview.movie_review.service.UserService;

import java.util.List;

public class MovieReviewSystem {
    private MovieService movieService;
    private UserService userService;
    private ReviewService reviewService;
    private QueryService queryService;

    public MovieReviewSystem(MovieService movieService, UserService userService) {
        this.movieService = movieService;
        this.userService = userService;
        reviewService = new ReviewService(movieService, userService);
        queryService = new QueryService(reviewService);
    }

    public String addMovie(String name, int releaseYear, List<Movie.Genre> genres) throws MovieAlreadyExists {
        if (genres.isEmpty()) {
            throw new IllegalArgumentException("Genres can't be empty");
        }

        return movieService.addMovie(name, releaseYear, genres);
    }

    public String addUser(String name) {
        return userService.addUser(name);
    }

    public boolean addReview (
            String userName, String movieName, int score
    ) throws UserNotFoundException, MovieNotFoundException, MultipleReviewException, MovieNotYetReleased {
        return reviewService.addReview(userName, movieName, score);
    }

    public List<Movie> topMoviesInGivenYear(int N, User.Level level, int year) {
        return queryService.topMoviesInGivenYear(N, level, year);
    }

    public List<Movie> topMoviesInGivenGenre(int N, User.Level level, Movie.Genre genre) {
        return queryService.topMoviesInGivenGenre(N, level, genre);
    }

    public double avgReviewScoreInGivenYear(int year) {
        return queryService.avgReviewScoreInGivenYear(year);
    }

    public double avgReviewScoreInGivenGenre(Movie.Genre genre) {
        return queryService.avgReviewScoreInGivenGenre(genre);
    }
}