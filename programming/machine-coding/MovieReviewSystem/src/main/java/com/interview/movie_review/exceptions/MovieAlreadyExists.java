package com.interview.movie_review.exceptions;

public class MovieAlreadyExists extends Exception {
    public MovieAlreadyExists() {
        super("Movie already exists");
    }
}
