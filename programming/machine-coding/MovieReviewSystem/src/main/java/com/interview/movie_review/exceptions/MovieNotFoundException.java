package com.interview.movie_review.exceptions;

public class MovieNotFoundException extends Exception {
    public MovieNotFoundException (String movie) {
        super("Unable to find movie: " + movie);
    }
}
