package com.interview.movie_review.exceptions;

public class MovieNotYetReleased extends Exception {
    public MovieNotYetReleased() {
        super("Movie not yet released.");
    }
}
