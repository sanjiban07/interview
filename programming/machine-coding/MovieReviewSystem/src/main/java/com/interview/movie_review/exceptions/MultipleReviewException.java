package com.interview.movie_review.exceptions;

public class MultipleReviewException extends Exception {
    public MultipleReviewException() {
        super("Exception multiple reviews not allowed");
    }
}
