package com.interview.movie_review.exceptions;

public class UserNotFoundException extends Exception {
    public UserNotFoundException(String user) {
        super("Unable to find user: " + user);
    }
}
