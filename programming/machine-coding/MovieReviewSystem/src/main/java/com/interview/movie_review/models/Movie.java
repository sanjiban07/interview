package com.interview.movie_review.models;

import com.sun.istack.internal.NotNull;
import lombok.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
public class Movie {
    public enum Genre {
        ACTION, DRAMA, COMEDY, ROMANCE, HORROR, SCI_FI;
    }

    @Setter(AccessLevel.NONE)
    private String id;
    @NotNull
    @Setter(AccessLevel.NONE)
    private String name;
    @NotNull
    @Setter(AccessLevel.NONE)
    private List<Genre> genres;
    private int releaseYear;
    private List<Review> reviews;

    public Movie (String name, int releaseYear, List<Genre> genres) {
        this.name = name;
        this.genres = genres;
        this.releaseYear = releaseYear;
        id = UUID.nameUUIDFromBytes(name.getBytes()).toString();
        reviews = new ArrayList<Review>();
    }
}
