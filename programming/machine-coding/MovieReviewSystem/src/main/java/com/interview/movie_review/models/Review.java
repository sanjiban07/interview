package com.interview.movie_review.models;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
public class Review {
    @Setter(AccessLevel.NONE)
    private User user;
    @Setter(AccessLevel.NONE)
    private Movie movie;
    @Setter(AccessLevel.NONE)
    private int score;
    @Setter(AccessLevel.NONE)
    private User.Level level;
}
