package com.interview.movie_review.models;

import com.sun.istack.internal.NotNull;
import lombok.*;

import java.util.UUID;

@Data
public class User {
    public enum Level {
        CRITIC(2, 6), VIEWER(1, 0);

        private int weightage;
        private int threshold;

        public int getWeightage () {
            return weightage;
        }

        public int getThreshold() {
            return threshold;
        }

        Level (int weightage, int threshold) {
            this.weightage = weightage;
            this.threshold = threshold;
        }
    }

    @Setter(AccessLevel.NONE)
    private String id;
    @NotNull
    @Setter(AccessLevel.NONE)
    private String name;
    private Level level;
    private int reviewCnt;

    public User(String name) {
        this.name = name;
        id = UUID.nameUUIDFromBytes(name.getBytes()).toString();
        level = level.VIEWER;
        reviewCnt = 0;
    }
}
