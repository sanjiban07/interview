package com.interview.movie_review.service;

import com.interview.movie_review.exceptions.MovieAlreadyExists;
import com.interview.movie_review.exceptions.MovieNotFoundException;
import com.interview.movie_review.models.Movie;

import java.util.*;

public class MovieService {
    private HashMap<String, Movie> movies;

    public MovieService () {
        movies = new HashMap<>();
    }

    public String addMovie(String name, int releaseYear, List<Movie.Genre> genres) throws MovieAlreadyExists {
        Movie movie = new Movie(name, releaseYear, genres);
        if (movies.containsKey(movie.getId())) {
            throw new MovieAlreadyExists();
        }
        movies.put(movie.getId(), movie);
        return movie.getId();
    }

    public Movie getMovieByName(String movieName) throws MovieNotFoundException {
        String movieId = UUID.nameUUIDFromBytes(movieName.getBytes()).toString();
        Movie movie = movies.getOrDefault(movieId, null);
        if (movie == null) {
            throw new MovieNotFoundException(movieName);
        }

        return movie;
    }
}
