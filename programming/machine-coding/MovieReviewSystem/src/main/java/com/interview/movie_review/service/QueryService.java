package com.interview.movie_review.service;

import com.interview.movie_review.models.Movie;
import com.interview.movie_review.models.Review;
import com.interview.movie_review.models.User;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class QueryService {
    private ReviewService reviewService;

    public QueryService(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    public List<Movie> topMoviesInGivenYear(int N, User.Level level, int year) {
        Map<Movie, Integer> movieWiseRatings = new HashMap<>();
        reviewService.getReviewsInGivenYear(year).stream()
                .filter(review -> { return review.getLevel() == level; })
                .forEach(review -> {
                    movieWiseRatings.put(review.getMovie(), review.getScore() + movieWiseRatings.getOrDefault(review.getMovie(), 0));
                });

        return movieWiseRatings.entrySet().stream()
                .sorted(Map.Entry.<Movie, Integer>comparingByValue().reversed())
                .limit(N)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public List<Movie> topMoviesInGivenGenre(int N, User.Level level, Movie.Genre genre) {
        Map<Movie, Integer> movieWiseRatings = new HashMap<>();
        reviewService.getReviewsInGivenGenre(genre).stream()
                .filter(review -> { return review.getLevel() == level; })
                .forEach(review -> {
                    movieWiseRatings.put(review.getMovie(), review.getScore() + movieWiseRatings.getOrDefault(review.getMovie(), 0));
                });

        return movieWiseRatings.entrySet().stream()
                .sorted(Map.Entry.<Movie, Integer>comparingByValue().reversed())
                .limit(N)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }

    public double avgReviewScoreInGivenYear(int year) {
        return reviewService.getReviewsInGivenYear(year).stream()
                .mapToInt(Review::getScore).average().orElse(0D);
    }

    public double avgReviewScoreInGivenGenre(Movie.Genre genre) {
        return reviewService.getReviewsInGivenGenre(genre).stream()
                .mapToInt(Review::getScore).average().orElse(0D);
    }}
