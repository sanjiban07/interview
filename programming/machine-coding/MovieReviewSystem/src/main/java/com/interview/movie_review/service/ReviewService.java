package com.interview.movie_review.service;

import com.interview.movie_review.exceptions.MovieNotFoundException;
import com.interview.movie_review.exceptions.MovieNotYetReleased;
import com.interview.movie_review.exceptions.MultipleReviewException;
import com.interview.movie_review.exceptions.UserNotFoundException;
import com.interview.movie_review.models.Movie;
import com.interview.movie_review.models.Review;
import com.interview.movie_review.models.User;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ReviewService {
    private MovieService movieService;
    private UserService userService;
    List<Review> reviews;

    public ReviewService(MovieService movieService, UserService userService) {
        this.movieService = movieService;
        this.userService = userService;
        reviews = new ArrayList<>();
    }

    public boolean addReview(String userName, String movieName, int score) throws MovieNotFoundException, UserNotFoundException, MultipleReviewException, MovieNotYetReleased {
        Movie movie = movieService.getMovieByName(movieName);
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        if (movie.getReleaseYear() > currentYear) {
            throw new MovieNotYetReleased();
        }

        User user = userService.getUserByName(userName);
        Review review = new Review(user, movie, score * user.getLevel().getWeightage(), user.getLevel());
        Optional<Review> existingReview = reviews.stream().filter(r -> {
            return r.getUser() == user && r.getMovie() == movie && r.getLevel() == user.getLevel();
        }).findAny();

        if (existingReview.isPresent()) {
            throw new MultipleReviewException();
        }

        reviews.add(review);
        userService.increaseReviewCnt(user);
        return true;
    }

    public List<Review> getReviewsInGivenYear(int year) {
        return reviews.stream()
                .filter(review -> { return review.getMovie().getReleaseYear() == year; })
                .collect(Collectors.toList());
    }

    public List<Review> getReviewsInGivenGenre (Movie.Genre genre) {
        return reviews.stream()
                .filter(review -> { return review.getMovie().getGenres().contains(genre); })
                .collect(Collectors.toList());
    }
}
