package com.interview.movie_review.service;

import com.interview.movie_review.exceptions.UserNotFoundException;
import com.interview.movie_review.models.User;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

public class UserService {
    private HashMap<String, User> users;

    public UserService() {
        users = new HashMap<>();
    }

    public String addUser(String name) {
        User user = new User(name);
        users.put(user.getId(), user);
        return user.getId();
    }

    public User getUserByName(String userName) throws UserNotFoundException {
        String userId = UUID.nameUUIDFromBytes(userName.getBytes()).toString();
        User user = users.getOrDefault(userId, null);
        if (user == null) {
            throw new UserNotFoundException(userName);
        }

        return user;
    }

    public void increaseReviewCnt(User user) {
        int reviewCnt = user.getReviewCnt() + 1;
        User.Level level = Stream.of(User.Level.values()).filter(lvl -> lvl.getThreshold() <= reviewCnt).findFirst().get();
        user.setReviewCnt(user.getReviewCnt() + 1);
        user.setLevel(level);
    }
}
