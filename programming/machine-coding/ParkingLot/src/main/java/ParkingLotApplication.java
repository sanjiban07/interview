import exceptions.InvalidCommandException;
import services.ICommandExecutor;
import services.impl.ParkingLotService;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ParkingLotApplication {
    public static void main(String args[]) throws IOException, InvalidCommandException {
        System.out.println("Args: " + String.join(", ", args));
        System.out.println("CWD: " + System.getProperty("user.dir"));
        String path = "src/test/resources";
        String inputFileName = "input.txt";
        ICommandExecutor parkingLotService = new ParkingLotService();
        FileReader fr = new FileReader(path + "/" + inputFileName);
        BufferedReader br = new BufferedReader(fr);
        String line= null;
        while((line=br.readLine())!=null) {
            System.out.println(parkingLotService.execute(line));
        }
    }
}
