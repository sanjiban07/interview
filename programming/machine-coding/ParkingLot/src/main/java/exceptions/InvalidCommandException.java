package exceptions;

public class InvalidCommandException extends Exception {
    public InvalidCommandException(String command) {
        super("Given command '" + command + "' is not valid");
    }

    public InvalidCommandException(Throwable cause) {
        super(cause);
    }

    public InvalidCommandException(String message, Throwable cause) {
        super(message, cause);
    }
}
