package models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;

@AllArgsConstructor
@Data
public class Car {
    @NonNull private String registrationNumber;
    @NonNull private String color;

    @Override
    public String toString() {
        return registrationNumber + ' ' + color;
    }
}
