package models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Arrays;

@Data
@AllArgsConstructor
public class Command {
    private String cmd;
    private String[] args;

    @Override
    public String toString() {
        return cmd + " " + String.join(" ", args);
    }
}