package models;

import java.util.ArrayList;
import java.util.List;

public class ParkingLot {
    private final List<Slot> slots;
    private final int size;

    public List<Slot> getSlots() {
        return slots;
    }

    public int getSize() {
        return size;
    }

    public ParkingLot(int size) {
        this.size = size;
        slots = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            slots.add(new Slot(i + 1));
        }
    }
}
