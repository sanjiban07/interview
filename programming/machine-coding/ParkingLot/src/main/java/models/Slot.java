package models;

import lombok.Data;

@Data
public class Slot {
    private int no;
    private Car car;

    public Slot(int no) {
        this.no = no;
    }

    @Override
    public String toString() {
        return car == null? Integer.toString(no) : no + " " + car;
    }
}