package services;

import exceptions.InvalidCommandException;
import models.Command;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public interface ICommandExecutor {
    default String execute(String command) throws InvalidCommandException {
        if (StringUtils.isBlank(command)) {
            throw new InvalidCommandException(command);
        }
        String[] commandSplit = command.trim().replaceAll("\\s+", " ").split(" ");
        String cmd = commandSplit[0].toLowerCase();
        String[] args = Arrays.copyOfRange(commandSplit, 1, commandSplit.length);
        return execute(new Command(cmd, args));
    }

    String execute(Command command) throws InvalidCommandException;
}
