package services.impl;

import commons.Constants;
import exceptions.InvalidCommandException;
import exceptions.ParkingException;
import models.Command;
import models.ParkingLot;
import services.ICommandExecutor;

import java.util.Arrays;
import java.util.List;

public class ParkingLotService implements ICommandExecutor {
    private ParkingLot parkingLot;
    private List<ICommandExecutor> commandExecutors;

    private String createParkingLot(int size) throws ParkingException {
        if (size < 1 || size > Constants.MAX_PARKING_LOT_SIZE) {
            throw new ParkingException("Parking lot size should be between 1 and " + Constants.MAX_PARKING_LOT_SIZE);
        }
        parkingLot = new ParkingLot(size);
        commandExecutors = Arrays.asList(new ParkingService(parkingLot), new QueryService(parkingLot));
        return "Created a parking lot with " + size + " slots";
    }

    @Override
    public String execute(Command command) throws InvalidCommandException {
        try {
            switch (command.getCmd()) {
                case "create_parking_lot":
                    return createParkingLot(Integer.parseInt(command.getArgs()[0]));
                default:
                    if (this.parkingLot == null) {
                        throw new InvalidCommandException("Parking lot is not yet initialized.");
                    }

                    for (ICommandExecutor executor: commandExecutors) {
                        String output = executor.execute(command);
                        if (output != null) {
                            return output;
                        }
                    }

                    throw new InvalidCommandException(command.getCmd());
            }
        } catch (Exception exception) {
            throw new InvalidCommandException("Failed to execute command: '" + command + "'.", exception);
        }
    }
}
