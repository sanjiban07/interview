package services.impl;

import exceptions.InvalidCommandException;
import exceptions.ParkingException;
import lombok.NonNull;
import models.Car;
import models.Command;
import models.ParkingLot;
import models.Slot;
import services.ICommandExecutor;

import java.util.stream.Collectors;

public class ParkingService implements ICommandExecutor {
    @NonNull
    private final ParkingLot parkingLot;

    public ParkingService(@NonNull ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    @Override
    public String execute(Command command) throws InvalidCommandException {
        try {
            switch (command.getCmd()) {
                case "park":
                    if (command.getArgs().length < 2) {
                        throw new ParkingException("Invalid registration number or color of the car");
                    }

                    return parkCar(command.getArgs()[0], command.getArgs()[1]);
                case "leave":
                    if (command.getArgs().length < 1) {
                        throw new ParkingException("Invalid registration number or color of the car");
                    }

                    try {
                        int slotNo = Integer.parseInt(command.getArgs()[0]);
                        return removeParkedCar(slotNo);
                    } catch (NumberFormatException exception) {
                        throw new ParkingException("Invalid slot number", exception);
                    }
                case "status":
                    return status();
            }
        } catch (ParkingException exception) {
            throw new InvalidCommandException("Failed to execute command", exception);
        }
        return null;
    }

    private String parkCar(String registrationNumber, String color) throws ParkingException {
        Car car = new Car(registrationNumber, color);
        for(Slot slot: parkingLot.getSlots()) {
            if (slot.getCar() == null) {
                slot.setCar(car);
                return "Allocated slot number: " + slot.getNo();
            }
        }

        return "Sorry, parking lot is full";
    }

    private String removeParkedCar(int slotNumber) throws ParkingException {
        if (slotNumber <= 0 || slotNumber > parkingLot.getSize()) {
            throw new ParkingException("Slot number should be between 1 and " + parkingLot.getSize());
        }

        if (parkingLot.getSlots().get(slotNumber - 1).getCar() == null) {
            throw new ParkingException("No car present in given slot: " + slotNumber);
        }

        parkingLot.getSlots().get(slotNumber - 1).setCar(null);
        return "Slot number " + slotNumber + " is free";
    }

    private String status() {
        StringBuilder builder = new StringBuilder();
        return "Slot No. Registration No Colour\n"
                + parkingLot.getSlots().stream()
                    .filter(slot -> slot.getCar() != null)
                    .map(Slot::toString)
                    .collect(Collectors.joining("\n"));
    }
}
