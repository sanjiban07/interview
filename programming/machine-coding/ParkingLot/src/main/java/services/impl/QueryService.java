package services.impl;

import exceptions.InvalidCommandException;
import exceptions.ParkingException;
import lombok.NonNull;
import models.Command;
import models.ParkingLot;
import services.ICommandExecutor;

import java.util.stream.Collectors;

public class QueryService implements ICommandExecutor {
    @NonNull
    private final ParkingLot parkingLot;

    public QueryService(@NonNull ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    @Override
    public String execute(Command command) throws InvalidCommandException {
        try {
            if (command.getArgs().length < 1) {
                throw new ParkingException("Missing argument in command: " + command);
            }

            switch (command.getCmd()) {
                case "registration_numbers_for_cars_with_colour":
                    return findCarsByColor(command.getArgs()[0]);
                case "slot_numbers_for_cars_with_colour":
                    return findSlotsByColor(command.getArgs()[0]);
                case "slot_number_for_registration_number":
                    return findSlotByRegistrationNumber(command.getArgs()[0]);
            }
        } catch (Exception exception) {
            throw new InvalidCommandException("Failed to execute command", exception);
        }
        return null;
    }

    private String findCarsByColor(String color) {
        return parkingLot.getSlots().stream()
                .filter(slot ->
                        slot.getCar() != null
                        && slot.getCar().getColor().equalsIgnoreCase(color)
                ).map(slot -> slot.getCar().getRegistrationNumber())
                .collect(Collectors.joining(", "));
    }

    private String findSlotsByColor(String color) {
        return parkingLot.getSlots().stream()
                .filter(slot ->
                        slot.getCar() != null
                        && slot.getCar().getColor().equalsIgnoreCase(color)
                ).map(slot -> Integer.toString(slot.getNo()))
                .collect(Collectors.joining(", "));
    }

    private String findSlotByRegistrationNumber(String registrationNumber) {
        return parkingLot.getSlots().stream()
                .filter(slot ->
                        slot.getCar() != null
                        && slot.getCar().getRegistrationNumber().equalsIgnoreCase(registrationNumber)
                ).map(slot -> Integer.toString(slot.getNo()))
                .findAny()
                .orElse("Not found");
    }

}
