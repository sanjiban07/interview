package com.flipkart.coflip;

import com.flipkart.coflip.models.User;
import com.flipkart.coflip.models.VaccinationCentre;
import com.flipkart.coflip.models.VaccinationSlot;
import com.flipkart.coflip.models.Vaccine;
import com.flipkart.coflip.service.UserService;
import com.flipkart.coflip.service.VaccinationCentreService;
import com.flipkart.coflip.service.VaccineBookingService;
import com.flipkart.coflip.service.VaccineService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CoFlipApplication {
    private UserService userService;
    private VaccineService vaccineService;
    private VaccinationCentreService vaccinationCentreService;
    private VaccineBookingService bookingService;
    public CoFlipApplication() {
        userService = new UserService();
        vaccineService = new VaccineService();
        vaccinationCentreService = new VaccinationCentreService();
        bookingService = new VaccineBookingService();
    }

    public Vaccine registerVaccine(String name) throws Exception {
        return vaccineService.registerVaccine(name);
    }

    public User registerUser(String name, String preferredVaccineStr) throws Exception {
        Vaccine preferredVaccine = vaccineService.getByName(preferredVaccineStr);
        return userService.registerUser(name, preferredVaccine);
    }

    public VaccinationCentre registerCentre(String id, Map<String, Integer> inventory) throws Exception {
        Map<Vaccine, Integer> vaccineInventory = new HashMap<>();
        for (Map.Entry<String, Integer> entry: inventory.entrySet()) {
            vaccineInventory.put(vaccineService.getByName(entry.getKey()), entry.getValue());
        }

        return vaccinationCentreService.registerCentre(id, vaccineInventory);
    }

    public List<VaccinationCentre> getCentres(String userName) throws Exception {
        User user = userService.getByName(userName);
        return vaccinationCentreService.getCentersWithVaccineType(user.getPreferredVaccine());
    }

    public VaccinationSlot bookslot(String userName, String centreId) throws Exception {
        User user = userService.getByName(userName);
        VaccinationCentre centre = vaccinationCentreService.getById(centreId);
        if (!centre.isVaccineAvailable(user.getPreferredVaccine())) {
            return null;
        }

        vaccinationCentreService.decreaseVaccineCount(centre, user.getPreferredVaccine());
        try {
            return bookingService.registerSlot(user, centre);
        } catch (Exception exception) {
            vaccinationCentreService.increaseVaccineCount(centre, user.getPreferredVaccine());
            throw exception;
        }
    }

    public Map<String, Object> getStats() {
        System.out.println(bookingService.getAllRegisteredUsers());
        return new HashMap<String, Object>(){{
            put("registered users", String.join(", ", bookingService.getAllRegisteredUsers()));
            put("vaccination centers", vaccinationCentreService.getAllAvailableVaccineCentres());
        }};
    }
}
