package com.flipkart.coflip;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;

public class CoFlipMain {
    public static void main (String [] args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        CoFlipApplication coFlipApplication = new CoFlipApplication();
        System.out.println("Vaccine added: " + (coFlipApplication.registerVaccine("COVAX")));
        System.out.println("Vaccine added: " + (coFlipApplication.registerVaccine("COVISHIELD")));
        System.out.println("User added: " + (coFlipApplication.registerUser("User1", "COVAX")));
        System.out.println("User added: " + (coFlipApplication.registerUser("User2", "COVISHIELD")));
        System.out.println("Centre added: " + (coFlipApplication.registerCentre("Center1", new HashMap<String, Integer>() {{
            put("COVISHIELD", 5);
            put("COVAX", 1);
        }})));
        System.out.println("Centre added: " + (coFlipApplication.registerCentre("Center2", new HashMap<String, Integer>() {{
            put("COVAX", 10);
        }})));
        System.out.println("Get centres: " + (coFlipApplication.getCentres("User1")));
        System.out.println("Book for user1: " + (coFlipApplication.bookslot("User1", "Center1")));
        System.out.println("Book for user2: " + (coFlipApplication.bookslot("User2", "Center2")));
        System.out.println("Stats: " + (coFlipApplication.getStats()));
    }
}
