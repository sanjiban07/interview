package com.flipkart.coflip;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.issue.models.response.IssueEligibilityResponse;

import java.util.Map;

public class TestMain {
    public static void main(String args[]) throws JsonProcessingException {
        String str = "{\n" +
                "    \"6332\": {\n" +
                "        \"issueId\": 6332,\n" +
                "        \"issueName\": \"Customer Return Requested\",\n" +
                "        \"entityType\": \"ORDER_ITEM\",\n" +
                "        \"entityInfoMap\": {\n" +
                "            \"12259395054155500000\": {\n" +
                "                \"type\": \"RETURN\",\n" +
                "                \"id\": \"12259395054155500000\",\n" +
                "                \"reason\": \"UNIT_UNDER_POLICY\",\n" +
                "                \"eligible\": true,\n" +
                "                \"eligibilityData\": {\n" +
                "                    \"returnCreationLastDate\": 1631816999999,\n" +
                "                    \"returnEligibleMessage\": \"UNIT_UNDER_POLICY\",\n" +
                "                    \"returnMessageReasonGroup\": \"ITEM_WITHIN_RETURN_POLICY_AND_DURATION\",\n" +
                "                    \"showExtensionMessage\": false\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "    }\n" +
                "}";
        ObjectMapper mapper = new ObjectMapper();
        System.out.println(mapper.writeValueAsString(mapper.readValue(str, IssueEligibilityResponse.class)));
    }
}
