package com.flipkart.coflip.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

@Getter
@AllArgsConstructor
public class VaccinationCentre {
    private String centreId;
    private Map<Vaccine, Integer> vaccineInventory;

    public int getVaccineCount (Vaccine vaccine) {
        return vaccineInventory.getOrDefault(vaccine, 0);
    }

    public boolean isVaccineAvailable (Vaccine vaccine) {
        return getVaccineCount(vaccine) > 0;
    }

    @Override
    public String toString() {
        return "VaccinationCentre{" +
                "centreId='" + centreId + '\'' +
                '}';
    }
}
