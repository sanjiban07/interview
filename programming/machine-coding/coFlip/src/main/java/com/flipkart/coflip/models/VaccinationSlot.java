package com.flipkart.coflip.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class VaccinationSlot {
    private User user;
    private Vaccine vaccine;
    private VaccinationCentre centre;

    @Override
    public String toString() {
        return "VaccinationSlot{" +
                "user=" + user +
                ", vaccine=" + vaccine +
                ", centre=" + centre +
                '}';
    }
}
