package com.flipkart.coflip.models;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Vaccine {
    private String name;

    @Override
    public String toString() {
        return "Vaccine{" +
                "name='" + name + '\'' +
                '}';
    }
}
