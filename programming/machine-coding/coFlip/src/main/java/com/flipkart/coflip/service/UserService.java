package com.flipkart.coflip.service;

import com.flipkart.coflip.models.User;
import com.flipkart.coflip.models.Vaccine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class UserService {
    private Map<String, User> userRepo;

    public UserService() {
        userRepo = new HashMap<String, User>();
    }

    public User registerUser(String name, Vaccine preferredVaccine) throws Exception {
        User user = userRepo.getOrDefault(name, null);
        if (user != null) {
            throw new Exception("User with name '" + name + "' is already registered.");
        }

        user = new User(name, preferredVaccine);
        userRepo.put(name, user);
        return user;
    }

    public Collection<User> getAll () {
        return userRepo.values();
    }

    public User getByName(String name) throws Exception {
        User user = userRepo.getOrDefault(name, null);
        if (user == null) {
            throw new Exception("User with name '" + name + "' is not yet registered.");
        }

        return user;
    }
}
