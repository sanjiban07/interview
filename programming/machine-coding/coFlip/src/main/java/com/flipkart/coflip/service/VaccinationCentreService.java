package com.flipkart.coflip.service;

import com.flipkart.coflip.models.VaccinationCentre;
import com.flipkart.coflip.models.VaccinationSlot;
import com.flipkart.coflip.models.Vaccine;

import java.util.*;
import java.util.stream.Collectors;

public class VaccinationCentreService {
    private Map<String, VaccinationCentre> vaccinationCentreRepo;

    public VaccinationCentreService() {
        vaccinationCentreRepo = new HashMap<>();
    }

    public VaccinationCentre registerCentre (String centreId, Map<Vaccine, Integer> vaccineInventory) throws Exception {
        VaccinationCentre centre = vaccinationCentreRepo.getOrDefault(centreId, null);
        if (centre != null) {
            throw new Exception("Vaccination centre with id '" + centreId + "' is already present.");
        } else {
            centre = new VaccinationCentre(centreId, vaccineInventory);
            vaccinationCentreRepo.put(centreId, centre);
        }

        return centre;
    }

    public Collection<VaccinationCentre> getAll () {
        return vaccinationCentreRepo.values();
    }

    public List<VaccinationCentre> getCentersWithVaccineType (Vaccine vaccine) throws Exception {
        return vaccinationCentreRepo.values().stream()
                .filter(centre -> centre.isVaccineAvailable(vaccine))
                .sorted((c1, c2) -> Integer.compare(c2.getVaccineCount(vaccine), c1.getVaccineCount(vaccine)))
                .collect(Collectors.toList());
    }

    public VaccinationCentre getById(String id) throws Exception {
        VaccinationCentre centre = vaccinationCentreRepo.getOrDefault(id, null);
        if (centre == null) {
            throw new Exception("Vaccination Centre with id '" + id + "' is not yet registered.");
        }

        return centre;
    }

    public void decreaseVaccineCount (VaccinationCentre centre, Vaccine vaccine) {
        centre.getVaccineInventory().put(vaccine, centre.getVaccineInventory().get(vaccine) - 1);
    }

    public void increaseVaccineCount (VaccinationCentre centre, Vaccine vaccine) {
        centre.getVaccineInventory().put(vaccine, centre.getVaccineInventory().get(vaccine) + 1);
    }

    public Collection<String> getAllAvailableVaccineCentres () {
        Collection<String> result = new ArrayList<>();
        vaccinationCentreRepo.values().forEach(centre -> {
            centre.getVaccineInventory().forEach((vaccine, count)-> {
                result.add(String.format("id: %s, vaccine: %s, count: %d", centre.getCentreId(), vaccine.getName(), count));
            });
        });

        return result;
    }
}
