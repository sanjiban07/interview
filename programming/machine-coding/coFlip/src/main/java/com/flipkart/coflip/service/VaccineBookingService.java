package com.flipkart.coflip.service;

import com.flipkart.coflip.models.User;
import com.flipkart.coflip.models.VaccinationCentre;
import com.flipkart.coflip.models.VaccinationSlot;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class VaccineBookingService {
    private Map<String, VaccinationSlot> vaccinationSlotRepo;

    public VaccineBookingService () {
        vaccinationSlotRepo = new HashMap<>();
    }

    public VaccinationSlot registerSlot (User user, VaccinationCentre centre) throws Exception {
        if (hasAlreadyBooked(user.getName())) {
            throw new Exception("User '" + user.getName() + "' is already registered.");
        }

        VaccinationSlot slot = new VaccinationSlot(user, user.getPreferredVaccine(), centre);
        vaccinationSlotRepo.put(user.getName(), slot);
        return slot;
    }

    public boolean hasAlreadyBooked (String userName) {
        return vaccinationSlotRepo.containsKey(userName);
    }

    public Set<String> getAllRegisteredUsers () {
        return vaccinationSlotRepo.keySet();
    }
}
