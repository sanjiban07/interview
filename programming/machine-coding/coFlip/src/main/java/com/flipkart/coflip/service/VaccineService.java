package com.flipkart.coflip.service;

import com.flipkart.coflip.models.User;
import com.flipkart.coflip.models.Vaccine;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class VaccineService {
    private Map<String, Vaccine> vaccineRepo;

    public VaccineService() {
        vaccineRepo = new HashMap<>();
    }

    public Vaccine registerVaccine(String name) throws Exception {
        Vaccine vaccine = vaccineRepo.getOrDefault(name, null);
        if (vaccine != null) {
            throw new Exception("Vaccine with name '" + name + "' is already registered.");
        } else {
            vaccine = new Vaccine(name);
            vaccineRepo.put(name, vaccine);
        }

        return vaccine;
    }

    public Collection<Vaccine> getAll () {
        return vaccineRepo.values();
    }

    public Vaccine getByName(String name) throws Exception {
        Vaccine vaccine = vaccineRepo.getOrDefault(name, null);
        if (vaccine == null) {
            throw new Exception("Vaccine with name '" + name + "' is not yet registered.");
        }

        return vaccine;
    }
}
