package org.example.newsfeed;

import org.example.newsfeed.service.impl.NewsFeedService;
import org.example.newsfeed.service.impl.UserService;

public class NewsFeedApplication {
    public static void main(String args[]) {
        NewsFeedService newsFeedService = new NewsFeedService();
        UserService userService = new UserService();
        userService.trySignup("lucious");
        userService.trySignup("albus");
        userService.trySignup("tom");
        userService.tryLogin("tom");
        newsFeedService.showNewsFeed(userService.getCurrentUser());
        newsFeedService.post(userService.getCurrentUser(), "I am going to be the darkest dark wizard of all time");
        newsFeedService.post(userService.getCurrentUser(), "I am lord Voldemort btw 3:");
        userService.tryLogin("lucious");
        newsFeedService.showNewsFeed(userService.getCurrentUser());
        newsFeedService.upvote(1);
        userService.follow("tom", userService.getCurrentUser());
        newsFeedService.reply(001, userService.getCurrentUser(), "I am with you dark lord!");
        userService.tryLogin("albus");
        newsFeedService.showNewsFeed(userService.getCurrentUser());
        newsFeedService.post(userService.getCurrentUser(), "Happiness can be found, even in the darkest of times, if one only remembers to turn on the light");
        userService.follow("tom", userService.getCurrentUser());
        newsFeedService.downvote(1);
        newsFeedService.downvote(2);
        newsFeedService.reply(002, userService.getCurrentUser(), "LOL!");
        newsFeedService.showNewsFeed(userService.getCurrentUser());

    }
}
