package org.example.newsfeed.models;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Getter
public class Post implements Comparable<Post> {
    private static long _ID = 0;
    private static final String _FORMAT = "%{6}sid: {0}\n%{6}s({1} upvotes, {2} downvotes)\n%{6}s{3}\n%{6}s{4}\n%{6}s{5}";

    private final long id;
    private final @NonNull User user;
    private final @NonNull String content;
    private final Date date;
    private final List<Reply> replies;
    @Setter
    long upvotes, downvotes;
    protected int spaceCount;

    @Builder
    public Post(User user, String content) {
        this.id = ++_ID;
        this.upvotes = downvotes = 0;
        this.user = user;
        this.content = content;
        this.date = new Date();
        this.replies = new ArrayList<>();
        this.spaceCount = 2;
    }

    @Override
    public String toString() {
        String formattedString = MessageFormat.format(_FORMAT, id, upvotes, downvotes, user.getUserName(), content, date, spaceCount);
        formattedString = String.format(formattedString, "", "", "", "", "");
        if (replies.size() == 0) {
            return formattedString;
        }

        return formattedString + "\n" + replies.stream().map(Reply::toString).collect(Collectors.joining("\n"));
    }


//    @Override
//    public String toString() {
//        String thisPost = "Post{" +
//                "id=" + id +
//                ", user=" + user +
//                ", content='" + content + '\'' +
//                ", date=" + date +
//                ", upvotes=" + upvotes +
//                ", downvotes=" + downvotes +
//                ", spaceCount=" + spaceCount +
//                '}';
//        return thisPost + "\n" + replies.stream().map(Reply::toString).collect(Collectors.joining("\n"));
//    }
//
    @Override
    public int compareTo(Post other) {
        return date.after(other.getDate())? 1 : -1;
    }
}
