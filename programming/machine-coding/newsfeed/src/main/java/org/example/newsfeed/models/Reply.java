package org.example.newsfeed.models;

import lombok.Getter;

@Getter
public class Reply extends Post {
    private final Post parent;

    public Reply(User user, String content, Post parent) {
        super(user, content);
        this.parent = parent;
        this.spaceCount = parent.spaceCount + 2;
    }
}
