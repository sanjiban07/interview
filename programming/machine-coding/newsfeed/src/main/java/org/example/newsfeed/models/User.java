package org.example.newsfeed.models;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.util.ArrayList;
import java.util.List;

@Getter
public class User {
    private @NonNull String userName;
    private List<User> following;

    @Builder
    public User(@NonNull String userName) {
        this.userName = userName;
        this.following = new ArrayList<>();
    }

    @Override
    public String toString() {
        return userName;
    }
}
