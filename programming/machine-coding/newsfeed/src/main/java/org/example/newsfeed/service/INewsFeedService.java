package org.example.newsfeed.service;

import org.example.newsfeed.models.User;

public interface INewsFeedService {
    boolean post(User user, String content);

    boolean reply(long parentId, User user, String content);
}
