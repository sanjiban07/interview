package org.example.newsfeed.service;

import org.example.newsfeed.models.User;

public interface IUserService {
    boolean trySignup(String userName);

    boolean tryLogin(String userName);

    User getCurrentUser();
}
