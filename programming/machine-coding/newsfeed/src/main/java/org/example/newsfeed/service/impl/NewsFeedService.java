package org.example.newsfeed.service.impl;

import org.example.newsfeed.models.Post;
import org.example.newsfeed.models.Reply;
import org.example.newsfeed.models.User;
import org.example.newsfeed.service.INewsFeedService;

import java.util.HashMap;
import java.util.Map;


public class NewsFeedService implements INewsFeedService {
    private Map<Long, Post> posts;

    public NewsFeedService() {
        this.posts = new HashMap<>();
    }

    @Override
    public boolean post(User user, String content) {
        Post post = Post.builder()
                .content(content)
                .user(user)
                .build();

        posts.put(post.getId(), post);
        return true;
    }

    @Override
    public boolean reply(long parentId, User user, String content) {
        Post parentPost = posts.getOrDefault(parentId, null);
        if (parentPost == null) return false;
        parentPost.getReplies().add(new Reply(user, content, parentPost));
        return true;
    }

    public boolean upvote(long postId) {
        Post post = posts.getOrDefault(postId, null);
        if (post == null) return false;
        post.setUpvotes(post.getUpvotes() + 1);
        return true;
    }

    public boolean downvote(long postId) {
        Post post = posts.getOrDefault(postId, null);
        if (post == null) return false;
        post.setDownvotes(post.getDownvotes() + 1);
        return true;
    }

    public void showNewsFeed(User user) {
        System.out.println("==============================");
        posts.values().stream()
                .sorted((post1, post2) -> {
                    boolean user1IsFollowing = user.getFollowing().contains(post1.getUser());
                    boolean user2IsFollowing = user.getFollowing().contains(post2.getUser());
                    if (user1IsFollowing && user2IsFollowing) {
                        return post1.compareTo(post2);
                    } else if (user1IsFollowing) {
                        return 1;
                    } else if (user2IsFollowing) {
                        return -1;
                    } else {
                        return post1.compareTo(post2);
                    }
                }).forEach(post -> {
                    System.out.println(post);
                });
    }
}