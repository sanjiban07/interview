package org.example.newsfeed.service.impl;

import org.example.newsfeed.models.User;
import org.example.newsfeed.service.IUserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UserService implements IUserService {
    private List<User> users;
    private User currentUser;

    public UserService() {
        users = new ArrayList<User>();
        currentUser = null;
    }

    public boolean trySignup(final String userName) {
        boolean exists = users.stream().anyMatch(user -> user.getUserName().equalsIgnoreCase(userName));
        if (exists) return false;
        users.add(User.builder().userName(userName).build());
        return true;
    }

    public boolean tryLogin(String userName) {
        Optional<User> userOptional = users.stream().filter(usr -> usr.getUserName().equalsIgnoreCase(userName)).findAny();
        if (!userOptional.isPresent()) return false;
        currentUser = userOptional.get();
        return true;
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public boolean follow(String userName, User user) {
        Optional<User> userOptional = users.stream().filter(usr -> usr.getUserName().equalsIgnoreCase(userName)).findAny();
        if (!userOptional.isPresent()) return false;
        user.getFollowing().add(userOptional.get());
        return true;
    }
}
