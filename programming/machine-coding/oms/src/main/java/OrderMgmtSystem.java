import pojo.Order;
import pojo.Restaurant;
import service.IOrderMgmtService;
import service.IRestaurantService;
import service.IUserService;

import java.util.Arrays;
import java.util.HashMap;

public class OrderMgmtSystem {
    static String mask(String text) {
        if ((text.startsWith("http://") || text.startsWith("https://")) && text.contains("/chat/")) {
            String[] textSplit = text.split("/chat/");
            return "/chat/" + textSplit[1].split("\\?")[0];
        }

        return text;
    }

    public static void main(String[] args) {
        String text = "http://app-prod.chatcx.nm.flipkart.com/chat/sessionId/SES159055943965850331?limit=0";
        System.out.println(text + ": " + mask(text));
        text = "http://app-prod.chatcx.nm.flipkart.com/chat/chatId/SES159055943965850331?limit=0";
        System.out.println(text + ": " + mask(text));
        text = "ASDASDASD";
        System.out.println(text + ": " + mask(text));

//        IRestaurantService restaurantService = null;
//        IOrderMgmtService orderService = null;
//        IUserService userService = null;
////        u1, u2, .. u5
//        userService.add("u1");
//        userService.add("u2");
//        userService.add("u3");
//        userService.add("u4");
//        userService.add("u5");
//        r1 > i2 20, i3 30, cap > 3
//        r2 > i2 22, cap > 4
//        r3 > i1 10, i3 28, cap > 2
//        Restaurant r1 = restaurantService.add("r1", new HashMap<>() {{
//            put("i2", 20);
//            put("i3", 30);
//        }}, 3);
//        Restaurant r2 = restaurantService.add("r2", new HashMap<>() {{
//            put("i2", 22);
//        }}, 4);
//        Restaurant r3 = restaurantService.add("r1", new HashMap<>() {{
//            put("i1", 20);
//            put("i3", 28);
//        }}, 2);
//        restaurantService.updatePrice(r3.getId(), "i1", 50);
////        o1 > i2, i3
////        o2> i2
////        o3 > i2, i3
////        o4 > i2, i3
////        o5 > i1, i2
////        o6 > i1
////        o7 > i2, i1
//        Order o1 = orderService.placeOrder("u1", Arrays.asList("i2", "i3"));
//        Order o2 = orderService.placeOrder("u2", Arrays.asList("i2"));
//        Order o3 = orderService.placeOrder("u3", Arrays.asList("i2", "i3"));
//        Order o4 = orderService.placeOrder("u4", Arrays.asList("i2", "i3"));
//        Order o5 = orderService.placeOrder("u5", Arrays.asList("i1", "i2"));
//        Order o6 = orderService.placeOrder("u1", Arrays.asList("i2"));
//        Order o7 = orderService.placeOrder("u2", Arrays.asList("i1", "i2"));
//        orderService.dispatchOrder(o2.getOrderId());
//        Order o8 = orderService.placeOrder("u4", Arrays.asList("i2", "i3"));

    }
}
