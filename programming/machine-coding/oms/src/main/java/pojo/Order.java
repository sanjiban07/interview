package pojo;

import lombok.Data;

import java.util.List;

@Data
public class Order {
    private String orderId;
    private List<Item> items;
    private String restaurantId;
    private String userId;
}
