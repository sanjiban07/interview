package pojo;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Data
@Builder
public class Restaurant {
    private final String id;
    private String name;
    private Map<String, Item> menu;
    private int maxOrdersCount;
    private int liveOrdersCount;
}
