package service;

import pojo.Order;

import java.util.List;

public interface IOrderMgmtService {
    Order placeOrder(String userId, List<String> items);
    void dispatchOrder(String orderId);
}
