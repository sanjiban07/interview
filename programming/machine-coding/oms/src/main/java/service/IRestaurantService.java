package service;

import java.util.Map;

public interface IRestaurantService {
    String add(String restaurantName, Map<String, Double> items, int maxOrdersCount);
//    Optional<Restaurant> getRestaurant(String restaurantId);
//    Optional<Restaurant> placeOrder(List<String> itemNames);
    void updatePrice(String restaurantId, String itemName, double newPrice);
}
