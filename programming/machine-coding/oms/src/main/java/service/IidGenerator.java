package service;

public interface IidGenerator {
    void register(String prefix);
    String getId(String prefix);
}
