package service.impl;

import service.IidGenerator;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class IDGenerator implements IidGenerator {
    private static final DateFormat DATE_PREFIX_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");

    private Map<String, Long> lastProcessedOrder;
    public IDGenerator() {
        this.lastProcessedOrder = new HashMap<>();
    }

    public void register(String prefix) {
        lastProcessedOrder.put(prefix, 0L);
    }

    public String getId(String prefix) {
        Long lastProcessedOrderId = lastProcessedOrder.getOrDefault(prefix, null);
        if (Objects.isNull(lastProcessedOrderId)) {
            throw new IllegalArgumentException("Prefix not found");
        }

        String newIdPrefix = getIdPrefix();
        if (lastProcessedOrderId.toString().startsWith(newIdPrefix)) {
            lastProcessedOrder.put(prefix, (lastProcessedOrderId + 1));
            return prefix + (lastProcessedOrderId + 1);
        }

        long newId = (Long.parseLong(newIdPrefix) * 10000);
        this.lastProcessedOrder.put(prefix, newId);
        return prefix + (Long.parseLong(newIdPrefix) * 10000);
    }

    private String getIdPrefix() {
        return DATE_PREFIX_FORMAT.format(new Date());
    }
}
