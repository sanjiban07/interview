package service.impl;

import pojo.Item;
import pojo.Restaurant;
import service.IRestaurantService;
import service.IidGenerator;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class RestaurantService implements IRestaurantService {
    private static final String ID_PREFIX = "RES";
    private final IidGenerator idGenerator;
    private final Map<String, Restaurant> restaurantMap;

    public RestaurantService(IidGenerator idGenerator) {
        this.idGenerator = idGenerator;
        this.restaurantMap = new HashMap<>();
        this.idGenerator.register(ID_PREFIX);
    }

    @Override
    public String add(String restaurantName, Map<String, Double> items, int maxOrdersCount) {
        String restaurantId = this.idGenerator.getId(ID_PREFIX);
        this.restaurantMap.put(
                restaurantId,
                Restaurant.builder()
                    .id(restaurantId)
                        .name(restaurantName)
                        .liveOrdersCount(0)
                        .maxOrdersCount(maxOrdersCount)
                        .menu(items.entrySet().stream().collect(
                                Collectors.toMap(
                                        (entry) -> entry.getKey(),
                                        (entry) -> new Item(entry.getKey(), entry.getValue())))
                        )
                    .build()
        );
        return restaurantId;
    }

    @Override
    public void updatePrice(String restaurantId, String itemName, double newPrice) {
        Restaurant restaurant = getRestaurantById(restaurantId);
//        Item = restaurant.getMenu().ge)
    }

    private Restaurant getRestaurantById(String restaurantId) {
        Restaurant restaurant = restaurantMap.getOrDefault(restaurantId, null);
        if (restaurant == null) {
            throw new IllegalArgumentException("Invalid restaurant ID: " + restaurantId);
        }
        return restaurant;
    }
}
