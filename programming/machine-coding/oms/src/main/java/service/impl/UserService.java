package service.impl;

import pojo.User;
import service.IUserService;
import service.IidGenerator;

import java.util.HashMap;
import java.util.Map;

public class UserService implements IUserService {
    private static final String ID_PREFIX = "ACC";
    private final IidGenerator idGenerator;
    private final Map<String, User> userMap;

    public UserService(IidGenerator idGenerator) {
        this.userMap = new HashMap<>();
        this.idGenerator = idGenerator;
        this.idGenerator.register(ID_PREFIX);
    }

    @Override
    public String add(String userName) {
        String userId = this.idGenerator.getId(ID_PREFIX);
        userMap.put(
                userId,
                User.builder()
                    .id(userId)
                    .name(userName)
                    .build()
        );

        return userId;
    }
}
