import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Environment;
import resource.RulesetResource;

public class RuleServerApplication extends Application<Configuration> {
    public static void main(String args[]) throws Exception {
        System.out.println("Hello World");
        new RuleServerApplication().run(args);
    }

    public void run(Configuration configuration, Environment environment) throws Exception {
        Injector injector = Guice.createInjector();
        environment.jersey().register(injector.getInstance(RulesetResource.class));
    }
}
