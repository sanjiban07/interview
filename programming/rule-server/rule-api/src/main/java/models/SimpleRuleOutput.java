package models;

import rule.engine.models.RuleOutput;

public class SimpleRuleOutput<V extends Comparable> extends RuleOutput<V, SimpleRuleOutput> {
    public int compareTo(SimpleRuleOutput other) {
        return getValue().compareTo(other.getValue());
    }
}
