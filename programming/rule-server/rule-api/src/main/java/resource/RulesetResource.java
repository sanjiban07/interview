package resource;

import com.google.inject.Inject;
import com.wordnik.swagger.annotations.ApiParam;
import models.SimpleRuleOutput;
import org.hibernate.validator.constraints.NotEmpty;
import rule.engine.RuleExecutionHandler;
import rule.engine.combiner.ICombiner;
import rule.engine.exception.RuleSetDoesnotExistException;
import rule.engine.models.EvaluationStrategy;
import rule.engine.models.Rule;
import rule.engine.models.RuleSet;
import rule.engine.repository.impl.BasicRulesRepository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Path("/api/v1/ruleset")
public class RulesetResource {
    private final RuleExecutionHandler<Map, SimpleRuleOutput> executionHandler;
    private final BasicRulesRepository rulesRepository;

    @Inject
    public RulesetResource() {
        this.rulesRepository = new BasicRulesRepository();
        this.executionHandler = new RuleExecutionHandler(rulesRepository, SimpleRuleOutput::new);
    }

    @PUT
    @Path("{context}")
    @Produces(MediaType.APPLICATION_JSON)
    public String create(@PathParam("context") String context,
                         @NotNull @Valid @QueryParam("combiner") ICombiner.Type combiner,
                         @ApiParam List<Rule> rules
    ) {
        try {
            return "Ruleset added: " + rulesRepository.addRuleSet(context, combiner.name(), rules);
        } catch (RuleSetDoesnotExistException exception) {
            return exception.getMessage();
        }
    }

    @POST
    @Path("{context}/add-rules")
    public String addRules(
            @PathParam("context") @NotEmpty String context,
            @ApiParam List<Rule> rules) {
        try {
            RuleSet ruleSet = rulesRepository.getRuleSet(context);
            ruleSet.getRules().addAll(rules);
            return "Rules added to ruleset: " + ruleSet;
        } catch (RuleSetDoesnotExistException exception) {
            return exception.getMessage();
        }
    }

    @GET
    @Path("{context}")
    @Produces("application/json")
    public Response get(@PathParam("context") @NotEmpty String context) throws RuleSetDoesnotExistException {
        try {
            RuleSet ruleSet = rulesRepository.getRuleSet(context);
            return Response.ok(ruleSet).build();
        } catch (RuleSetDoesnotExistException exception) {
            return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
        }
    }

    @DELETE
    @Path("{context}")
    @Produces("application/json")
    public RuleSet delete(@PathParam("context") @NotEmpty String context) throws RuleSetDoesnotExistException {
        return rulesRepository.removeRuleSet(context);
    }

    @POST
    @Path("{context}/execute-all")
    public Response executeAll(
            @PathParam("context") @NotEmpty String context,
            @ApiParam Map ruleInput,
            @QueryParam("strategy") EvaluationStrategy evaluationStrategy
    ) throws RuleSetDoesnotExistException {
        try {
            List<SimpleRuleOutput> output = executionHandler.executeAll(ruleInput, context, evaluationStrategy);
            return Response.ok(output, MediaType.APPLICATION_JSON).build();
        } catch (RuleSetDoesnotExistException exception) {
            return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
        } catch (Exception exception) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Failed to execute").build();
        }
    }

    @POST
    @Path("{context}/execute")
    public Response execute(
            @PathParam("context") @NotEmpty String context,
            @ApiParam Map ruleInput,
            @QueryParam("strategy") EvaluationStrategy evaluationStrategy
    ) throws RuleSetDoesnotExistException {
        try {
            Optional<SimpleRuleOutput> output = executionHandler.execute(ruleInput, context, evaluationStrategy);
            if (output.isPresent()) {
                return Response.ok(output, MediaType.APPLICATION_JSON).build();
            } else {
                return Response.status(Response.Status.NO_CONTENT).build();
            }
        } catch (RuleSetDoesnotExistException exception) {
            return Response.status(Response.Status.BAD_REQUEST).entity(exception.getMessage()).build();
        } catch (Exception exception) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity("Failed to execute").build();
        }
    }
}
