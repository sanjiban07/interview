package rule.engine;

import lombok.Getter;
import rule.engine.combiner.CombinerFactory;
import rule.engine.combiner.ICombiner;
import rule.engine.constants.Constants;
import rule.engine.evaluator.IRuleEvaluator;
import rule.engine.evaluator.impl.MvelRuleEvaluator;
import rule.engine.exception.RuleEvaluationException;
import rule.engine.exception.RuleSetDoesnotExistException;
import rule.engine.models.EvaluationStrategy;
import rule.engine.models.Rule;
import rule.engine.models.RuleOutput;
import rule.engine.models.RuleSet;
import rule.engine.repository.IRulesRepository;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Supplier;

public class RuleExecutionHandler<RuleInp, RuleOutp extends RuleOutput> {
    @Getter
    private final IRulesRepository rulesRepository;
    @Getter
    private final CombinerFactory<RuleOutp> combinerFactory;
    private final IRuleEvaluator<RuleInp, RuleOutp> evaluator;
    private final ExecutorService executorService;

    public RuleExecutionHandler(IRulesRepository rulesRepository, Supplier<RuleOutp> supplier) {
        this.rulesRepository = rulesRepository;
        this.combinerFactory = new CombinerFactory<>();
        this.evaluator = new MvelRuleEvaluator<>(supplier);
        this.executorService = Executors.newFixedThreadPool(Constants.THREAD_POOL_SIZE);
    }

    public Optional<RuleOutp> execute(RuleInp ruleInput, String context, EvaluationStrategy strategy) throws RuleEvaluationException, ExecutionException, InterruptedException, RuleSetDoesnotExistException {
        RuleSet ruleSet = rulesRepository.getRuleSet(context);
        ICombiner<RuleOutp> combiner = combinerFactory.getCombiner(ruleSet.getCombinerType());
        return combiner.combine(executeAll(ruleInput, context, strategy));
    }

    public Optional<RuleOutp> execute(RuleInp ruleInput, String context, String ruleName) throws RuleEvaluationException, RuleSetDoesnotExistException {
        RuleSet ruleSet = rulesRepository.getRuleSet(context);
        Optional<Rule> ruleOptional = ruleSet.getRules().stream()
                .filter(r -> r.getName().equalsIgnoreCase(ruleName))
                .findAny();
        if (!ruleOptional.isPresent()) {
            return Optional.empty();
        }

        return Optional.ofNullable(evaluator.evaluate(ruleInput, ruleOptional.get()));
    }

    public List<RuleOutp> executeAll(RuleInp ruleInput, String context, EvaluationStrategy strategy) throws RuleEvaluationException, ExecutionException, InterruptedException, RuleSetDoesnotExistException {
        RuleSet ruleSet = rulesRepository.getRuleSet(context);
        return executeSync(ruleInput, ruleSet.getRules(), strategy);
    }

    private List<RuleOutp> executeSync(RuleInp ruleInput, List<Rule> rules, EvaluationStrategy strategy) throws RuleEvaluationException {
        return evaluator.evaluate(ruleInput, rules, strategy);
    }

    private CompletableFuture<List<RuleOutp>> executeAsync(RuleInp ruleInput, List<Rule> rules, EvaluationStrategy strategy) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return evaluator.evaluate(ruleInput, rules, strategy);
            } catch (RuleEvaluationException e) {
                return Collections.emptyList();
            }
        }, executorService);
    }
}
