package rule.engine.combiner;

import rule.engine.combiner.impl.BestMatchCombiner;
import rule.engine.combiner.impl.FirstMatchCombiner;
import rule.engine.combiner.impl.FirstSuccessCombiner;
import rule.engine.combiner.impl.WorstMatchCombiner;
import rule.engine.exception.RuleEvaluationException;
import rule.engine.models.RuleOutput;

import java.util.HashMap;
import java.util.Map;

public class CombinerFactory<RuleOutp extends RuleOutput> {
    private final Map<String, ICombiner<RuleOutp>> map;

    public CombinerFactory() {
        this.map = new HashMap<String, ICombiner<RuleOutp>>() {{
            put(ICombiner.Type.BEST.name(), new BestMatchCombiner<RuleOutp>());
            put(ICombiner.Type.WORST.name(), new WorstMatchCombiner<RuleOutp>());
            put(ICombiner.Type.FIRST.name(), new FirstMatchCombiner<RuleOutp>());
            put(ICombiner.Type.FIRST_SUCCESS.name(), new FirstSuccessCombiner<>());
        }};
    }

    public void registerCombiner(String type, ICombiner combiner) {
        map.put(type, combiner);
    }

    public ICombiner<RuleOutp> getCombiner(ICombiner.Type type) throws RuleEvaluationException {
        return getCombiner(type.name().toUpperCase());
    }

    public ICombiner<RuleOutp> getCombiner(String type) throws RuleEvaluationException {
        type = type.toUpperCase();
        ICombiner<RuleOutp> combiner = map.getOrDefault(type, null);
        if (combiner == null) {
            throw new RuleEvaluationException("Failed to find combiner type '" + type + "'.");
        }

        return combiner;
    }
}
