package rule.engine.combiner;

import rule.engine.models.RuleOutput;

import java.util.List;
import java.util.Optional;

public interface ICombiner<RuleOutp extends RuleOutput> {
    Optional<RuleOutp> combine(List<RuleOutp> outputs);

    enum Type {
        BEST, WORST, FIRST, FIRST_SUCCESS
    }
}
