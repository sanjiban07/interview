package rule.engine.combiner.impl;

import rule.engine.combiner.ICombiner;
import rule.engine.models.RuleOutput;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class WorstMatchCombiner<RuleOutp extends RuleOutput> implements ICombiner<RuleOutp> {
    @Override
    public Optional<RuleOutp> combine(List<RuleOutp> outputs) {
        if (outputs == null) {
            return Optional.empty();
        }

        return outputs.stream().filter(Objects::nonNull).min(RuleOutp::compareTo);
    }
}
