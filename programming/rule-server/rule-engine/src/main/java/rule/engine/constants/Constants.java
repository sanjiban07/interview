package rule.engine.constants;

public final class Constants {
    public static final String RuleInp = "RuleInp";
    public static final String OUT = "out";
    public static final int THREAD_POOL_SIZE = 10;
}
