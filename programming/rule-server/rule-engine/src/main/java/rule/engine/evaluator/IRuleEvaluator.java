package rule.engine.evaluator;

import rule.engine.exception.RuleEvaluationException;
import rule.engine.models.EvaluationStrategy;
import rule.engine.models.Rule;
import rule.engine.models.RuleOutput;

import java.util.List;

public interface IRuleEvaluator<RuleInp, RuleOutp extends RuleOutput> {
    // Evaluate single rule
    RuleOutp evaluate(RuleInp ruleInput, Rule rule) throws RuleEvaluationException;

    // Evaluate multiple rules
    List<RuleOutp> evaluate(RuleInp ruleInput, List<Rule> rules, EvaluationStrategy strategy) throws RuleEvaluationException;

    void addImport(Class clazz);
}
