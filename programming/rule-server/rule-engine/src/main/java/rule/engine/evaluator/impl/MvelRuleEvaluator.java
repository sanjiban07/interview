package rule.engine.evaluator.impl;

import org.mvel2.MVEL;
import org.mvel2.ParserContext;
import rule.engine.evaluator.IRuleEvaluator;
import rule.engine.exception.RuleEvaluationException;
import rule.engine.models.EvaluationStrategy;
import rule.engine.models.Rule;
import rule.engine.models.RuleOutput;

import java.io.Serializable;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static rule.engine.constants.Constants.OUT;
import static rule.engine.constants.Constants.RuleInp;

public class MvelRuleEvaluator<RuleInp, RuleOutp extends RuleOutput> implements IRuleEvaluator<RuleInp, RuleOutp> {
    private final Supplier<RuleOutp> supplier;
    private ParserContext context;

    public MvelRuleEvaluator(Supplier<RuleOutp> supplier) {
        this.context = new ParserContext();
        this.supplier = supplier;
    }

    @Override
    public RuleOutp evaluate(RuleInp ruleInput, Rule rule) throws RuleEvaluationException {
        boolean isSuccess = evaluateRuleExpression(rule, ruleInput);
        RuleOutp output = supplier.get();
        output.setSuccess(isSuccess);
        output.setReason(rule.getName());
        if (rule.getPostRuleAction() == null) {
            return output;
        }

        return evaluateRuleAction(isSuccess, rule, ruleInput, output);
    }

    @Override
    public List<RuleOutp> evaluate(RuleInp ruleInput, List<Rule> rules, EvaluationStrategy strategy) throws RuleEvaluationException {
        try {
            List<RuleOutp> outputs = new ArrayList<>();
            for (Rule rule : getActiveRules(rules)) {
                RuleOutp output = evaluate(ruleInput, rule);
                outputs.add(output);
                if (
                        (strategy == EvaluationStrategy.STOP_ON_FIRST_SUCCESS && output.isSuccess())
                                || (strategy == EvaluationStrategy.STOP_ON_FIRST_FAILURE && !output.isSuccess())
                ) {
                    break;
                }
            }

            return outputs;
        } catch (Exception exception) {
            throw new RuleEvaluationException("Failed to evaluate rules.", exception);
        }
    }

    @Override
    public void addImport(Class clazz) {
        if (!context.hasImport(clazz.getName())) {
            context.addImport(clazz);
        }
    }

    private List<Rule> getActiveRules(List<Rule> rules) throws RuleEvaluationException {
        rules = rules.stream()
                .filter(Objects::nonNull)
                .filter(Rule::isActive)
                .collect(Collectors.toList());
        if (rules.isEmpty()) {
            throw new RuleEvaluationException("No active rule for given context '" + context + "'.");
        }

        return rules;
    }

    private boolean evaluateRuleExpression(Rule rule, RuleInp ruleInput) throws RuleEvaluationException {
        try {
            Map<String, Object> ruleInputMap = new HashMap<>();
            ruleInputMap.put(RuleInp, ruleInput);
            if (rule.getCompiledExpression() == null) {
                rule.setCompiledExpression(MVEL.compileExpression(rule.getExpression(), context));
            }

            return (Boolean) MVEL.executeExpression(rule.getCompiledExpression(), ruleInputMap);
        } catch (Exception exception) {
            throw new RuleEvaluationException("Error in evaluating expression for rule '" + rule.getName() + "'", exception);
        }
    }

    private RuleOutp evaluateRuleAction(boolean isSuccess, Rule rule, RuleInp ruleInput, RuleOutp output) throws RuleEvaluationException {
        String action = isSuccess ? rule.getPostRuleAction().getOnSuccess() : rule.getPostRuleAction().getOnFailure();
        if (action == null || action.trim().isEmpty()) {
            return output;
        }

        try {
            Map<String, Object> ruleInputMap = new HashMap<>();
            ruleInputMap.put(RuleInp, ruleInput);
            ruleInputMap.put(OUT, output);
            Serializable compiledAction;
            if (isSuccess && rule.getPostRuleAction().getCompiledOnSuccess() == null) {
                compiledAction = MVEL.compileExpression(action, context);
                rule.getPostRuleAction().setCompiledOnSuccess(compiledAction);
            } else if (!isSuccess && rule.getPostRuleAction().getCompiledOnFailure() == null) {
                compiledAction = MVEL.compileExpression(action, context);
                rule.getPostRuleAction().setCompiledOnFailure(compiledAction);
            } else {
                compiledAction = isSuccess ?
                        rule.getPostRuleAction().getCompiledOnSuccess()
                        : rule.getPostRuleAction().getCompiledOnFailure();
            }

            MVEL.executeExpression(compiledAction, ruleInputMap);
            return output;
        } catch (Exception exception) {
            throw new RuleEvaluationException("Error in execution post action for rule '" + rule.getName() + "'", exception);
        }
    }
}