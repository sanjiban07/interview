package rule.engine.exception;

public class RuleSetDoesnotExistException extends Throwable {
    public RuleSetDoesnotExistException(String msg) {
        super(msg);
    }
}
