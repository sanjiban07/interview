package rule.engine.exception;

public class VariableEvaluationException extends RuntimeException {

    public VariableEvaluationException(String message) {
        super(message);
    }

}
