package rule.engine.models;

public enum EvaluationStrategy {
    EVALUATE_ALL, STOP_ON_FIRST_FAILURE, STOP_ON_FIRST_SUCCESS
}