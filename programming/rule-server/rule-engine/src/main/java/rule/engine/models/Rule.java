package rule.engine.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Rule {
    private String name;
    private String expression;
    private boolean isActive;
    private int priority;
    private PostRuleAction postRuleAction;
    @JsonIgnore
    private Serializable compiledExpression;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class PostRuleAction {
        private String onSuccess, onFailure;
        @JsonIgnore
        private Serializable compiledOnSuccess, compiledOnFailure;
    }
}
