package rule.engine.models;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public abstract class RuleOutput<V, T> implements Comparable<T> {
    @NonNull
    private V value;
    private String reason;
    private boolean success;
    public RuleOutput(V value, String reason, boolean success) {
        this.value = value;
        this.reason = reason;
        this.success = success;
    }
}
