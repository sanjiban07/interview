package rule.engine.models;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class RuleSet {
    private String context;
    private String combinerType;
    private List<Rule> rules;

    @Override
    public String toString() {
        return "RuleSet{" +
                "context='" + context + '\'' +
                ", combinerType='" + combinerType + '\'' +
                '}';
    }
}
