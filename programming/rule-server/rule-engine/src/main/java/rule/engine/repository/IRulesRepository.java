package rule.engine.repository;

import rule.engine.exception.RuleSetDoesnotExistException;
import rule.engine.models.RuleSet;

public interface IRulesRepository {
    RuleSet getRuleSet(String context) throws RuleSetDoesnotExistException;
}
