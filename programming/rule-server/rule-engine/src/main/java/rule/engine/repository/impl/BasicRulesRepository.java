package rule.engine.repository.impl;

import rule.engine.exception.RuleSetDoesnotExistException;
import rule.engine.models.Rule;
import rule.engine.models.RuleSet;
import rule.engine.repository.IRulesRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BasicRulesRepository implements IRulesRepository {
    protected final Map<String, RuleSet> rulesMap;

    public BasicRulesRepository() {
        rulesMap = new HashMap<>();
    }

    @Override
    public RuleSet getRuleSet(String context) throws RuleSetDoesnotExistException {
        RuleSet ruleSet = rulesMap.getOrDefault(context, null);
        if (ruleSet == null || ruleSet.getRules() == null) {
            throw new RuleSetDoesnotExistException("Invalid rule context '" + context + "'.");
        }

        return ruleSet;
    }

    public void addRuleSet(RuleSet ruleSet) {
        rulesMap.put(ruleSet.getContext(), ruleSet);
    }

    public synchronized RuleSet addRuleSet(String context, String combinerType, List<Rule> rules) throws RuleSetDoesnotExistException {
        RuleSet ruleSet = rulesMap.getOrDefault(context, null);
        if (ruleSet != null) {
            throw new RuleSetDoesnotExistException("Already exists ruleset: " + ruleSet);
        }

        ruleSet = RuleSet.builder()
                .context(context)
                .combinerType(combinerType.toUpperCase())
                .rules(rules)
                .build();

        rulesMap.put(context, ruleSet);
        return ruleSet;
    }

    public RuleSet removeRuleSet(String context) {
        return rulesMap.remove(context);
    }
}
