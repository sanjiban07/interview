import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.TestCase;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import org.junit.Test;
import rule.engine.RuleExecutionHandler;
import rule.engine.exception.RuleEvaluationException;
import rule.engine.exception.RuleSetDoesnotExistException;
import rule.engine.models.EvaluationStrategy;
import rule.engine.models.Rule;
import rule.engine.models.RuleOutput;
import rule.engine.repository.impl.BasicRulesRepository;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static rule.engine.combiner.ICombiner.Type.FIRST_SUCCESS;

public class TestRuleEngine extends TestCase {
    RuleExecutionHandler<Map, SimpleRuleOutput> executionHandler;
    ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    @Override
    protected void setUp() throws Exception {
        Rule ruleEnum, rule1, rule2, rule3;
        rule1 = mapper.readValue("{\"name\":\"Rule_1\",\"expression\":\"RuleInp != null && RuleInp.name != null && RuleInp.name.equals('1')\",\"priority\":0,\"postRuleAction\":{\"onSuccess\":\"out.value = 'Successful_1'\",\"onFailure\":\"out.value = 'Failed_1'\"},\"active\":true}", Rule.class);
        rule2 = mapper.readValue("{\"name\":\"Rule_2\",\"expression\":\"RuleInp != null && RuleInp.inner == null && RuleInp.name != null && RuleInp.name.equals('2')\",\"priority\":0,\"postRuleAction\":{\"onSuccess\":\"out.value = 'Successful_2'\",\"onFailure\":\"out.value = 'Failed_2'\"},\"active\":true}", Rule.class);
        rule3 = mapper.readValue("{\"name\":\"Rule_3\",\"expression\":\"RuleInp != null && RuleInp.name != null && RuleInp.name.equals('3')\",\"priority\":0,\"postRuleAction\":{\"onSuccess\":\"out.value = 'Successful_3'\",\"onFailure\":\"out.value = 'Failed_3'\"},\"active\":true}", Rule.class);

        ruleEnum = Rule.builder().name("SampleRule")
                .expression("RuleInp != null && RuleInp.inner != null && RuleInp.inner.value != null && RuleInp.inner.value.equals('InnerValue')")
                .isActive(true)
                .priority(0)
                .postRuleAction(
                        Rule.PostRuleAction.builder()
                                .onSuccess("out.value = true; out.reason = RuleInp.inner.value")
                                .onFailure("out.value = false")
                                .build()
                ).build();

        BasicRulesRepository rulesRepository = new BasicRulesRepository();
        rulesRepository.addRuleSet("CTX_1", FIRST_SUCCESS.name(), Arrays.asList(rule1, rule2, rule3));
        rulesRepository.addRuleSet("CTX_2", FIRST_SUCCESS.name(), Arrays.asList(ruleEnum));
        executionHandler = new RuleExecutionHandler(rulesRepository, SimpleRuleOutput::new);
    }

    @Test
    public void test() throws JsonProcessingException, RuleEvaluationException, ExecutionException, InterruptedException, RuleSetDoesnotExistException {
        RuleInput ruleInput;
        Optional<SimpleRuleOutput> output;
        ruleInput = new RuleInput();
        output = executionHandler.execute(getSample(ruleInput), "CTX_1", EvaluationStrategy.STOP_ON_FIRST_SUCCESS);
        validate(ruleInput, output.get());
        ruleInput = new RuleInput();
        output = executionHandler.execute(getSample(ruleInput), "CTX_1", EvaluationStrategy.STOP_ON_FIRST_SUCCESS);
        validate(ruleInput, output.get());
        ruleInput = new RuleInput();
        output = executionHandler.execute(getSample(ruleInput), "CTX_1", EvaluationStrategy.STOP_ON_FIRST_SUCCESS);
        validate(ruleInput, output.get());
        ruleInput = new RuleInput("InnerValue");
        output = executionHandler.execute(getSample(ruleInput), "CTX_2", EvaluationStrategy.STOP_ON_FIRST_SUCCESS);
        Object value = output.get().getValue();
        assertEquals("InnerValue", output.get().getReason());
        assertEquals(true, output.get().isSuccess());
    }

    Map getSample(RuleInput ruleInput) throws JsonProcessingException {
        return mapper.readValue(mapper.writeValueAsString(ruleInput), Map.class);
    }

    private void validate(RuleInput ruleInput, RuleOutput output) {
        assertEquals("Successful_" + ruleInput.getName(), output.getValue());
        assertEquals("Rule_" + ruleInput.getName(), output.getReason());
        assertEquals(true, output.isSuccess());
    }

    @Getter
    public static class RuleInput {
        private static int ID = 1;
        private final String name;
        private final InnerClass inner;

        public RuleInput() {
            inner = null;
            this.name = Integer.toString(ID);
            ID += 1;
        }

        public RuleInput(String value) {
            inner = new InnerClass(value);
            this.name = Integer.toString(ID);
            ID += 1;
        }

        @Getter
        public class InnerClass {
            private String value;

            public InnerClass(String value) {
                this.value = value;
            }
        }
    }

    @Data
    static class SimpleRuleOutput<V extends Comparable> extends RuleOutput<V, SimpleRuleOutput> {
        @Override
        public int compareTo(SimpleRuleOutput other) {
            return 0;
        }
    }
}
