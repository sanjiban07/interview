# AutoAgile
## Problem Statement
Design a system for SDLC management
* Email ID: vivek.jain@mindtickle.com
## Components of AutoAgile
AutoAgile will be following a workflow based design pattern for delivering features.
### Workflows
* Defines the SDLC workflow.
* Sample workflow for agile development<br>
![AutoAgileWorkflowChart](AutoAgileWorkflowChart.jpg) 
* Sample workflow for onboarding of new developers<br>
![NewJoinee](NewJoinee.jpg) 
### Task
* Tasks are units of work. 
* Every task has following components:
	* Worker (Employee who works)
	* Owner (Employee who will do final review and close/reopen the task)
	* Subtasks (Tasks which need to be completed to complete this task)
	* Next tasks (Next start which should start once this is complete)
## Workflow Executor
Workflow executor system design<br>
![WorkflowExecutor](WorkflowExecutor.jpg)
### Compoents of Workflow Executor
* Admin Service
	* Add or update workflows
* Reporting Service
	* Project reports
	* Efficiency
	* Performance report
* Workflow Service
	* Receive task updates from employees and push to workflow events queue.
* Work Manager Service
	* Capacity management
	* Employee allocation
* Config Service
	* Add or update workflow configs
* Task Executor
	* State machine
	* Get current workflow context and workflow status and determine next workflow status.
* Rule engine
	* Process workflow rules
* Workflow Context Service
	* Provide faster access for workflow contexts
* Workflow Analyser
	* Frequent job to crunch workflow data from workflow updates
* Reporting Analyser
	* Create reporting view