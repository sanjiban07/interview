# Doordash/Swiggy/Uber eats
Food delivery service founded

### Points before starting
* Understand the functional as well as non-functional requirements
* Discuss the scope of the problem
* Identify the users
* Discuss about the scale & growth

## Requirements and Goals of the System
### Main actors in our system
* Customers/consumers
* Restaurant (Merchant)
* Doordashers (Drivers)
* DoorDash Admin (from the company)
### Assumptions
* Unlimited food is available between the times a restaurant opens up and closes. So, no need to check for the quantity of available food. No inventory management required.
* Restaurants don’t have their own online ordering system/infrastructure, they will solely rely on us for all orders.
* Customers will be shown restaurants within a particular radius, say 10 miles.
* Customers are allowed to order food from only one restaurant at a time in an online order. Menu items from multiple restaurants can’t be combined in an order.
### Functional Requirements
* Customers should be able to:
	* Search for restaurants based on the cuisines type, menu items, etc.
	* Create a cart, add menu items to the cart and order.
	* Receive notifications about the status of an order once placed.
	* Track the status of the order on the app.
	* Cancel the order.
	* Pay for the order.
	* Create/Update their account and contact information.
* Restaurants should be able to:
	* Create their profile (onboarding) and create/refresh/add new menu items, pictures.
	* Receive notifications about orders placed, assigned doordasher, and update the status of order etc.
	* Offboarding: If the restaurant goes out of business, or decides to discontinue taking online orders.
* Doordashers should be able to:
	* Receive notifications about the available orders in the area, from which they can choose.
	* Know when the order is available for pickup.
	* Inform the customer/restaurant of any problems they encounter while executing the order pickup/delivery.
	* De-register in case they don’t want to continue with the job anymore.

### Non Functional Requirements
* Latency: Hungry users will not like to wait to see the menu/ restaurant details, so the search functionality should be very fast. The ordering experience should also not have high latency and must be seamless and fast. Also, since the Restaurant/menu related data will be entered through a different service altogether, the lag between the data being entered and the result showing up in the search should be acceptable but shouldn't be too much.
* Consistency: When a new restaurant is onboarded, or a new menu is added, the information needn’t be available immediately. Eventual consistency is desirable. However, when an order is placed, the customer, the restaurant, and the door dasher should see the same order without any issues. Hence, in this scenario consistency is imperative.
* Availability: High availability is desirable for the best experience of a customer and also the restaurants that are processing the order, as well as the doordasher. No restaurant would like their business to go down just because the system has crashed, that's a serious loss of $.
* High Throughput: The system should be able to handle high peak load without problems or failures.

### Out of scope
* Customers being able to rate a restaurant or a specific doordasher.
* Sales reporting, dashboarding capabilities.
* Doordasher onboarding & payout.
* Restaurant payouts.
* Customer service/management.
* OAuth 2.0 authentication via Facebook/Google/Apple, etc.
* Recommendations based on customer’s order history/profile and other preferences.
* Mapping capabilities (in the form of Google Maps), to show the location of the doordasher, or location of the restaurant.
* ETA calculation using Isochrones.
* DoorDash admin functionalities.
* Supply/demand logistics & decision making.


### Search ecosystem
::: mermaid
graph LR
    CX[Customer]-->RSS[Restaurant<br/>Search service]
	RSS --> |Faster<br/>lookup| C[Cache]	
	RSS --> E["Search Cluster<br/>(Elasticsearch)"]
	R[Restaurant] --> RS[Restaurant<br/>Profile Service]
	RS --> Q[Queue]
	Q --> DI[Data Indexer]
	Q --> DI[Data Indexer]
	DI --> E
	RS --> RDBMS[Database]
	DI --> RDBMS
	RS --> S3[Object Storage]
:::
### Search ecosystem
::: mermaid
flowchart LR
	CX[Customer]<-->Search
	CX[Customer]<-->OMS[Order Service]
	OMS <-->  PG[Payment Gateway]
	CX[Customer]<-->FF[Fulfilment Service]
	CX[Customer]<-->UP[User Profile <br/>Mgmt. Service]
	UP<-->RDBMS[Database]
	A[Delivary Agent] <--> UP
	A <--> FF
	A <--> DP[Dispatch Service]
	FF <--> R[Restaurant]
	OMS --> Queue
	FF <--> Queue
	DP <--> Queue
	Queue --> NS[Notification<br/Service>]
	NS --> CX1[Customer]
	NS --> R1[Restaurant]
	NS --> A1[Agent]
:::

sequenceDiagram
    participant Alice
    participant Bob
    Alice->>John: Hello John, how are you?
    loop Healthcheck
        John->>John: Fight against hypochondria
    end
    Note right of John: Rational thoughts <br/>prevail!
    John-->>Alice: Great!
    John->>Bob: How about you?
    Bob-->>John: Jolly good!