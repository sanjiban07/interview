# News App
## FR
* Personalized news
* Swipe and read stories - infinite scrolling/finite scrolling both are fine
## NFR

## Components
* Feed Service
    * Get feed for a user
    * Create personalized feed
* Personalization Engine
    * Track users preferences
* Discovery Service
    * New articles from different sources



Example
* user ->
    * News 1 => SRC 1
    * News 1 => SRC 2
* 

User feed  Cache
{
    u1: [art_1, art_2, art_3]
}

Tracking
* user opened article
* user read article


art_1: {tag_1, tag_2}
art_2: {tag_2, tag_3}

tag_2: 2, tag_1: 1

GET feed/user/<user_id>?country=&city=?

Response:
{
    "feed": [
        {
            id,
            title,
            snippet,
            img
            url
        },
        {
            id,
            title,
            snippet,
            img
            url
        }
    ]
}


### Article DB (NoSQL)
{
    article_id1: {
        id,
        title,
        snippet,
        img
        url
        topic_ids: []
    }
}

{
    topic1 : {
        articles_id_1: Count
        articles_id_2: Count
    }
}

### SQL
* Topic
topic_id, count


POST user/<user1>/click/article/<articleId>
{
    topics: []
}

### Users Preferred Topics
{
    user: {
        topics: cnt
    }
}